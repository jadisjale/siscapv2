package test;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.junit.Ignore;
import org.junit.Test;

import br.com.siscap.dao.LicitanteJuridicoDAO;
import br.com.siscap.dao.OrgaoLicitanteDAO;
import br.com.siscap.dao.ProcessoDAO;
import br.com.siscap.dao.SecretariaDAO;
import br.com.siscap.dao.UsuarioDAO;
import br.com.siscap.model.Fornecedor;
import br.com.siscap.model.Funcao;
import br.com.siscap.model.LicitanteJuridico;
import br.com.siscap.model.OrgaoLicitante;
import br.com.siscap.model.Processo;
import br.com.siscap.model.Secretaria;
import br.com.siscap.model.Usuario;

public class TestApp {

	@Test
	@Ignore
	public void salvar() {
		Usuario u = new Usuario();
		u.setNome("TESTE");
		u.setMatricula("1234");
		u.setSenha("123");
		Funcao f = new Funcao();
		f.setId(1l);
		u.setFuncao(f);
		SimpleHash hash = new SimpleHash("md5", u.getSenha());
		u.setSenha(hash.toHex());

		UsuarioDAO usuarioDAO = new UsuarioDAO();
		usuarioDAO.save(u);

	}

	@Test
	@Ignore
	public void salvarProcesso() throws ParseException {

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		java.sql.Date data1 = new java.sql.Date(format.parse("11/07/2014").getTime());

		java.sql.Date data2 = new java.sql.Date(format.parse("11/07/2015").getTime());
		java.sql.Date data3 = new java.sql.Date(format.parse("11/07/2016").getTime());

		Processo p = new Processo();
		p.setComissaoRespProc("teste");
		p.setDataContrato(data1);
		p.setEntradaProcesso(data2);
		p.setInicioProcesso(data3);
		p.setModalidadeProc("teste");
		p.setNumeroProcesso("teste");
		p.setNumeroSolicProc("teste");
		p.setNumModalProc("teste");
		p.setObjetoProc("teste");
		p.setObservacaoProc("teste");
		p.setSituacaoProc("teste");
		BigDecimal payment = new BigDecimal("1115.37");
		p.setVlorEstimadoProc(payment);
		Secretaria s = new Secretaria();
		Secretaria s2 = new Secretaria();
		s.setId(11L);
		s2.setId(12L);
		ProcessoDAO dao = new ProcessoDAO();
		dao.save(p);
	}
	
	@Test
	@Ignore
	public void salvarSecretaria() throws ParseException {

		OrgaoLicitante o = new OrgaoLicitante();
		LicitanteJuridico l = new LicitanteJuridico();
		Secretaria s = new Secretaria();
		OrgaoLicitanteDAO oDAO =  new OrgaoLicitanteDAO();
		LicitanteJuridicoDAO lDAO = new LicitanteJuridicoDAO();
		SecretariaDAO sDAO = new SecretariaDAO();
		
		o.setNomeLicitante("Secret�ria de Sa�de");
		o.setTitularLicitante("Jo�o Paiva");
		o.setTelefoneLicitante("(85)32902036");
		o.setFaxLicitante("n�o possui");
		o.setEmailLicitante("sescSaude@gov.com.br");
		o.setTelefoneTitularLicitante("(8532351762)");
		o.setEmailTitularLicitante("joaopaiva@gmail.com");
		o.setRua("Av. Mister hull");
		o.setBairro("Antonio Bezerra");
		o.setCidade("Fortaleza");
		o.setEstado("Cear�");
		o.setCep("60.352-330");
		oDAO.save(o);
		
		l.setOrgaoLicitante(o);
		l.setCnpj("76.587.967/0001-83");
		lDAO.save(l);
		
		s.setLicitanteJuridico(l);
		s.setGerenteSec("Jo�o Paiva");
		s.setTelGerenteSec("(85)32356018");
		s.setCelGerenteSec("(85)988279902");
		s.setEmailGerenteSec("joaopaiva@gmail.com");
		
		sDAO.save(s);
		
	}


}
