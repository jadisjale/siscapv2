<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SISCAP | LOGIN</title>

<!-- imports js -->
<script src="resources/js/jquery.js" type="text/javascript"></script>
<!-- finish -->

<!-- imports css -->
<link href="resources/css/bootstrap/css/bootstrap-theme.min.css"
	rel="stylesheet">
<link href="resources/css/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="resources/css/custom/Login.css" rel="stylesheet">
<link href="resources/css/custom/font-awesine.css" rel="stylesheet">
<link href="resources/css/custom/animate.css" rel="stylesheet">
<!-- finish -->

</head>
<body>
<body class="gray-bg">
	<div class="middle-box text-center loginscreen animated fadeInDown">
		<div>
			<div>
				<h1 class="logo-name">SISCAP</h1>
			</div>
			<h3>Bem vindo(a) ao SISCAP</h3>
			<p>O SISCAP � um sistema para acompanhamento de licita��o.</p>
			<p>Entre. Para v�-lo em a��o.</p>
			<form class="m-t" role="form" action="/SisCapV2/admin/home">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Matr�cula"
						required="">
				</div>
				<div class="form-group">
					<input type="password" class="form-control" placeholder="Senha"
						required="">
				</div>
				<button type="submit" class="btn btn-primary block full-width m-b">Login</button>
			</form>
			<p class="m-t">
				<small>Desenvolvido por WeSoluctions &copy; 2016</small>
			</p>
		</div>
	</div>
</body>

<script type="text/javascript" src="resources/js/custom/adminLogin.js"></script>
<script>
	$(document).ready(function() {

	});
</script>
</html>