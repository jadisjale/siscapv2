var adminFornecedorFisico = {
		index: function(){
			// input mask telefone
			 $('#telGerenteSec').mask('(99) 99999-9999');
			 $('#celGerenteSec').mask('(99) 99999-9999');
			 $('#telefoneTitularLicitante').mask('(99) 99999-9999');
			 $('#telefoneLicitante').mask('(99) 99999-9999');		 
			// fim mask telefone
			 
			 $('#cpf').mask('999.999.999-99');
			 
			 $('.datepicker').datepicker({
					language : 'pt-BR',
					format : 'dd/mm/yyyy',
					startDate : '-3d',
				});

				$(".datepicker").each(function(index, element) {
					var context = $(this);
					context.on("blur", function(e) {
						// The setTimeout is the key here.
						setTimeout(function() {
							if (!context.is(':focus')) {
								$(context).datepicker("hide");
							}
						}, 250);
					});
				});
				
				adminFornecedorFisico.validador();
		},
		
		// se correto retorna true
		valideEmail : function(email) {
			er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
			if (er.exec(email))
				return true;
			else
				return false;
		},
		
		iscpfValue : function (cpfValue) {
            cpfValue = cpfValue.replace(/[^\d]+/g,'');    
               if(cpfValue == '') return false; 
               // Elimina cpfValues invalidos conhecidos    
               if (cpfValue.length != 11 || 
                   cpfValue == "00000000000" || 
                   cpfValue == "11111111111" || 
                   cpfValue == "22222222222" || 
                   cpfValue == "33333333333" || 
                   cpfValue == "44444444444" || 
                   cpfValue == "55555555555" || 
                   cpfValue == "66666666666" || 
                   cpfValue == "77777777777" || 
                   cpfValue == "88888888888" || 
                   cpfValue == "99999999999")
                       return false;       
               // Valida 1o digito 
               add = 0;    
               for (i=0; i < 9; i ++)       
                   add += parseInt(cpfValue.charAt(i)) * (10 - i);  
                   rev = 11 - (add % 11);  
                   if (rev == 10 || rev == 11)     
                       rev = 0;    
                   if (rev != parseInt(cpfValue.charAt(9)))     
                       return false;       
               // Valida 2o digito 
               add = 0;    
               for (i = 0; i < 10; i ++)        
                   add += parseInt(cpfValue.charAt(i)) * (11 - i);  
               rev = 11 - (add % 11);  
               if (rev == 10 || rev == 11) 
                   rev = 0;    
               if (rev != parseInt(cpfValue.charAt(10)))
                   return false;       
               return true;   
       },
			
		validador : function() {
			
			$('#submitSavefisico').click(function(event){
				event.preventDefault();
				
				console.log("click");
							
				var nomeLicitatne = $('#nomeLicitante').val();
				var titularLicitante = $('#titularLicitante').val();
				var telefoneLicitante = $('#telefoneLicitante').val();
				var faxLicitante = $('#faxLicitante').val();
				var emailLicitante = $('#emailLicitante').val();
				var telefoneTitularLicitante = $('#telefoneTitularLicitante').val();
				var emailTitularLicitante = $('#emailTitularLicitante').val();
				var rua = $('#rua').val();
				var bairro = $('#bairro').val();
				var cidade = $('#cidade').val();
				var estado = $('#estado').val();
				var cep = $('#cep').val();
				var cpf = $('#cpf').val();
				var expedicaoForn = $('#expedicaoForn').val();
				var numRegstroForn = $('#numRegstroForn').val();
				var validadeForn = $('#validadeForn').val();
				
				$( ".alert-danger" ).remove();
				
				if(nomeLicitatne.length > 3 && nomeLicitatne != null){
					adminUser.removeBorderMessage('.form-nome-licitante', '#border-nome-licitante', '#message-nome-licitante');
					if(titularLicitante.length > 3 && titularLicitante != null){
						adminUser.removeBorderMessage('.form-titular-licitante', '#border-titular-licitante', '#message-titular-licitante');
						if(telefoneLicitante.length > 3 && telefoneLicitante != null){
							adminUser.removeBorderMessage('.form-telefone-licitante', '#border-telefone-licitante', '#message-telefone-licitante');
							if(faxLicitante.length > 3 && faxLicitante != null){
								adminUser.removeBorderMessage('.form-fax-licitante', '#border-fax-licitante', '#message-fax-licitante');
								if(adminSecretaria.valideEmail(emailLicitante)== true){
									adminUser.removeBorderMessage('.form-email-licitante', '#border-email-licitante', '#message-email-licitante');
									if(telefoneTitularLicitante.length > 3 && telefoneTitularLicitante != null){
										adminUser.removeBorderMessage('.form-telefone-titular', '#border-telefone-titular', '#message-telefone-titular');
										if(adminSecretaria.valideEmail(emailTitularLicitante)== true){
											adminUser.removeBorderMessage('.form-email-titular', '#border-email-titular', '#message-email-titular');
											if(rua.length > 3 && rua != null){
												adminUser.removeBorderMessage('.form-rua', '#border-rua', '#message-rua');
												if(bairro.length > 3 && bairro != null){
													adminUser.removeBorderMessage('.form-bairro', '#border-bairro', '#message-bairro');
													if(cidade.length > 3 && cidade != null){
														adminUser.removeBorderMessage('.form-cidade', '#border-cidade', '#message-cidade');
														if(estado.length > 3 && estado != null){
															adminUser.removeBorderMessage('.form-estado', '#border-estado', '#message-estado');
															if(cep.length > 3 && cep != null){
																adminUser.removeBorderMessage('.form-cep', '#border-cep', '#message-cep');
																if(adminFornecedorFisico.iscpfValue(cpf) == true){
																	adminUser.removeBorderMessage('.form-cpf', '#border-cpf', '#message-cpf');
																	if(expedicaoForn.length > 3 && expedicaoForn != null){
																		adminUser.removeBorderMessage('.form-expedicaoForn', '#border-expedicaoForn', '#message-expedicaoForn');
																		if(numRegstroForn.length > 3 && numRegstroForn != null){
																			adminUser.removeBorderMessage('.form-numRegstroForn', '#border-numRegstroForn', '#message-numRegstroForn');
																			if(validadeForn.length > 3 && validadeForn != null){
																				adminUser.removeBorderMessage('.form-validadeForn', '#bordervalidadeForn', '#message-validadeForn');
																				adminFornecedorFisico.ajaxSaveFornecedorFisisco();
																			} else {
																					adminUser.addBorderMessage('.form-validadeForn', '#bordervalidadeForn', '#message-validadeForn', 'Validade Fornecedor', '#validadeForn');
																				} 
																			} else {
																				adminUser.addBorderMessage('.form-numRegstroForn', '#border-numRegstroForn', '#message-numRegstroForn', 'Número Registro Fornecedor', '#numRegstroForn');
																			}
																		} else {
																			adminUser.addBorderMessage('.form-expedicaoForn', '#border-expedicaoForn', '#message-expedicaoForn', 'Expedição Fornecedor', '#expedicaoForn');
																		}
																	} else {
																		adminUser.addBorderMessage('.form-cpf', '#border-cpf', '#message-cpf', 'CPF', '#cpf');
																	}
																} else {
																	adminUser.addBorderMessage('.form-cep', '#border-cep', '#message-cep', 'CEP', '#cep');
																}
															} else {
																adminUser.addBorderMessage('.form-estado', '#border-estado', '#message-estado', 'Estado', '#estado');
															}
														} else {
															adminUser.addBorderMessage('.form-cidade', '#border-cidade', '#message-cidade', 'Cidade', '#cidade');
														}
													} else {
														adminUser.addBorderMessage('.form-bairro', '#border-bairro', '#message-bairro', 'Bairro', '#bairro');
													}
												} else {
													adminUser.addBorderMessage('.form-rua', '#border-rua', '#message-rua', 'Rua', '#rua');
												}
											}else {
												adminUser.addBorderMessage('.form-email-titular', '#border-email-titular', '#message-email-titular', 'Email Titular', '#emailTitularLicitante');
											}
										} else {
											adminUser.addBorderMessage('.form-telefone-titular', '#border-telefone-titular', '#message-telefone-titular', 'Telefone Celular', '#telefoneTitularLicitante');
										}
									}else{
										adminUser.addBorderMessage('.form-email-licitante', '#border-email-licitante', '#message-email-licitante', 'Email Licitante', '#emailLicitante');
									}
								} else {
									adminUser.addBorderMessage('.form-fax-licitante', '#border-fax-licitante', '#message-fax-licitante', 'Fax Licitante', '#faxLicitante');
								}
							} else {
								adminUser.addBorderMessage('.form-telefone-licitante', '#border-telefone-licitante', '#message-telefone-licitante', 'Telefone Licitante', '#telefoneLicitante');
							}
						} else {
							adminUser.addBorderMessage('.form-titular-licitante', '#border-nome-licitante', '#message-titular-licitante', 'Titular Licitante', '#titularLicitante');
						}
					} else {
						adminUser.addBorderMessage('.form-nome-licitante', '#border-nome-licitante', '#message-nome-licitante', 'Nome Licitante', '#nomeLicitante');
					}
			});
			
		},
		
		ajaxSaveFornecedorFisisco : function() {
			console.log("ajax");
			$.ajax({
				url : '/SisCapV2/admin/saveFornecedorFisico',
				type : 'POST',
				data : $('#saveFormFornecedorFisico').serialize(),
				success : function(data) {
					adminUser.messageSuccess($('#nomeLicitante').val());
					adminFornecedorFisico.clearInputs();
				},
				error : function(data) {
					console.log("error");
					$("form").append("<div class='alert alert-danger' role='alert' >Registro duplicado!</div>");
					},
					complete : function(data) {

					}

			});
		},
		
		clearInputs : function (){
			$('#nomeLicitante').val("");
			$('#titularLicitante').val("");
			$('#telefoneLicitante').val("");
			$('#faxLicitante').val("");
			$('#emailLicitante').val("");
			$('#telefoneTitularLicitante').val("");
			$('#emailTitularLicitante').val("");
			$('#rua').val("");
			$('#bairro').val("");
			$('#cidade').val("");
			$('#estado').val("");
			$('#cep').val("");
			$('#cpf').val("");
			$('#expedicaoForn').val("");
			$('#numRegstroForn').val("");
			$('#validadeForn').val("");
		}

}