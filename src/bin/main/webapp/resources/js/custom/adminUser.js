adminUser = {
		
		table : '#tableUser',
		
		index : function() {
			$('#inputNameUser').focus();
			adminUser.validation();
		},
		
		indexList : function() {
			adminUser.loadTableUser();
		},
		
		validation : function(){
			$('#submitSaveUser').click(function(event){
				event.preventDefault();
				$( ".alert-danger" ).remove();
				
				var id = $("#id").val();
				var funcao = $( "#functionUser" ).val();
				var nome = $('#inputNameUser').val();
				var matricula = $('#inputRegistrationUser').val();
				var senha = $('#inputPasswordUser').val();
				
				if(nome.length > 3 && nome != null){
					adminUser.removeBorderMessage('.form-name-user', '#borderUser', '#messageUser');
					if(matricula.length > 3 && matricula != null){
						adminUser.removeBorderMessage('.form-registrion-user', '#form-registrion-user', '#messageRegisterUser');
						if(senha.length > 3 && senha != null){
							adminUser.removeBorderMessage('.form-password-user', '#form-password-user', '#messagePasswordUser');
							$.ajax({
									url : '/SisCapV2/admin/saveUser',
									type : 'POST',
									data : {
										id : id,	
										fkFuncao: funcao,
										nome : nome,
										matricula : matricula,
										senha : senha
									},
									success : function(data) {
										adminUser.messageSuccess($('#inputNameUser').val());
										$('#inputNameUser').val("");
										$('#inputRegistrationUser').val("");
										$('#inputPasswordUser').val("");
										$('#inputNameUser').focus();
										$('#functionUser option[value=1]').attr('selected','selected');
									},
									error : function(data) {
										$("form").append(
											"<div class='alert alert-danger' role='alert' >Registro duplicado!</div>");
									},
									complete: function(data){
		 								
									}
		
						 	});
						} else {
							adminUser.addBorderMessage('.form-password-user', '#form-password-user', '#messagePasswordUser', 'Senha', '#inputPasswordUser');
						} 
					} else {
						adminUser.addBorderMessage('.form-registrion-user', '#form-registrion-user', '#messageRegisterUser', 'Registro', '#inputRegistrationUser');
					}
				} else {		
					adminUser.addBorderMessage('.form-name-user', '#borderUser', '#messageUser', 'Nome', '#inputNameUser');
				}
			});
		},
		
		addBorderMessage: function (form, border, message, name, input){
			adminUser.removeBorderMessage(form, border, message);
			$(input).focus();
			$(form).addClass('has-error has-feedback');
			$(border).addClass('glyphicon glyphicon-remove form-control-feedback');
			$(message).addClass('help-block');
			$(message).append("Campo "+ name +" requerido");
		},
		
		removeBorderMessage: function (form, border, message){
			$(form).removeClass('has-error has-feedback');
			$(border).removeClass('glyphicon glyphicon-remove form-control-feedback');
			$(message).removeClass('help-block');
			$(message).html("");
		},
		
		messageSuccess: function(nameUser){
			$("form").append(
					"<div class='alert alert-info alert-dismissible' role='alert' style='margin-top: 15px'> "
					+ "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"
					+ "<strong>Usuario!</strong> <strong>"
					+ nameUser
					+ "</strong> salvo com sucesso.</div>");
						
					setTimeout(function(){ $( ".alert-info" ).remove(); }, 4000);			
		},
		
		loadTableUser: function(){
			$(adminUser.table).dataTable( {
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
		        },
				   "ajax": {
				      "url": window.location.protocol + '//' + window.location.host + "/SisCapV2/admin/getUsers",
				      "dataSrc": "",
				   },
				   "columns": [
				      { "data": "nome" },
				      { "data": "matricula"},
				      { "data": "funcao.funcao"},
				      { "data": "id"},
				      { "data": "id"}
				   ],
				   columnDefs: [
				                {
				                    render: function(data) {
				                        return "<a href= '/SisCapV2/admin/getUser/" +data+ "' class='edit'><i class='glyphicon glyphicon-pencil'></i></a>";
				                    },
				                    targets: 3
				                },
				                {
				                    render: function(data) {
				                        return "<a href= 'javascript:void(0);' class='del'><i class='glyphicon glyphicon-remove'></i></a>"
				                    },
				                    targets: 4
				                },
				                { orderable: false,  targets: [3, 4] }
				            ],
				            	
				            createdRow: function (row, data, index) {
				                adminUser.setClickInCreatedRow(row, data);
				            }
				});
	
		},
		
		setClickInCreatedRow: function(htmlRow, data) {
	        $(htmlRow).find('.del').click(function(event) {
	            
	        	event.preventDefault();
	        	$.confirm({
	                title: 'Confirme o delete!',
	                text: 'Deseja deletar o registro?',
	                confirmButton: 'Delete',
	                cancelButton: 'Cancelar',
	                confirmButtonClass: 'btn-primary',
	                cancelButtonClass: 'btn-danger',
	                confirm: function() { adminUser.ajaxDeleteUser(data.id); },
	                cancel: function() { }
	            });
	        });
	    },
	    
	    ajaxDeleteUser : function (id){
	    	$.ajax({
				url : '/SisCapV2/admin/getUser/remove/'+id,
				type : 'POST',
				data : {
					id : id	
				},
				success : function(data) {
					$('#tableUser').DataTable().ajax.reload();
				},
				error : function(data) {
					$("form").append(
						"<div class='alert alert-danger' role='alert' >Erro ao excluir</div>");
				},
				complete: function(data){
					$('#myModal').modal('hide');	
				}

	 			});
	    }
}