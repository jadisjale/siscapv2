var adminSecretaria = {

	index : function() {
		adminSecretaria.validador();
		$('#gerenteSec').focus();
		$('#telGerenteSec').mask('(99) 9999-9999');
		$('#celGerenteSec').mask('(99) 99999-9999');
	},

	indexList : function() {
		adminSecretaria.loadTableSecretaria();
	},
	
	loadTableSecretaria: function(){
		$('#tableSecretaria').dataTable( {
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
	        },
			   "ajax": {
			      "url": window.location.protocol + '//' + window.location.host + "/SisCapV2/admin/getSecretarias",
			      "dataSrc": "",
			   },
			   "columns": [
			      { "data": "licitanteJuridico.orgaoLicitante.nomeLicitante" },
			      { "data": "gerenteSec"},
			      { "data": "emailGerenteSec"},
			      { "data": "id"},
			      { "data": "id"},
			      { "data": "id"}
			   ],
			   columnDefs: [
			                {
			                    render: function(data) {
			                        return "<a href= '/SisCapV2/admin/getSecretaria/" +data+ "' class='edit'><i class='glyphicon glyphicon-pencil'></i></a>";
			                    },
			                    targets: 3
			                },
			                {
			                    render: function(data) {
			                        return "<a href= 'javascript:void(0);' class='del'><i class='glyphicon glyphicon-remove'></i></a>"
			                    },
			                    targets: 4
			                },
			                {
			                    render: function(data) {
			                        return "<a href= '/SisCapV2/admin/dtlLicitanteJuridico/" +data+ "' class='edit'><i class='glyphicon glyphicon-eye-open'></i></a>";
			                    },
			                    targets: 5
			                },
			                { orderable: false,  targets: [3, 4, 5] }
			            ],
			            	
			            createdRow: function (row, data, index) {
			                adminSecretaria.setClickInCreatedRow(row, data);
			            }
			});

	},
	
	setClickInCreatedRow: function(htmlRow, data) {
        $(htmlRow).find('.del').click(function(event) {
        	event.preventDefault();
        	$.confirm({
                title: 'Confirme o delete!',
                text: 'Deseja deletar o registro?',
                confirmButton: 'Delete',
                cancelButton: 'Cancelar',
                confirmButtonClass: 'btn-primary',
                cancelButtonClass: 'btn-danger',
                confirm: function() { adminSecretaria.ajaxDeleteSecretaria(data.id); },
                cancel: function() { }
            });
        });
    },
    
    ajaxDeleteSecretaria : function (id){
    	$.ajax({
			url : '/SisCapV2/admin/secretaria/remove/'+id,
			type : 'POST',
			data : {
				id : id	
			},
			success : function(data) {
				$('#tableSecretaria').DataTable().ajax.reload();
			},
			error : function(data) {
				$("form").append(
					"<div class='alert alert-danger' role='alert' >Erro ao excluir</div>");
			},
			complete: function(data){
				$('#myModal').modal('hide');	
			}

 			});
    },

	carregarJuridico : function(idLicitanteJuridico) {
		$.ajax({
			url : '/SisCapV2/admin/getJuridicosIdName',
			type : 'GET',
			dataType : 'json',
			success : function(data) {
				var cmb = "";
				$.each(data, function(i, value) {
					if(value.id != idLicitanteJuridico){
						cmb = cmb + '<option value="' + value.id + '">'
						+ value.orgaoLicitante.nomeLicitante + '</option>';
					} 
				});
				$('#fkJuridico').append(cmb);
			},
			complete : function() {
				$('#fkJuridico').select2();
			}
		});
	},

	validador : function() {
		$('#submitSaveSecretaria').click(
				function(event) {
					event.preventDefault();
					$(".alert-danger").remove();

					var id = $("#id").val();
					var gerenteSec = $("#gerenteSec").val();
					var celGerenteSec = $('#celGerenteSec').val();
					var telGerenteSec = $('#telGerenteSec').val();
					var emailGerenteSec = $('#emailGerenteSec').val();
					var fkJuridico = $('#fkJuridico').val();

					if (gerenteSec.length > 3 && gerenteSec != null) {
						adminUser.removeBorderMessage('.form-gerenteSec',
								'#border-gerenteSec', '#message-gerenteSec');
						if (celGerenteSec.length > 3 && celGerenteSec != null) {
							adminUser.removeBorderMessage(
									'.form-celGerenteSec',
									'#form-celGerenteSec',
									'#message-celGerenteSec');
							if (telGerenteSec != null
									&& telGerenteSec.length > 3) {
								adminUser.removeBorderMessage(
										'.form-telGerenteSec',
										'#form-telGerenteSec',
										'#message-telGerenteSec');
								if (adminSecretaria
										.valideEmail(emailGerenteSec) == true) {
									adminSecretaria.ajaxSaveSecretaria();
									adminUser.removeBorderMessage(
											'.form-emailGerenteSec',
											'#form-emailGerenteSec',
											'#message-emailGerenteSec');
								} else {
									adminUser
											.addBorderMessage(
													'.form-emailGerenteSec',
													'#form-emailGerenteSec',
													'#message-emailGerenteSec',
													'Email Gerente',
													'#emailGerenteSec');
								}
							} else {
								adminUser
										.addBorderMessage(
												'.form-telGerenteSec',
												'#form-telGerenteSec',
												'#message-telGerenteSec',
												'Telefone do Gerente',
												'#telGerenteSec');
							}
						} else {
							adminUser.addBorderMessage('.form-celGerenteSec',
									'#form-celGerenteSec',
									'#message-celGerenteSec',
									'Celular Gerente', '#celGerenteSec');
						}
					} else {
						adminUser.addBorderMessage('.form-gerenteSec',
								'#border-gerenteSec', '#message-gerenteSec',
								'Nome do gerente', '#gerenteSec');
					}
				});
	},

	ajaxSaveSecretaria : function() {
		$
				.ajax({
					url : '/SisCapV2/admin/saveSecretaria',
					type : 'POST',
					data : $('#saveFormSecretaria').serialize(),
					success : function(data) {
						adminUser.messageSuccess($('#gerenteSec').val());
						adminSecretaria.clearInputs();
					},
					error : function(data) {
						$("form")
								.append(
										"<div class='alert alert-danger' role='alert' >Registro duplicado!</div>");
					},
					complete : function(data) {

					}

				});
	},

	// se correto retorna true
	valideEmail : function(email) {
		er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
		if (er.exec(email))
			return true;
		else
			return false;
	},

	// se correto retorna true
	isCNPJValid : function(cnpj) {
		cnpj = cnpj.replace(/[^\d]+/g, '');
		if (cnpj == '')
			return false;
		if (cnpj.length != 14)
			return false;
		if (cnpj == "00000000000000" || cnpj == "11111111111111"
				|| cnpj == "22222222222222" || cnpj == "33333333333333"
				|| cnpj == "44444444444444" || cnpj == "55555555555555"
				|| cnpj == "66666666666666" || cnpj == "77777777777777"
				|| cnpj == "88888888888888" || cnpj == "99999999999999")
			return false;

		tamanho = cnpj.length - 2
		numeros = cnpj.substring(0, tamanho);
		digitos = cnpj.substring(tamanho);
		soma = 0;
		pos = tamanho - 7;
		for (i = tamanho; i >= 1; i--) {
			soma += numeros.charAt(tamanho - i) * pos--;
			if (pos < 2)
				pos = 9;
		}
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(0))
			return false;

		tamanho = tamanho + 1;
		numeros = cnpj.substring(0, tamanho);
		soma = 0;
		pos = tamanho - 7;
		for (i = tamanho; i >= 1; i--) {
			soma += numeros.charAt(tamanho - i) * pos--;
			if (pos < 2)
				pos = 9;
		}
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(1))
			return false;

		return true;
	},

	clearInputs : function() {
		$('#gerenteSec').val('');
		$('#celGerenteSec').val('');
		$('#emailGerenteSec').val('');
		$('#telGerenteSec').val('');
	}

}