var adminProcesso = {

	indexList : function() {
		adminProcesso.listProcesso();
	},
	
	listProcesso : function (){
		$('#tableProcesso').dataTable( {
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
	        },
			   "ajax": {
			      "url": window.location.protocol + '//' + window.location.host + "/SisCapV2/admin/getProcesso",
			      "dataSrc": "",
			   },
			   "columns": [
			      { "data": "numeroProcesso" },
			      { "data": "secretaria.nomeLicitante"},
			      { "data": "secretaria2.nomeLicitante"},
			      { "data": "id"},
			      { "data": "id"},
			      { "data": "id"}
			   ],
			   columnDefs: [
			                {
			                    render: function(data) {
			                        return "<a href= '/SisCapV2/admin/getSecretaria/" +data+ "' class='edit'><i class='glyphicon glyphicon-pencil'></i></a>";
			                    },
			                    targets: 3
			                },
			                {
			                    render: function(data) {
			                        return "<a href= '/SisCapV2/admin/detalheSecretaria/" +data+ "' class='open'><i class='glyphicon glyphicon-info-sign'></i></a>"
			                    },
			                    targets: 4
			                },
			                {
			                    render: function(data) {
			                        return "<a href= 'javascript:void(0);' class='del'><i class='glyphicon glyphicon-remove'></i></a>"
			                    },
			                    targets: 5
			                },
			                { orderable: false,  targets: [3, 4, 5] }
			            ],
			            
			            createdRow: function (row, data, index) {
			                adminProcesso.setClickInCreatedRow(row, data);
			            },  
			            
			});
	},
	
	setClickInCreatedRow: function(htmlRow, data) {
        $(htmlRow).find('.del').click(function(event) {
            
        	event.preventDefault();
        	$.confirm({
                title: 'Confirme o delete!',
                text: 'Deseja deletar o registro?',
                confirmButton: 'Delete',
                cancelButton: 'Cancelar',
                confirmButtonClass: 'btn-primary',
                cancelButtonClass: 'btn-danger',
                confirm: function() { adminProcesso.ajaxDeleteProcesso(data.id); },
                cancel: function() { }
            });
        });
    },
    
    ajaxDeleteProcesso : function (id){
    	$.ajax({
			url : '/SisCapV2/admin/getProcesso/remove/'+id,
			type : 'POST',
			data : {
				id : id	
			},
			success : function(data) {
				$('#tableProcesso').DataTable().ajax.reload();
			},
			error : function(data) {
				$("form").append(
					"<div class='alert alert-danger' role='alert' >Erro ao excluir</div>");
			},
			complete: function(data){
				
			}

 			});
    },

	index : function() {

		$('.datepicker').datepicker({
			language : 'pt-BR',
			format : 'dd/mm/yyyy',
			startDate : '-3d',
		});

		$(".datepicker").each(function(index, element) {
			var context = $(this);
			context.on("blur", function(e) {
				// The setTimeout is the key here.
				setTimeout(function() {
					if (!context.is(':focus')) {
						$(context).datepicker("hide");
					}
				}, 250);
			});
		});

		$('#vlorEstimadoProc').maskMoney({
			thousands : '',
			decimal : '.'
		});

		adminProcesso.carregarSelect();

		adminProcesso.validador();
	},

	carregarSelect : function() {
		$.ajax({
			url : '/SisCapV2/admin/getProcessoSecretaria',
			type : 'GET',
			dataType : 'json',
			success : function(data) {
				var cmb = "";
				$.each(data, function(i, value) {
					cmb = cmb + '<option value="' + value.id + '">'
							+ value.licitanteJuridico.orgaoLicitante.nomeLicitante + '</option>';
				});
				$('#idSecretariaDois').append(cmb);
				$('#idSecretariaUm').append(cmb);
			},
			complete : function() {
				$('#idSecretariaUm').select2();
				$('#idSecretariaDois').select2();
			}
		});
	},

	validador : function() {

		$('#submitSaveProcesso')
				.click(
						function(event) {
							event.preventDefault();

							// pegar valores e jogar em var
							var inicioProcesso = $('#inicioProcesso').val();
							var numeroProcesso = $('#numeroProcesso').val();
							var entradaProcesso = $('#entradaProcesso').val();
							var vlorEstimadoProc = $('#vlorEstimadoProc').val();
							var numeroSolicProc = $('#numeroSolicProc').val();
							var numModalProc = $('#numModalProc').val();
							var modalidadeProc = $('#modalidadeProc').val();
							var tipoProc = $('#tipoProc').val();
							var origemRecProc = $('#origemRecProc').val();
							var comissaoRespProc = $('#comissaoRespProc').val();
							var objetoProc = $('#objetoProc').val();
							var situacaoProc = $('#situacaoProc').val();
							var observacaoProc = $('#observacaoProc').val();
							var dataContrato = $('#dataContrato').val();
							var idSecretariaUm = $('#idSecretariaUm').val();
							var idSecretariaDois = $('#idSecretariaDois').val();
							// FIM VALORES

							$(".alert-danger").remove();

							if (inicioProcesso.length > 3
									&& inicioProcesso != null) {
								adminUser.removeBorderMessage(
										'.form-inicioProcesso',
										'#border-inicioProcesso',
										'#message-inicioProcesso');
								if (numeroProcesso.length > 3
										&& numeroProcesso != null) {
									adminUser.removeBorderMessage(
											'.form-numeroProcesso',
											'#border-numeroProcesso',
											'#message-numeroProcesso');
									if (entradaProcesso.length > 3
											&& entradaProcesso != null) {
										adminUser.removeBorderMessage(
												'.form-entradaProcesso',
												'#border-entradaProcesso',
												'#message-entradaProcesso');
										if (vlorEstimadoProc.length > 3
												&& vlorEstimadoProc != null) {
											adminUser
													.removeBorderMessage(
															'.form-vlorEstimadoProc',
															'#border-vlorEstimadoProc',
															'#message-vlorEstimadoProc');
											if (numeroSolicProc.length > 3
													&& numeroSolicProc != null) {
												adminUser
														.removeBorderMessage(
																'.form-numeroSolicProc',
																'#border-numeroSolicProc',
																'#message-numeroSolicProc');
												if (numModalProc.length > 3
														&& numModalProc != null) {
													adminUser
															.removeBorderMessage(
																	'.form-numModalProc',
																	'#border-numModalProc',
																	'#message-numModalProc');
													if (modalidadeProc.length > 3
															&& modalidadeProc != null) {
														adminUser
																.removeBorderMessage(
																		'.form-modalidadeProc',
																		'#border-modalidadeProc',
																		'#message-modalidadeProc');
														if (tipoProc.length > 3
																&& tipoProc != null) {
															adminUser
																	.removeBorderMessage(
																			'.form-tipoProc',
																			'#border-tipoProc',
																			'#message-tipoProc');
															if (origemRecProc.length > 3
																	&& origemRecProc != null) {
																adminUser
																		.removeBorderMessage(
																				'.form-origemRecProc',
																				'#border-origemRecProc',
																				'#message-origemRecProc');
																if (comissaoRespProc.length > 3
																		&& comissaoRespProc != null) {
																	adminUser
																			.removeBorderMessage(
																					'.form-comissaoRespProc',
																					'#border-comissaoRespProc',
																					'#message-comissaoRespProc');
																	if (objetoProc.length > 3
																			&& objetoProc != null) {
																		adminUser
																				.removeBorderMessage(
																						'.form-objetoProc',
																						'#border-objetoProc',
																						'#message-objetoProc');
																		if (situacaoProc.length > 3
																				&& situacaoProc != null) {
																			adminUser
																					.removeBorderMessage(
																							'.form-situacaoProc',
																							'#border-situacaoProc',
																							'#message-situacaoProc');
																			if (observacaoProc.length > 3
																					&& observacaoProc != null) {
																				adminUser
																						.removeBorderMessage(
																								'.form-observacaoProc',
																								'#border-observacaoProc',
																								'#message-observacaoProc');
																				if (dataContrato.length > 3
																						&& dataContrato != null) {
																					adminUser
																							.removeBorderMessage(
																									'.form-dataContrato',
																									'#border-dataContrato',
																									'#message-dataContrato');
																					if (idSecretariaUm != null) {
																						adminUser
																								.removeBorderMessage(
																										'.form-id1',
																										'#border-id1',
																										'#message-id1');
																						if (idSecretariaDois != null) {
																							adminUser
																									.removeBorderMessage(
																											'.form-id2',
																											'#border-id2',
																											'#message-id2');
																							adminProcesso
																									.ajaxSaveProcesso();
																						} else {
																							adminUser
																									.addBorderMessage(
																											'.form-id2',
																											'#border-id2',
																											'#message-id2',
																											'Secretaria Destinatário',
																											'#idSecretariaDois');
																						}
																					} else {
																						adminUser
																								.addBorderMessage(
																										'.form-id1',
																										'#border-id1',
																										'#message-id1',
																										'Secretaria Remetente',
																										'#idSecretariaUm');
																					}
																				} else {
																					adminUser
																							.addBorderMessage(
																									'.form-dataContrato',
																									'#border-dataContrato',
																									'#message-dataContrato',
																									'Data Contrato',
																									'#dataContrato');
																				}
																			} else {
																				adminUser
																						.addBorderMessage(
																								'.form-observacaoProc',
																								'#border-observacaoProc',
																								'#message-observacaoProc',
																								'Observação Processo',
																								'#observacaoProc');
																			}
																		} else {
																			adminUser
																					.addBorderMessage(
																							'.form-situacaoProc',
																							'#border-situacaoProc',
																							'#message-situacaoProc',
																							'Situacão Processo',
																							'#situacaoProc');
																		}
																	} else {
																		adminUser
																				.addBorderMessage(
																						'.form-objetoProc',
																						'#border-objetoProc',
																						'#message-objetoProc',
																						'Objeto Processo',
																						'#objetoProc');
																	}
																} else {
																	adminUser
																			.addBorderMessage(
																					'.form-comissaoRespProc',
																					'#border-comissaoRespProc',
																					'#message-comissaoRespProc',
																					'Comissão Responsável Processo',
																					'#comissaoRespProc');
																}
															} else {
																adminUser
																		.addBorderMessage(
																				'.form-origemRecProc',
																				'#border-origemRecProc',
																				'#message-origemRecProc',
																				'origemRecProc',
																				'#origemRecProc');
															}
														} else {
															adminUser
																	.addBorderMessage(
																			'.form-tipoProc',
																			'#border-tipoProc',
																			'#message-tipoProc',
																			'Tipo Processo',
																			'#tipoProc');
														}
													} else {
														adminUser
																.addBorderMessage(
																		'.form-modalidadeProc',
																		'#border-modalidadeProc',
																		'#message-modalidadeProc',
																		'Modalidade Processo',
																		'#modalidadeProc');
													}
												} else {
													adminUser
															.addBorderMessage(
																	'.form-numModalProc',
																	'#border-numModalProc',
																	'#message-numModalProc',
																	'Número Modalidade Processo',
																	'#numModalProc');
												}
											} else {
												adminUser
														.addBorderMessage(
																'.form-numeroSolicProc',
																'#border-numeroSolicProc',
																'#message-numeroSolicProc',
																'Número Solicitação Processo',
																'#numeroSolicProc');
											}
										} else {
											adminUser
													.addBorderMessage(
															'.form-vlorEstimadoProc',
															'#border-vlorEstimadoProc',
															'#message-vlorEstimadoProc',
															'Valor Estimado Processo',
															'#vlorEstimadoProc');
										}
									} else {
										adminUser.addBorderMessage(
												'.form-entradaProcesso',
												'#border-entradaProcesso',
												'#message-entradaProcesso',
												'Entrada Processo',
												'#entradaProcesso');
									}
								} else {
									adminUser.addBorderMessage(
											'.form-numeroProcesso',
											'#border-numeroProcesso',
											'#message-numeroProcesso',
											'Número Processo',
											'#numeroProcesso');
								}
							} else {
								adminUser.addBorderMessage(
										'.form-inicioProcesso',
										'#border-inicioProcesso',
										'#message-inicioProcesso',
										'Início processp', '#inicioProcesso');
							}
						});

	},

	ajaxSaveProcesso : function() {
		$
				.ajax({
					url : '/SisCapV2/admin/saveProcesso',
					type : 'POST',
					data : $('#saveFormProcesso').serialize(),
					success : function(data) {
						console.log("success");
						adminUser.messageSuccess($('#objetoProc').val());
						adminProcesso.clearInputs();
					},
					error : function(data) {
						console.log("error");
						$("form")
								.append(
										"<div class='alert alert-danger' role='alert' >Registro duplicado!</div>");
					},
					complete : function(data) {

					}

				});
	},

	clearInputs : function() {
		$('#inicioProcesso').val("");
		$('#numeroProcesso').val("");
		$('#entradaProcesso').val("");
		$('#vlorEstimadoProc').val("");
		$('#numeroSolicProc').val("");
		$('#numModalProc').val("");
		$('#modalidadeProc').val("");
		$('#tipoProc').val("");
		$('#origemRecProc').val("");
		$('#comissaoRespProc').val("");
		$('#objetoProc').val("");
		$('#situacaoProc').val("");
		$('#observacaoProc').val("");
		$('#dataContrato').val("");
		$('#idSecretariaUm').val("");
		$('#idSecretariaDois').val("");
	}
}