<%@include file="home.jsp"%>
<div class="container">
	<h2>Cadastro Secretaria</h2>
</div>
<hr>
<div class="container">
	<form role="form" id="saveFormSecretaria">

		<input type="hidden" name="id" value="${id}"></input>
		<input type="hidden" id="licitanteJuridico" value="${licitanteJuridico}"></input>
		<input type="hidden" id="idLicitanteJuridico" value="${idLicitanteJuridico}"></input>

		<div class="form-group form-gerenteSec">
			<label for="email">Nome Gerente Secretaria:</label> <input type="text"
				class="form-control" id="gerenteSec" required="required"
				name="gerenteSec" value="${gerenteSec}"> <span id="border-gerenteSec"></span> <span
				id="message-gerenteSec"></span>
		</div>
		<div class="form-group form-celGerenteSec">
			<label for="pwd">Celular Gerente Secretaria:</label> <input type="text"
				class="form-control" id="celGerenteSec" required="required"
				name="celGerenteSec" value="${celGerenteSec}"><span id="border-celGerenteSec"></span> <span
					id="message-celGerenteSec"></span>
		</div>
		<div class="form-group form-telGerenteSec">
			<label for="pwd">Telefone Gerente Secretaria:</label> <input type="text"
				class="form-control" id="telGerenteSec" required="required"
				name="telGerenteSec" value="${telGerenteSec}"><span id="border-telGerenteSec"></span> <span
					id="message-telGerenteSec"></span>
		</div>
		<div class="form-group form-emailGerenteSec">
			<label for="pwd">Email Gerente Secretaria:</label> <input type="text"
				class="form-control" id="emailGerenteSec" required="required"
				name="emailGerenteSec" value="${emailGerenteSec}"><span id="border-emailGerenteSec"></span> <span
					id="message-emailGerenteSec"></span>
		</div>
		
		<div class="form-group form-fkJuridico">
			<label for="email">Fornecedor:</label>
			<select id="fkJuridico" class="form-control selectField" name="fkJuridico">	
				
			</select>
			<span id="border-fkJuridico"></span> 
			<span id="message-fkJuridico"></span>
		</div>
		
		<button type="submit" class="btn btn-primary"
			id="submitSaveSecretaria">Salvar</button>
	</form>

</div>
<hr>
<%@include file="footer.jsp"%>
<script type="text/javascript">
	$(document).ready(function() {
		if($('#idLicitanteJuridico').val() != ''){
			var licitanteJuridico = $('#licitanteJuridico').val();
			var idLicitanteJuridico = $('#idLicitanteJuridico').val();
			var opt = "<option value='"+idLicitanteJuridico+"' selected ='selected'>" +licitanteJuridico + " </option>";
			$('#fkJuridico').append(opt);
			adminSecretaria.carregarJuridico(idLicitanteJuridico);
		} else {
			adminSecretaria.carregarJuridico(0);
		}
		adminSecretaria.index();
	});
</script>
