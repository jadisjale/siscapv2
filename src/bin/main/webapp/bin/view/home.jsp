<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SISCAP | MENU</title>

<link rel="stylesheet"
	href="/SisCapV2/resources/css/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="/SisCapV2/resources/css/bootstrap/css/dataTables.bootstrap.min.css">
<link rel="stylesheet"
	href="/SisCapV2/resources/css/bootstrap/css/jquery.dataTables.min.css">
<link rel="stylesheet"
	href="/SisCapV2/resources/css/bootstrap/css/bootstrap-select.min.css">
<link rel="stylesheet" href="/SisCapV2/resources/css/custom/siscap.css">
<link rel="stylesheet"
	href="/SisCapV2/resources/css/bootstrap/css/datepicker.css">
<link rel="stylesheet" href="/SisCapV2/resources/css/custom/select2.css">
<link rel="stylesheet"
	href="/SisCapV2/resources/css/select2-bootstrap.css">

<script src="/SisCapV2/resources/js/jquery.js"></script>
<script src="/SisCapV2/resources/js/select2.js"></script>
<script src="/SisCapV2/resources/js/mask.js"></script>
<script src="/SisCapV2/resources/js/jquery.maskMoney.js"></script>
<script src="/SisCapV2/resources/js/bootstrap/js/bootstrap.min.js"></script>
<script src="/SisCapV2/resources/js/bootstrap/js/jquery.dataTables.js"></script>
<script
	src="/SisCapV2/resources/js/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="/SisCapV2/resources/js/bootstrap/js/bootstrap-filestyle.js"></script>
<script
	src="/SisCapV2/resources/js/bootstrap/js/bootstrap-select.min.js"></script>
<script type="text/javascript"
	src="/SisCapV2/resources/js/bootstrap/js/bootstrap-datepicker.js"></script>
<script type="text/javascript"
	src="/SisCapV2/resources/js/custom/adminUser.js"></script>
<script type="text/javascript"
	src="/SisCapV2/resources/js/custom/adminTramite.js"></script>
<script type="text/javascript"
	src="/SisCapV2/resources/js/jquery.confirm.min.js"></script>
<script type="text/javascript" src="/SisCapV2/resources/js/mask.js"></script>
<script type="text/javascript"
	src="/SisCapV2/resources/js/custom/adminSecretaria.js"></script>
<script type="text/javascript"
	src="/SisCapV2/resources/js/custom/adminProcesso.js"></script>
<script type="text/javascript"
	src="/SisCapV2/resources/js/custom/adminFornecedorFisico.js"></script>
<script type="text/javascript"
	src="/SisCapV2/resources/js/custom/adminFornecedorJuridico.js"></script>
<script type="text/javascript"
	src="/SisCapV2/resources/js/custom/adminProduto.js"></script>


</head>
<body>
	<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
				aria-expanded="false">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/SisCapV2/admin/home">In�cio</a>
		</div>

		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false">Fornecedor<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="/SisCapV2/admin/addFornecedorFisico">Adicionar
								| F�sico</a></li>
						<li><a href="/SisCapV2/admin/addFornecedorJuridico">Adicionar
								| Jur�dico</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="/SisCapV2/admin/addProduto">Adicionar
								Produto</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="/SisCapV2/admin/listLicitanteJuridico">Lista jur�dica</a></li>

						<li role="separator" class="divider"></li>
						<li><a href="/SisCapV2/admin/listProduto">Lista de
								Produtos</a></li>
					</ul></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false">Processo<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="/SisCapV2/admin/addProcesso">Adicionar</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="/SisCapV2/admin/listProcesso">Listar</a></li>
					</ul></li>

				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false">Secretaria<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="/SisCapV2/admin/addSecretaria">Adicionar</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="/SisCapV2/admin/listSecretaria">Listar</a></li>
					</ul></li>

				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false">Tramita��o<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="/SisCapV2/tramitacao/addTramite">Adicionar</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="#">Listar</a></li>
					</ul></li>

				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false">Usu�rio<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="/SisCapV2/admin/addUser">Adicionar</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="/SisCapV2/admin/listUser">Listar</a></li>
					</ul></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false">Licitante Jur�dico<span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="#">Adicionar</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="#">Listar</a></li>
					</ul></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-haspopup="true"
					aria-expanded="false">Ajuda <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="#">Sair</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="#">Sobre</a></li>
					</ul></li>
			</ul>
		</div>
	</div>
	</nav>