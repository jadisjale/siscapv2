<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="home.jsp"%>

<div class="container">
	<h2>
		Cadastro de Tramitações
		<h2>
</div>
<hr>
<div class="container">

	<form class="form-horizontal">
		<input type="hidden" name="id" id="id" value="${id}">
		<div class="form-group form-processo">
			<label class="col-xs-2 control-label" for="inputSuccess">N° do Processo</label>
			<div class="col-xs-10">
				<input type="text" name="processo" id="inputProcesso"
					class="form-control" placeholder="Número do processo"
					value="${processo}"> <span id="borderProcess"></span> <span
					id="messageProcess"></span>
			</div>
		</div>
		<div class="form-group form-data">
			<label class="col-xs-2 control-label" for="inputSuccess">Data</label>
			<div class="col-xs-10">
				<input type="text" name="data" id="inputData"
					class="form-control" placeholder="Data do processo"
					value="${data}"> <span id="borderData"></span>
				<span id="messageData"></span>
			</div>
		</div>
		<div class="form-group form-destino">
			<label class="col-xs-2 control-label" for="inputSuccess">Destino</label>
			<div class="col-xs-10">
				<input type="destino" name="destino" id="inputDestino"
					class="form-control" placeholder="destino do processo"
					value="${destino}"> <span id="borderDestino"></span> <span
					id="messageDestino"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-xs-2 control-label" for="inputError">Secretaria</label>
			<div class="col-xs-10">
				<select class="form-control selectpicker show-tick"
					id="secretaria" name="secretaria">

					<c:if test="${funcao == 1}"> <option value="1" selected="selected">ADMIN</option> </c:if>
					<c:if test="${funcao != 1}"> <option value="1">ADMIN</option></c:if>
					<c:if test="${funcao == 2}"> <option value="2" selected="selected">MASTER</option> </c:if>
					<c:if test="${funcao != 2}"> <option value="2">MASTER</option></c:if>
					<c:if test="${funcao == 3}"> <option value="3" selected="selected">PREMIUN</option></c:if>
					<c:if test="${funcao != 3}"> <option value="3">PREMIUN</option></c:if>
					<c:if test="${funcao == 4}"> <option value="4" selected="selected">USUARIO</option> </c:if>
					<c:if test="${funcao != 4}"><option value="4">USUARIO</option></c:if>
					
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-offset-2 col-xs-10">
				<button type="submit" class="btn btn-primary" id="submitSaveTramite">Salvar</button>
			</div>
		</div>
	</form>
</div>
<hr>
<%@include file="footer.jsp"%>
<script type="text/javascript">
	$(document).ready(function() {
		adminTramite.index();
	});
</script>
