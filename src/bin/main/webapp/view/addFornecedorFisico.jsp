<%@include file="home.jsp"%>
<div class="container">
	<h2>Cadastro Fornecedor F�sico</h2>
</div>
<hr>
<div class="container">
	<form role="form" id="saveFormFornecedorFisico">

		<input type="hidden" name="id" value="${id}"></input>

		<div class="form-group form-nome-licitante">
			<label for="email">Nome Licitante:</label> <input type="text"
				class="form-control" id="nomeLicitante" required="required"
				name="nomeLicitante" value="${nomeLicitante}"> <span id="border-nome-licitante"></span> <span
				id="message-nome-licitante"></span>
		</div>
		<div class="form-group form-titular-licitante">
			<label for="pwd">Titular Licitante:</label> <input type="text"
				class="form-control" id="titularLicitante" required="required"
				name="titularLicitante" value="${titularLicitante}"><span id="border-titular-licitante"></span> <span
					id="message-titular-licitante"></span>
		</div>
		<div class="form-group form-telefone-licitante">
			<label for="pwd">Telefone Licitante:</label> <input type="text"
				class="form-control" id="telefoneLicitante" required="required"
				name="telefoneLicitante" value="${telefoneLicitante}"><span id="border-telefone-licitante"></span> <span
					id="message-telefone-licitante"></span>
		</div>
		<div class="form-group form-fax-licitante">
			<label for="pwd">Fax Licitante:</label> <input type="text"
				class="form-control" id="faxLicitante" required="required"
				name="faxLicitante" value="${faxLicitante}"><span id="border-fax-licitante"></span> <span
					id="message-fax-licitante"></span>
		</div>
		<div class="form-group form-email-licitante">
			<label for="pwd">Email Licitante:</label> <input type="text"
				class="form-control" id="emailLicitante" required="required"
				name="emailLicitante" value="${emailLicitante}"><span id="border-email-licitante"></span> <span
					id="message-email-licitante"></span>
		</div>
		<div class="form-group form-telefone-titular">
			<label for="pwd">Telefone Titular Licitante:</label> <input
				type="text" class="form-control" id="telefoneTitularLicitante"
				required="required" name="telefoneTitularLicitante" value="${telefoneTitularLicitante}"><span id="border-telefone-titular"></span> <span
					id="message-telefone-titular"></span>
		</div>
		<div class="form-group form-email-titular">
			<label for="pwd">Email Titular Licitante:</label> <input type="text"
				class="form-control" id="emailTitularLicitante" required="required"
				name="emailTitularLicitante" value="${emailTitularLicitante}"><span id="border-email-titular"></span> <span
					id="message-email-titular"></span>
		</div>
		<div class="form-group form-rua">
			<label for="pwd">Rua:</label> <input type="text" class="form-control"
				id="rua" required="required" name="rua" value="${rua}"><span id="border-rua"></span> <span
					id="message-rua"></span>
		</div>
		<div class="form-group form-bairro">
			<label for="pwd">Bairro:</label> <input type="text"
				class="form-control" id="bairro" required="required" name="bairro" value="${bairro}"><span id="border-bairro"></span> <span
					id="message-bairro"></span>
		</div>
		<div class="form-group form-cidade">
			<label for="pwd">Cidade:</label> <input type="text"
				class="form-control" id="cidade" required="required" name="cidade" value="${cidade}"><span id="border-cidade"></span> <span
					id="message-cidade"></span>
		</div>
		<div class="form-group form-estado">
			<label for="pwd">Estado:</label> <input type="text"
				class="form-control" id="estado" required="required" name="estado" value="${estado}"><span id="border-estado"></span> <span
					id="message-estado"></span>
		</div>
		<div class="form-group form-cep">
			<label for="pwd">CEP:</label> <input type="text" class="form-control"
				id="cep" required="required" name="cep" value="${cep}"><span id="border-cep"></span> <span
					id="message-cep"></span>
		</div>
		<div class="form-group form-cpf">
			<label for="pwd">CPF:</label> <input type="text" class="form-control"
				id="cpf" required="required" name="cpf" value="${cpf}"><span id="border-cpf"></span> <span
					id="message-cpf"></span>
		</div>
		<div class="form-group form-expedicaoForn">
			<label for="pwd">Expedi��o do Fornecedor:</label> <input type="text"
				class="form-control datepicker" id="expedicaoForn"
				required="required" name="expedicaoFornParam" value="${expedicaoForn}"><span
				id="border-expedicaoForn"></span> <span id="message-expedicaoForn"></span>
		</div>
		<div class="form-group form-numRegstroForn">
			<label for="pwd">N�mero Registro Fornecedor:</label> <input type="text" class="form-control"
				id="numRegstroForn" required="required" name="numRegstroForn" value="${numRegstroForn}"><span id="border-numRegstroForn"></span> <span
					id="message-numRegstroForn"></span>
		</div>
		<div class="form-group form-validadeForn">
			<label for="pwd">Validade do Fornecedor:</label> <input type="text" class="form-control datepicker"
				id="validadeForn" required="required" name="validadeFornParam" value="${validadeForn}"><span id="border-validadeForn"></span> <span
					id="message-validadeForn"></span>
		</div>
		<button type="submit" class="btn btn-primary"
			id="submitSavefisico">Salvar</button>
	</form>

</div>
<hr>
<%@include file="footer.jsp"%>
<script type="text/javascript">
	$(document).ready(function() {
		adminFornecedorFisico.index();
	});
</script>