<%@include file="home.jsp"%>
<div class="container">
	<h2>Cadastro Processo</h2>
</div>
<hr>
<div class="container">
	<form role="form" id="saveFormProcesso">

		<input type="hidden" name="id" value="${id}"></input>

		<div class="form-group form-inicioProcesso">
			<label for="email">In�cio Processo:</label> <input type="text"
				class="form-control datepicker" id="inicioProcesso"
				name="inicioProcessoParam"> <span id="border-inicioProcesso"></span>
			<span id="message-inicioProcesso"></span>
		</div>
		<div class="form-group form-numeroProcesso">
			<label for="pwd">N�mero Processo:</label> <input type="text"
				class="form-control" id="numeroProcesso" name="numeroProcesso">
			<span id="border-numeroProcesso"></span> <span
				id="message-numeroProcesso"></span>
		</div>
		<div class="form-group form-entradaProcesso">
			<label for="email">Entrada Processo:</label> <input type="text"
				class="form-control datepicker" id="entradaProcesso"
				name="entradaProcessoParam"> <span id="border-entradaProcesso"></span>
			<span id="message-entradaProcesso"></span>
		</div>
		<div class="form-group form-vlorEstimadoProc">
			<label for="pwd">Valor Estimado Processo:</label> <input type="text"
				class="form-control " id="vlorEstimadoProc" name="vlorEstimadoProcParam">
			<span id="border-vlorEstimadoProc"></span> <span
				id="message-vlorEstimadoProc"></span>
		</div>
		<div class="form-group form-numeroSolicProc">
			<label for="pwd">N�mero Solicita��o Processo:</label> <input
				type="text" class="form-control" id="numeroSolicProc"
				name="numeroSolicProc"> <span id="border-numeroSolicProc"></span>
			<span id="message-numeroSolicProc"></span>
		</div>
		<div class="form-group form-numModalProc">
			<label for="pwd">N�mero Modalidade Processo:</label> <input
				type="text" class="form-control" id="numModalProc"
				name="numModalProc"> <span id="border-numModalProc"></span>
			<span id="message-numModalProc"></span>
		</div>
		<div class="form-group form-modalidadeProc">
			<label for="pwd">Modalidade Processo:</label> <input type="text"
				class="form-control" id="modalidadeProc" name="modalidadeProc">
			<span id="border-modalidadeProc"></span> <span
				id="message-modalidadeProc"></span>
		</div>
		<div class="form-group form-tipoProc">
			<label for="pwd">Tipo Processo:</label> <input type="text"
				class="form-control" id="tipoProc" name="tipoProc"> <span
				id="border-tipoProc"></span> <span id="message-tipoProc"></span>
		</div>
		<div class="form-group form-origemRecProc">
			<label for="pwd">origemRecProc:</label> <input type="text"
				class="form-control" id="origemRecProc" name="origemRecProc">
			<span id="border-origemRecProc"></span> <span
				id="message-origemRecProc"></span>
		</div>
		<div class="form-group form-comissaoRespProc">
			<label for="pwd">Comiss�o Respons�vel Processo:</label> <input
				type="text" class="form-control" id="comissaoRespProc"
				name="comissaoRespProc"> <span id="border-comissaoRespProc"></span>
			<span id="message-comissaoRespProc"></span>
		</div>
		<div class="form-group form-objetoProc">
			<label for="pwd">Objeto Processo:</label> <input type="text"
				class="form-control" id="objetoProc" name="objetoProc"> <span
				id="border-objetoProc"></span> <span id="message-objetoProc"></span>
		</div>
		<div class="form-group form-situacaoProc">
			<label for="pwd">Situac�o Processo:</label> <input type="text"
				class="form-control" id="situacaoProc" name="situacaoProc">
			<span id="border-situacaoProc"></span> <span
				id="message-situacaoProc"></span>
		</div>
		<div class="form-group form-observacaoProc">
			<label for="pwd">Observa��o Processo:</label> <input type="text"
				class="form-control" id="observacaoProc" name="observacaoProc">
			<span id="border-observacaoProc"></span> <span
				id="message-observacaoProc"></span>
		</div>
		<div class="form-group form-dataContrato">
			<label for="email">Data Contrato:</label> <input type="text"
				class="form-control datepicker" id="dataContrato"
				name="dataContratoParam"> <span id="border-dataContrato"></span>
			<span id="message-dataContrato"></span>
		</div>
		
		
		<div class="form-group form-id1">
			<label for="email">Secretaria Remetente:</label>
			<select class="form-control selectField" id="idSecretariaUm" name="id1">	
			
			</select>
			<span id="border-id1"></span> 
			<span id="message-id1"></span>
		</div>
		
		<div class="form-group form-id2">
			<label for="email">Secretaria Destinat�rio:</label>
			<select id="idSecretariaDois" class="form-control selectField" name="id2">	
				
			</select>
			<span id="border-id2"></span> 
			<span id="message-id2"></span>
		</div>

		<button type="submit" class="btn btn-primary"
			id="submitSaveProcesso">Salvar</button>
	</form>

</div>
<hr>
<%@include file="footer.jsp"%>
<script type="text/javascript">
	$(document).ready(function() {
		adminProcesso.index();
	});
</script>
