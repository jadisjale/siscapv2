package br.com.siscap.Jdbc;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.siscap.util.HibernateUtil;

public class Session_Factory {

    private static Session session;
    private static Transaction trans;

    public static void openSession() {
        try {
            Session_Factory.session = HibernateUtil.getSessionFactory().openSession();
            Session_Factory.trans = Session_Factory.session.beginTransaction();

        } catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    public static void close() {
        if (Session_Factory.session != null && Session_Factory.session.isOpen()) {
            // HibernateUtil.getSessionFactory().close(); //fecha a fabrica
            // completa

            Session_Factory.session().close();// feicha somente a conecção

        } else {
            System.out.println("fechar");
        }
    }

    public static Session session() {
        return session;
    }

    public static Transaction transaction() {
        return trans;
    }

}
