package br.com.siscap.controller;


import java.math.BigDecimal;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import br.com.siscap.Jdbc.Session_Factory;
import br.com.siscap.dao.LicitanteJuridicoDAO;
import br.com.siscap.dao.ProcessoDAO;
import br.com.siscap.model.Fornecedor;
import br.com.siscap.model.LicitanteJuridico;
import br.com.siscap.model.Processo;
import br.com.siscap.model.Secretaria;
import br.com.siscap.model.Usuario;
import br.com.siscap.util.ResponseJson;

@Controller
@RequestMapping("/processo")
public class ProcessoController extends ControllerService {
	
	Gson gson = new Gson();
	ResponseJson json;
	ProcessoDAO dao;
	
	@RequestMapping(value = "/addProcesso", method = RequestMethod.GET)
	public ModelAndView addProcesso(ModelMap model) {
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		ModelAndView modelAndView = new ModelAndView("addProcesso");
		return modelAndView;
	}
	
	@RequestMapping(value = "/saveProcesso", method = RequestMethod.POST)
	public @ResponseBody String saveProcesso(@RequestParam("id1") Long id1, @RequestParam("id2") Long id2,
			@RequestParam("inicioProcessoParam") String inicioProcessoParam,
			@RequestParam("entradaProcessoParam") String entradaProcessoParam,
			@RequestParam("dataContratoParam") String dataContratoParam,
			@RequestParam("vlorEstimadoProcParam") String vlorEstimadoProcParam, Processo processo,
			@RequestParam("idFornecedor") Long idFornecedor)
					throws ParseException {

		Secretaria secretaria1 = new Secretaria();
		secretaria1.setId(id1);
		Secretaria secretaria2 = new Secretaria();
		secretaria2.setId(id2);
		Fornecedor fornecedor = new Fornecedor();
		fornecedor.setId(idFornecedor);

		processo.setSecretaria(secretaria1);
		processo.setSecretaria2(secretaria2);
		processo.setFornecedor(fornecedor);

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date inicio = new Date(format.parse(inicioProcessoParam).getTime());
		Date entrada = new Date(format.parse(entradaProcessoParam).getTime());
		Date contrato = new Date(format.parse(dataContratoParam).getTime());

		BigDecimal payment = new BigDecimal(vlorEstimadoProcParam);
		processo.setVlorEstimadoProc(payment);

		processo.setInicioProcesso(inicio);
		processo.setEntradaProcesso(entrada);
		processo.setDataContrato(contrato);

		dao = new ProcessoDAO();
		
		try {
			dao.merge(processo);
			if(processo.getId() != null ){
				json = new ResponseJson("2", "Processo " + processo.getNumeroProcesso() + ", alterado com sucesso");
			} else {
				json = new ResponseJson("1", "Processo " + processo.getNumeroProcesso() + ", inserido com sucesso");
			} 
			return gson.toJson(json);
		} catch (Exception e) {
			json = new ResponseJson("0", "Erro ao inserir processo");
			return gson.toJson(json);
		}
	}

	@RequestMapping(value = "/getProcesso", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Processo> getProcesso() {
		Session_Factory.openSession();
		Criteria criteria = Session_Factory.session().createCriteria(Processo.class)
				.setProjection(Projections.projectionList().add(Projections.property("id"), "id")
						.add(Projections.property("numeroProcesso"), "numeroProcesso")
						.add(Projections.property("secretaria"), "secretaria")
						.add(Projections.property("secretaria2"), "secretaria2"))
				.setResultTransformer(Transformers.aliasToBean(Processo.class));

		return (ArrayList<Processo>) criteria.list();
	}

	@RequestMapping(value = "/listProcesso", method = RequestMethod.GET)
	public ModelAndView listProcesso(ModelMap model) {
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		ModelAndView modelAndView = new ModelAndView("listProcesso");
		return modelAndView;
	}

	@RequestMapping(value = "/getProcesso/remove/{id}", method = RequestMethod.POST)
	public String processoByid(@PathVariable Long id) {
		ProcessoDAO dao = new ProcessoDAO();
		Processo processo = dao.buscarId(id);
		try {
			dao.merge(processo);
			if(processo.getId() != null ){
				json = new ResponseJson("1", "Processo " + processo.getNumeroProcesso() + ", exclu�do com sucesso");
			}
			return gson.toJson(json);
		} catch (Exception e) {
			json = new ResponseJson("0", "Erro ao excluir processo");
			return gson.toJson(json);
		}
	}
	
	@RequestMapping(value = "/getProcessoIdNumero", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Processo> getProcessoIdName() {
		Session_Factory.openSession();
		Criteria criteria = Session_Factory.session().createCriteria(Processo.class)
				.setProjection(Projections.projectionList().add(Projections.property("id"), "id")
						.add(Projections.property("numeroProcesso"), "numeroProcesso"))
				.setResultTransformer(Transformers.aliasToBean(Processo.class));
		return (ArrayList<Processo>) criteria.list();
	}
	
	
	@RequestMapping(value = "/getProcessoNumId", method = RequestMethod.POST)
	public @ResponseBody ArrayList<Processo> getProcessoNumId() {
		Session_Factory.openSession();
		Criteria criteria = Session_Factory.session().createCriteria(Processo.class)
				.setProjection(Projections.projectionList().add(Projections.property("id"), "id")
						.add(Projections.property("numeroProcesso"), "numeroProcesso"))
				.setResultTransformer(Transformers.aliasToBean(Processo.class));
		return (ArrayList<Processo>) criteria.list();
	}

	@RequestMapping(value = "/getProcesso/{id}", method = RequestMethod.GET)
	public ModelAndView getProcesso(@PathVariable Long id, ModelMap model) throws ParseException {

		dao = new ProcessoDAO();
		Processo processo = dao.buscarId(id);
		
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());

		model.addAttribute("id", processo.getId());	
		model.addAttribute("inicioProcessoParam", processo.getDataFormatada(processo.getInicioProcesso()));
		model.addAttribute("numeroProcesso", processo.getNumeroProcesso());
		model.addAttribute("entradaProcessoParam", processo.getDataFormatada(processo.getEntradaProcesso()));
		model.addAttribute("vlorEstimadoProcParam", processo.getVlorEstimadoProc());
		model.addAttribute("numeroSolicProc", processo.getNumeroSolicProc());
		model.addAttribute("numModalProc", processo.getNumModalProc());
		model.addAttribute("modalidadeProc", processo.getModalidadeProc());
		model.addAttribute("tipoProc", processo.getTipoProc());
		model.addAttribute("origemRecProc", processo.getOrigemRecProc());
		model.addAttribute("comissaoRespProc", processo.getComissaoRespProc());
		model.addAttribute("objetoProc", processo.getObjetoProc());
		model.addAttribute("situacaoProc", processo.getSituacaoProc());
		model.addAttribute("observacaoProc", processo.getObservacaoProc());
		model.addAttribute("dataContratoParam", processo.getDataFormatada(processo.getDataContrato()));
		model.addAttribute("secretaria1", processo.getSecretaria().getId());
		model.addAttribute("secretaria2", processo.getSecretaria().getId());
		model.addAttribute("idFornecedor", processo.getFornecedor().getId());
		ModelAndView modelAndView = new ModelAndView("addProcesso");
		return modelAndView;
	}
	
}
