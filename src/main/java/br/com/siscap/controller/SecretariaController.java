package br.com.siscap.controller;

import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.transform.Transformers;
import org.jboss.logging.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import br.com.siscap.Jdbc.Session_Factory;
import br.com.siscap.dao.LicitanteJuridicoDAO;
import br.com.siscap.dao.OrgaoLicitanteDAO;
import br.com.siscap.dao.SecretariaDAO;
import br.com.siscap.dao.UsuarioDAO;
import br.com.siscap.model.Fornecedor;
import br.com.siscap.model.LicitanteJuridico;
import br.com.siscap.model.OrgaoLicitante;
import br.com.siscap.model.Secretaria;
import br.com.siscap.model.Usuario;
import br.com.siscap.util.ResponseJson;

@Controller
@RequestMapping("/secretaria")
public class SecretariaController extends ControllerService {
	
	Gson gson = new Gson();
	ResponseJson json;
	SecretariaDAO dao;
	
	@RequestMapping(value = "/addSecretaria")
	public ModelAndView addSecretaria(ModelMap model) {
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		ModelAndView modelAndView = new ModelAndView("addSecretaria");
		return modelAndView;
	}
	
	@RequestMapping(value = "/saveSecretaria", method = RequestMethod.POST)
	public @ResponseBody String saveSecretaria(OrgaoLicitante orgaoLicitante,
			LicitanteJuridico licitanteJuridico, 
			Secretaria secretaria, @RequestParam("idSecretaria") Long idSecretaria,
			@RequestParam("idLicitanteJuridico") Long idLicitanteJuridico, @RequestParam("idOrgaoLicitante") Long idOrgaoLicitante){
		try {
			if(idOrgaoLicitante != null){
				orgaoLicitante.setId(idOrgaoLicitante);
			}
			
			if(idLicitanteJuridico != null){
				licitanteJuridico.setId(idLicitanteJuridico);
			}
		
			if(idSecretaria != null){
				secretaria.setId(idSecretaria);
			}
			
			dao = new SecretariaDAO();
			licitanteJuridico.setOrgaoLicitante(orgaoLicitante);
			secretaria.setLicitanteJuridico(licitanteJuridico);
			dao.merge(secretaria);
			if(secretaria.getId() != null ){
				json = new ResponseJson("2", "Org�o alterado com sucesso");
			} else {
				json = new ResponseJson("1", "Org�o salvo com sucesso");
			} 
			return gson.toJson(json);
		} catch (Exception e) {
			json = new ResponseJson("0", "Erro ao inserir secretaria");
			return gson.toJson(json);
		}
	}
	
	@RequestMapping(value = "/getSecretaria/{id}", method = RequestMethod.GET)
	public ModelAndView getSecretariaById(@PathVariable Long id, ModelMap model) {
		dao = new SecretariaDAO();
		Secretaria secretaria = dao.buscarId(id);
		
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		
		model.addAttribute("idSecretaria", secretaria.getId());
		model.addAttribute("idLicitanteJuridico", secretaria.getLicitanteJuridico().getId());
		model.addAttribute("idOrgaoLicitante", secretaria.getLicitanteJuridico().getOrgaoLicitante().getId());
		
			
		model.addAttribute("gerenteSec", secretaria.getGerenteSec());
		model.addAttribute("celGerenteSec", secretaria.getCelGerenteSec());
		model.addAttribute("emailGerenteSec", secretaria.getEmailGerenteSec());
		model.addAttribute("telGerenteSec", secretaria.getTelGerenteSec());
		
		model.addAttribute("nomeLicitante", secretaria.getLicitanteJuridico().getOrgaoLicitante().getNomeLicitante());
		model.addAttribute("cnpj", secretaria.getLicitanteJuridico().getCnpj());
		model.addAttribute("titularLicitante", secretaria.getLicitanteJuridico().getOrgaoLicitante().getTitularLicitante());
		model.addAttribute("emailTitular", secretaria.getLicitanteJuridico().getOrgaoLicitante().getEmailLicitante());
		model.addAttribute("telefoneLicitante", secretaria.getLicitanteJuridico().getOrgaoLicitante().getTelefoneLicitante());
		
		model.addAttribute("faxLicitante", secretaria.getLicitanteJuridico().getOrgaoLicitante().getFaxLicitante());
		model.addAttribute("emailLicitante", secretaria.getLicitanteJuridico().getOrgaoLicitante().getEmailLicitante());
		model.addAttribute("telefoneTitularLicitante", secretaria.getLicitanteJuridico().getOrgaoLicitante().getTelefoneTitularLicitante());
		model.addAttribute("emailTitularLicitante", secretaria.getLicitanteJuridico().getOrgaoLicitante().getEmailTitularLicitante());
		
		model.addAttribute("cep", secretaria.getLicitanteJuridico().getOrgaoLicitante().getCep());
		model.addAttribute("bairro", secretaria.getLicitanteJuridico().getOrgaoLicitante().getBairro());
		model.addAttribute("cidade", secretaria.getLicitanteJuridico().getOrgaoLicitante().getCidade());
		model.addAttribute("estado", secretaria.getLicitanteJuridico().getOrgaoLicitante().getEstado());
		model.addAttribute("rua", secretaria.getLicitanteJuridico().getOrgaoLicitante().getRua());
		
		
		ModelAndView modelAndView = new ModelAndView("addSecretaria");
		return modelAndView;
	}

	@RequestMapping(value = "/listSecretaria")
	public ModelAndView listSecretaria(ModelMap model) {
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		ModelAndView modelAndView = new ModelAndView("listSecretaria");
		return modelAndView;
	}

	@RequestMapping(value = "/dtlSecretaria")
	public ModelAndView dtlSecretaria(ModelMap model) {
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		ModelAndView modelAndView = new ModelAndView("dtlSecretaria");
		return modelAndView;
	}

	@RequestMapping(value = "/getSecretarias", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Secretaria> getSecretarias() {
		Session_Factory.openSession();
		Criteria criteria = Session_Factory.session().createCriteria(Secretaria.class)
				.setProjection(Projections.projectionList().add(Projections.property("id"), "id")
						.add(Projections.property("gerenteSec"), "gerenteSec")
						.add(Projections.property("emailGerenteSec"), "emailGerenteSec")
						.add(Projections.property("licitanteJuridico"), "licitanteJuridico"))
				.setResultTransformer(Transformers.aliasToBean(Secretaria.class));

		return (ArrayList<Secretaria>) criteria.list();
	}

	@RequestMapping(value = "/secretaria/remove/{id}", method = RequestMethod.POST)
	public @ResponseBody String removeByidSecretaria(@PathVariable Long id) {
		
		dao = new SecretariaDAO();
		Secretaria secretaria = dao.buscarId(id);
		
		try {
			dao.remove(secretaria);
			json = new ResponseJson("1", "Org�o exclu�do com sucesso.");
			return gson.toJson(json);
		} catch (Exception e) {
			json = new ResponseJson("0", "Erro ao deletar org�o");
			return gson.toJson(json);
		}
		
	}

	@RequestMapping(value = "/detalheSecretaria/{id}", method = RequestMethod.GET)
	public ModelAndView dtlSecretaria(@PathVariable Long id, ModelMap model) {
		dao = new SecretariaDAO();
		Secretaria secretaria = dao.buscarId(id);
		
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		
		model.addAttribute("gerenteSec", secretaria.getGerenteSec());
		model.addAttribute("celGerenteSec", secretaria.getCelGerenteSec());
		model.addAttribute("emailGerenteSec", secretaria.getEmailGerenteSec());
		model.addAttribute("telGerenteSec", secretaria.getTelGerenteSec());
		
		model.addAttribute("nomeLicitante", secretaria.getLicitanteJuridico().getOrgaoLicitante().getNomeLicitante());
		model.addAttribute("cnpj", secretaria.getLicitanteJuridico().getCnpj());
		model.addAttribute("titular", secretaria.getLicitanteJuridico().getOrgaoLicitante().getTitularLicitante());
		model.addAttribute("emailTitular", secretaria.getLicitanteJuridico().getOrgaoLicitante().getEmailLicitante());
		model.addAttribute("fax", secretaria.getLicitanteJuridico().getOrgaoLicitante().getFaxLicitante());
		
		model.addAttribute("cep", secretaria.getLicitanteJuridico().getOrgaoLicitante().getCep());
		model.addAttribute("rua", secretaria.getLicitanteJuridico().getOrgaoLicitante().getRua());
		model.addAttribute("bairro", secretaria.getLicitanteJuridico().getOrgaoLicitante().getBairro());
		model.addAttribute("cidade", secretaria.getLicitanteJuridico().getOrgaoLicitante().getCidade());
		model.addAttribute("estado", secretaria.getLicitanteJuridico().getOrgaoLicitante().getEstado());

		ModelAndView modelAndView = new ModelAndView("dtlSecretaria");
		return modelAndView;
	}
	
	@RequestMapping(value = "/getSecretariasIdName", method = RequestMethod.POST)
	public @ResponseBody ArrayList<Secretaria> getSecretariasIdName() {
		Session_Factory.openSession();
		Criteria criteria = Session_Factory.session().createCriteria(Secretaria.class)
				.setProjection(Projections.projectionList().add(Projections.property("id"), "id")
						.add(Projections.property("licitanteJuridico"), "licitanteJuridico"))
				.setResultTransformer(Transformers.aliasToBean(Secretaria.class));
		return (ArrayList<Secretaria>) criteria.list();
	}
	
	@RequestMapping(value = "/getProcessoSecretaria", method = RequestMethod.POST)
	public @ResponseBody ArrayList<Secretaria> getProcessoSecretaria() {
		Session_Factory.openSession();
		Criteria criteria = Session_Factory.session().createCriteria(Secretaria.class);
		return (ArrayList<Secretaria>) criteria.list();
	}
	
	@RequestMapping(value = "/getFornecedores", method = RequestMethod.POST)
	public @ResponseBody ArrayList<Fornecedor> getFornecedores() {
		Session_Factory.openSession();
		Criteria criteria = Session_Factory.session().createCriteria(Fornecedor.class);
		return (ArrayList<Fornecedor>) criteria.list();
	}

}
