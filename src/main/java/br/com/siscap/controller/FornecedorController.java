package br.com.siscap.controller;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import br.com.siscap.Jdbc.Session_Factory;
import br.com.siscap.dao.FornecedorDAO;
import br.com.siscap.model.Fornecedor;
import br.com.siscap.model.LicitanteFisico;
import br.com.siscap.model.LicitanteJuridico;
import br.com.siscap.model.OrgaoLicitante;
import br.com.siscap.model.Usuario;
import br.com.siscap.util.ResponseJson;

@Controller
@RequestMapping("/fornecedor")
public class FornecedorController extends ControllerService {
	
	Gson gson = new Gson();
	ResponseJson json;
	FornecedorDAO dao;
	
	@RequestMapping(value = "/addFornecedorFisico", method = RequestMethod.GET)
	public ModelAndView addFornecedorFisico(ModelMap model) {
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		ModelAndView modelAndView = new ModelAndView("addFornecedorFisico");
		return modelAndView;
	}

	@RequestMapping(value = "/saveFornecedorFisico", method = RequestMethod.POST)
	public @ResponseBody String saveProcesso(OrgaoLicitante orgaoLicitante, LicitanteFisico licitanteFisico,
			Fornecedor fornecedor, @RequestParam("expedicaoFornParam") String expedicaoFornParam,
			@RequestParam("validadeFornParam") String validadeFornParam, @RequestParam("id") Long id,
			@RequestParam("idJuridico") Long idJuridico, @RequestParam("idForm") Long idForn
			) throws ParseException {

		licitanteFisico.setOrgaoLicitante(orgaoLicitante);
		
		if (id != null) {
			orgaoLicitante.setId(id);
		}

		if (idJuridico != null) {
			licitanteFisico.setId(idJuridico);
		}

		if (idForn != null) {
			fornecedor.setId(idForn);
		}

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date expedicaoForn = new Date(format.parse(expedicaoFornParam).getTime());
		Date validadeForn = new Date(format.parse(validadeFornParam).getTime());

		fornecedor.setExpedicaoForn(expedicaoForn);
		fornecedor.setValidadeForn(validadeForn);
		fornecedor.setLicitanteFisico(licitanteFisico);
		
		try {
			dao = new FornecedorDAO();
			dao.merge(fornecedor);
			if(fornecedor.getId() != null ){
				json = new ResponseJson("1", "Fornecedor alterado com sucesso");
			} else {
				json = new ResponseJson("1", "Fornecedor cadastrado com sucesso");
			} 
			return gson.toJson(json);
		} catch (Exception e) {
			json = new ResponseJson("0", "Erro ao inserir fornecedor");
			return gson.toJson(json);
		}
		
	}

	@RequestMapping(value = "/getNomeFornecedor", method = RequestMethod.POST)
	public @ResponseBody ArrayList<Fornecedor> getNomeFornecedor() {
		Session_Factory.openSession();
		Criteria criteria = Session_Factory.session().createCriteria(Fornecedor.class);
		return (ArrayList<Fornecedor>) criteria.list();
	}
	
	@RequestMapping(value = "/addFornecedorJuridico")
	public ModelAndView addlicitanteJuridico(ModelMap model) {
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		ModelAndView modelAndView = new ModelAndView("addFornecedorJuridico");
		return modelAndView;
	}

	@RequestMapping(value = "/saveFornecedorJuridico", method = RequestMethod.POST)
	public @ResponseBody String saveJuridico(OrgaoLicitante orgaoLicitante, LicitanteJuridico licitanteJuridico,
			Fornecedor fornecedor, @RequestParam("expedicaoFornParam") String expedicaoFornParam,
			@RequestParam("validadeFornParam") String validadeFornParam, @RequestParam("id") Long id,
			@RequestParam("idJuridico") Long idJuridico, @RequestParam("idForn") Long idForn) throws ParseException {

		if (id != null) {
			orgaoLicitante.setId(id);
		}

		if (idJuridico != null) {
			licitanteJuridico.setId(idJuridico);
		}

		if (idForn != null) {
			fornecedor.setId(idForn);
		}

		licitanteJuridico.setOrgaoLicitante(orgaoLicitante);

		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date expedicaoForn = new Date(format.parse(expedicaoFornParam).getTime());
		Date validadeForn = new Date(format.parse(validadeFornParam).getTime());

		fornecedor.setExpedicaoForn(expedicaoForn);
		fornecedor.setValidadeForn(validadeForn);
		fornecedor.setLicitanteJuridico(licitanteJuridico);
		
		try {
			dao = new FornecedorDAO();
			dao.merge(fornecedor);
			if(fornecedor.getId() != null ){
				json = new ResponseJson("2", "Fornecedor alterado com sucesso");
			} else {
				json = new ResponseJson("1", "Fornecedor cadastrado com sucesso");
			} 
			return gson.toJson(json);
		} catch (Exception e) {
			json = new ResponseJson("0", "Erro ao inserir fornecedor");
			return gson.toJson(json);
		}

	}
	
	@RequestMapping(value = "/listLicitanteJuridico")
	public ModelAndView listLicitanteJuridico(ModelMap model) {
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		ModelAndView modelAndView = new ModelAndView("listLicitanteJuridico");
		return modelAndView;
	}


}
