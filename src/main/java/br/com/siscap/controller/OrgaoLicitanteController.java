package br.com.siscap.controller;

import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import br.com.siscap.Jdbc.Session_Factory;
import br.com.siscap.dao.LicitanteFisicoDAO;
import br.com.siscap.dao.LicitanteJuridicoDAO;
import br.com.siscap.dao.OrgaoLicitanteDAO;
import br.com.siscap.dao.UsuarioDAO;
import br.com.siscap.model.Fornecedor;
import br.com.siscap.model.LicitanteFisico;
import br.com.siscap.model.LicitanteJuridico;
import br.com.siscap.model.OrgaoLicitante;
import br.com.siscap.model.Usuario;
import br.com.siscap.util.ResponseJson;

@Controller
@RequestMapping("/orgaolicitante")
public class OrgaoLicitanteController extends ControllerService {
	
	Gson gson = new Gson();
	ResponseJson json;
	OrgaoLicitanteDAO dao;
	
	@RequestMapping(value = "/getJuridicos", method = RequestMethod.GET)
	public @ResponseBody ArrayList<LicitanteJuridico> getJuridicos() {		
		Session_Factory.openSession();
		Criteria consulta = Session_Factory.session().createCriteria(LicitanteJuridico.class);
		return (ArrayList<LicitanteJuridico>) consulta.list();
	}

	@RequestMapping(value = "/getJuridico/remove/{id}", method = RequestMethod.POST)
	public @ResponseBody String juridicoByid(@PathVariable Long id) {
		dao = new OrgaoLicitanteDAO();
		OrgaoLicitante orgaoLicitante = dao.buscarId(id);
		try {
			dao.remove(orgaoLicitante);
			json = new ResponseJson("1", "Org�o licitante " + orgaoLicitante.getNomeLicitante() + ", exclu�do com sucesso!");
		} catch (Exception e) {
			json = new ResponseJson("0", "Erro ao excluir org�o licitante " + orgaoLicitante.getNomeLicitante());
		}
		return gson.toJson(json);
	}

	@RequestMapping(value = "/getLicitanteJuridico/{id}", method = RequestMethod.GET)
	public ModelAndView getLicitanteJuridico(@PathVariable Long id, ModelMap model) {

		LicitanteJuridicoDAO dao = new LicitanteJuridicoDAO();
		LicitanteJuridico licitanteJuridico = dao.buscarId(id);
		
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());

		model.addAttribute("id", licitanteJuridico.getOrgaoLicitante().getId());
		model.addAttribute("nomeLicitante", licitanteJuridico.getOrgaoLicitante().getNomeLicitante());
		model.addAttribute("titularLicitante", licitanteJuridico.getOrgaoLicitante().getTitularLicitante());
		model.addAttribute("telefoneLicitante", licitanteJuridico.getOrgaoLicitante().getTelefoneLicitante());
		model.addAttribute("faxLicitante", licitanteJuridico.getOrgaoLicitante().getFaxLicitante());
		model.addAttribute("emailLicitante", licitanteJuridico.getOrgaoLicitante().getEmailLicitante());
		model.addAttribute("telefoneTitularLicitante", licitanteJuridico.getOrgaoLicitante().getTelefoneLicitante());
		model.addAttribute("emailTitularLicitante", licitanteJuridico.getOrgaoLicitante().getEmailTitularLicitante());
		model.addAttribute("rua", licitanteJuridico.getOrgaoLicitante().getRua());
		model.addAttribute("bairro", licitanteJuridico.getOrgaoLicitante().getBairro());
		model.addAttribute("cidade", licitanteJuridico.getOrgaoLicitante().getCidade());
		model.addAttribute("estado", licitanteJuridico.getOrgaoLicitante().getEstado());
		model.addAttribute("cep", licitanteJuridico.getOrgaoLicitante().getCep());

		model.addAttribute("idJuridico", licitanteJuridico.getId());
		model.addAttribute("cnpj", licitanteJuridico.getCnpj());

		Fornecedor fornecedor;
		try {
			Session_Factory.openSession();
			Criteria consulta = Session_Factory.session().createCriteria(Fornecedor.class);
			consulta.add(Restrictions.eq("licitanteJuridico.id", licitanteJuridico.getId()));
			fornecedor = (Fornecedor) consulta.uniqueResult();
		} catch (RuntimeException e) {
			throw e;
		} finally {
			Session_Factory.close();
		}

		model.addAttribute("idForm", fornecedor.getId());
		model.addAttribute("expedicaoForn", fornecedor.getDataFormatada(fornecedor.getExpedicaoForn()));
		model.addAttribute("numRegstroForn", fornecedor.getNumRegstroForn());
		model.addAttribute("validadeForn", fornecedor.getDataFormatada(fornecedor.getValidadeForn()));

		ModelAndView modelAndView = new ModelAndView("addFornecedorJuridico");
		return modelAndView;
	}

	@RequestMapping(value = "/getJuridicosIdName", method = RequestMethod.POST)
	public @ResponseBody ArrayList<LicitanteJuridico> getJuridicosIdName() {
	
		Session_Factory.openSession();
		String hql = "select licitanteJuridico.id from Fornecedor where licitanteJuridico.id is not null";
		Query query = Session_Factory.session().createQuery(hql);
		ArrayList<Long> ids = (ArrayList<Long>) query.list();
		Criteria criteria = Session_Factory.session().createCriteria(LicitanteJuridico.class)
				.setProjection(Projections.projectionList().add(Projections.property("id"), "id")
						.add(Projections.property("orgaoLicitante"), "orgaoLicitante"))
				.add(Restrictions.in("id", ids))
				.setResultTransformer(Transformers.aliasToBean(LicitanteJuridico.class));
		return (ArrayList<LicitanteJuridico>) criteria.list();
	}
	
	@RequestMapping(value = "/dtlLicitanteJuridico/{id}", method = RequestMethod.GET)
	public ModelAndView dtlLicitanteJuridico(@PathVariable Long id, ModelMap model) {

		LicitanteJuridicoDAO dao = new LicitanteJuridicoDAO();
		LicitanteJuridico licitanteJuridico = dao.buscarId(id);
		
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());

		model.addAttribute("id", licitanteJuridico.getOrgaoLicitante().getId());
		model.addAttribute("nomeLicitante", licitanteJuridico.getOrgaoLicitante().getNomeLicitante());
		model.addAttribute("titularLicitante", licitanteJuridico.getOrgaoLicitante().getTitularLicitante());
		model.addAttribute("telefoneLicitante", licitanteJuridico.getOrgaoLicitante().getTelefoneLicitante());
		model.addAttribute("faxLicitante", licitanteJuridico.getOrgaoLicitante().getFaxLicitante());
		model.addAttribute("emailLicitante", licitanteJuridico.getOrgaoLicitante().getEmailLicitante());
		model.addAttribute("telefoneTitularLicitante", licitanteJuridico.getOrgaoLicitante().getTelefoneLicitante());
		model.addAttribute("emailTitularLicitante", licitanteJuridico.getOrgaoLicitante().getEmailTitularLicitante());
		model.addAttribute("rua", licitanteJuridico.getOrgaoLicitante().getRua());
		model.addAttribute("bairro", licitanteJuridico.getOrgaoLicitante().getBairro());
		model.addAttribute("cidade", licitanteJuridico.getOrgaoLicitante().getCidade());
		model.addAttribute("estado", licitanteJuridico.getOrgaoLicitante().getEstado());
		model.addAttribute("cep", licitanteJuridico.getOrgaoLicitante().getCep());
		
		model.addAttribute("idJuridico", licitanteJuridico.getId());
		model.addAttribute("cnpj", licitanteJuridico.getCnpj());

		Fornecedor fornecedor;
		try {
			Session_Factory.openSession();
			Criteria consulta = Session_Factory.session().createCriteria(Fornecedor.class);
			consulta.add(Restrictions.eq("licitanteJuridico.id", licitanteJuridico.getId()));
			fornecedor = (Fornecedor) consulta.uniqueResult();
		} catch (RuntimeException e) {
			throw e;
		} finally {
			Session_Factory.close();
		}

		model.addAttribute("idForm", fornecedor.getId());
		model.addAttribute("expedicaoForn", fornecedor.getDataFormatada(fornecedor.getExpedicaoForn()));
		model.addAttribute("numRegstroForn", fornecedor.getNumRegstroForn());
		model.addAttribute("validadeForn", fornecedor.getDataFormatada(fornecedor.getValidadeForn()));
		ModelAndView modelAndView = new ModelAndView("dtlJuridico");
		return modelAndView;
	}
	
	@RequestMapping(value = "/listLicitanteFisico", method = RequestMethod.GET)
	public @ResponseBody ArrayList<LicitanteFisico> getListLicitante (){
		Session_Factory.openSession();
		Criteria criteria = Session_Factory.session().createCriteria(LicitanteFisico.class);
		return (ArrayList<LicitanteFisico>) criteria.list();
	}
	
	@RequestMapping(value = "/listFisicos", method = RequestMethod.GET)
	public ModelAndView listFisicos(ModelMap model) {
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		ModelAndView modelAndView = new ModelAndView("listLicitanteFisico");
		return modelAndView;
	}
	
	@RequestMapping(value = "/getLicitanteFisico/{id}", method = RequestMethod.GET)
	public ModelAndView getLicitanteFisico(@PathVariable Long id, ModelMap model) {

		LicitanteFisicoDAO dao = new LicitanteFisicoDAO();
		LicitanteFisico licitanteFisico = dao.buscarId(id);
		
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());

		model.addAttribute("id", licitanteFisico.getOrgaoLicitante().getId());
		model.addAttribute("nomeLicitante", licitanteFisico.getOrgaoLicitante().getNomeLicitante());
		model.addAttribute("titularLicitante", licitanteFisico.getOrgaoLicitante().getTitularLicitante());
		model.addAttribute("telefoneLicitante", licitanteFisico.getOrgaoLicitante().getTelefoneLicitante());
		model.addAttribute("faxLicitante", licitanteFisico.getOrgaoLicitante().getFaxLicitante());
		model.addAttribute("emailLicitante", licitanteFisico.getOrgaoLicitante().getEmailLicitante());
		model.addAttribute("telefoneTitularLicitante", licitanteFisico.getOrgaoLicitante().getTelefoneLicitante());
		model.addAttribute("emailTitularLicitante", licitanteFisico.getOrgaoLicitante().getEmailTitularLicitante());
		model.addAttribute("rua", licitanteFisico.getOrgaoLicitante().getRua());
		model.addAttribute("bairro", licitanteFisico.getOrgaoLicitante().getBairro());
		model.addAttribute("cidade", licitanteFisico.getOrgaoLicitante().getCidade());
		model.addAttribute("estado", licitanteFisico.getOrgaoLicitante().getEstado());
		model.addAttribute("cep", licitanteFisico.getOrgaoLicitante().getCep());
		model.addAttribute("idJuridico", licitanteFisico.getId());
		model.addAttribute("cpf", licitanteFisico.getCpf());

		Fornecedor fornecedor;
		try {
			Session_Factory.openSession();
			Criteria consulta = Session_Factory.session().createCriteria(Fornecedor.class);
			consulta.add(Restrictions.eq("licitanteFisico.id", id));
			fornecedor = (Fornecedor) consulta.uniqueResult();
		} catch (RuntimeException e) {
			throw e;
		} finally {
			Session_Factory.close();
		}

		model.addAttribute("idForm", fornecedor.getId());
		model.addAttribute("expedicaoForn", fornecedor.getDataFormatada(fornecedor.getExpedicaoForn()));
		model.addAttribute("numRegstroForn", fornecedor.getNumRegstroForn());
		model.addAttribute("validadeForn", fornecedor.getDataFormatada(fornecedor.getValidadeForn()));

		ModelAndView modelAndView = new ModelAndView("addFornecedorFisico");
		return modelAndView;
	}
	
	@RequestMapping(value = "/dtlLicitanteFisico/{id}", method = RequestMethod.GET)
	public ModelAndView dtlLicitanteFisico(@PathVariable Long id, ModelMap model) {

		LicitanteFisicoDAO dao = new LicitanteFisicoDAO();
		LicitanteFisico licitanteFisico = dao.buscarId(id);
		
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());

		model.addAttribute("id", licitanteFisico.getOrgaoLicitante().getId());
		model.addAttribute("nomeLicitante", licitanteFisico.getOrgaoLicitante().getNomeLicitante());
		model.addAttribute("titularLicitante", licitanteFisico.getOrgaoLicitante().getTitularLicitante());
		model.addAttribute("telefoneLicitante", licitanteFisico.getOrgaoLicitante().getTelefoneLicitante());
		model.addAttribute("faxLicitante", licitanteFisico.getOrgaoLicitante().getFaxLicitante());
		model.addAttribute("emailLicitante", licitanteFisico.getOrgaoLicitante().getEmailLicitante());
		model.addAttribute("telefoneTitularLicitante", licitanteFisico.getOrgaoLicitante().getTelefoneLicitante());
		model.addAttribute("emailTitularLicitante", licitanteFisico.getOrgaoLicitante().getEmailTitularLicitante());
		model.addAttribute("rua", licitanteFisico.getOrgaoLicitante().getRua());
		model.addAttribute("bairro", licitanteFisico.getOrgaoLicitante().getBairro());
		model.addAttribute("cidade", licitanteFisico.getOrgaoLicitante().getCidade());
		model.addAttribute("estado", licitanteFisico.getOrgaoLicitante().getEstado());
		model.addAttribute("cep", licitanteFisico.getOrgaoLicitante().getCep());

		model.addAttribute("idJuridico", licitanteFisico.getId());
		model.addAttribute("cpf", licitanteFisico.getCpf());

		Fornecedor fornecedor;
		try {
			Session_Factory.openSession();
			Criteria consulta = Session_Factory.session().createCriteria(Fornecedor.class);
			consulta.add(Restrictions.eq("licitanteFisico.id", licitanteFisico.getId()));
			fornecedor = (Fornecedor) consulta.uniqueResult();
		} catch (RuntimeException e) {
			throw e;
		} finally {
			Session_Factory.close();
		}

		model.addAttribute("idForm", fornecedor.getId());
		model.addAttribute("expedicaoForn", fornecedor.getDataFormatada(fornecedor.getExpedicaoForn()));
		model.addAttribute("numRegstroForn", fornecedor.getNumRegstroForn());
		model.addAttribute("validadeForn", fornecedor.getDataFormatada(fornecedor.getValidadeForn()));

		ModelAndView modelAndView = new ModelAndView("dtlFisico");
		return modelAndView;
	}

	@RequestMapping(value = "/getLicito/remove/{id}", method = RequestMethod.POST)
	public @ResponseBody String getLicito(@PathVariable Long id) {
//		dao = new OrgaoLicitanteDAO();
		LicitanteFisicoDAO daoL = new LicitanteFisicoDAO();
		LicitanteFisico licitanteFisico = daoL.buscarId(id);
		try {
			daoL.remove(licitanteFisico);
			json = new ResponseJson("1", "Org�o licitante " + licitanteFisico.getOrgaoLicitante().getNomeLicitante() + ", exclu�do com sucesso!");
		} catch (Exception e) {
			json = new ResponseJson("0", "Erro ao excluir org�o licitante " + licitanteFisico.getOrgaoLicitante().getNomeLicitante());
		}
		return gson.toJson(json);
	}

}
