package br.com.siscap.controller;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.mapping.Collection;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import br.com.siscap.Jdbc.Session_Factory;
import br.com.siscap.model.Usuario;

public class ControllerService {

	public Usuario getUsuarioLogado() {
		User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String matricula = user.getUsername();
		Session_Factory.openSession();
		Criteria consulta = Session_Factory.session().createCriteria(Usuario.class);
		consulta.add(Restrictions.eq("matricula", matricula));
		Usuario usuario = (Usuario) consulta.uniqueResult();
		return usuario;
	}
	
	public String returnaFuncao() {
		
		String role = null;
		
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        List<GrantedAuthority> authorities = (List<GrantedAuthority>) auth.getAuthorities();

        //This is the loop which do actual search
        for (GrantedAuthority grantedAuthority : authorities) {
        	role = grantedAuthority.getAuthority(); 
        }
        return role;
    }
		
}
