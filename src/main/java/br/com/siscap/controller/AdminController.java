package br.com.siscap.controller;

import java.util.ArrayList;

import org.hibernate.Criteria;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import br.com.siscap.Jdbc.Session_Factory;
import br.com.siscap.model.LicitanteFisico;
import br.com.siscap.model.LicitanteJuridico;
import br.com.siscap.model.Usuario;

@Controller
@RequestMapping("/admin")
public class AdminController extends ControllerService{
	

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("home");
		return modelAndView;
	}
	
	@RequestMapping(value = "/graficos", method = RequestMethod.GET)
	public ModelAndView graficos(ModelMap model) {
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		ModelAndView modelAndView = new ModelAndView("graficos");
		return modelAndView;
	}
	
	@RequestMapping(value = "/getJuridicos", method = RequestMethod.POST)
	public @ResponseBody ArrayList<LicitanteJuridico> getJuridicos() {		
		Session_Factory.openSession();
		Criteria consulta = Session_Factory.session().createCriteria(LicitanteJuridico.class);
		return (ArrayList<LicitanteJuridico>) consulta.list();
	}
	
	@RequestMapping(value = "/getFisicos", method = RequestMethod.POST)
	public @ResponseBody ArrayList<LicitanteFisico> getFisicos() {		
		Session_Factory.openSession();
		Criteria consulta = Session_Factory.session().createCriteria(LicitanteFisico.class);
		return (ArrayList<LicitanteFisico>) consulta.list();
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView("index");
		return modelAndView;
	}
	
	@RequestMapping(value = "/permissao_negada", method = RequestMethod.GET)
	public ModelAndView permissaoNegada() {
		ModelAndView modelAndView = new ModelAndView("permissaoNegada");
		return modelAndView;
	}

}

