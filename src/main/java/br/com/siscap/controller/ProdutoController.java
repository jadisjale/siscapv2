package br.com.siscap.controller;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import br.com.siscap.Jdbc.Session_Factory;
import br.com.siscap.dao.ProcessoDAO;
import br.com.siscap.dao.ProdutoDAO;
import br.com.siscap.model.Fornecedor;
import br.com.siscap.model.LicitanteJuridico;
import br.com.siscap.model.Processo;
import br.com.siscap.model.Produto;
import br.com.siscap.model.Secretaria;
import br.com.siscap.model.Usuario;
import br.com.siscap.util.ResponseJson;

@Controller
@RequestMapping("/produto")
public class ProdutoController extends ControllerService {
	
	Gson gson = new Gson();
	ResponseJson json;
	ProdutoDAO dao; 

	@RequestMapping(value = "/addProduto", method = RequestMethod.GET)
	public ModelAndView addProduto(ModelMap model) {
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		ModelAndView modelAndView = new ModelAndView("addProduto");
		return modelAndView;
	}

	@RequestMapping(value = "/saveProduto", method = RequestMethod.POST)
	public @ResponseBody String saveProduto(Produto produto, @RequestParam("fkFornecedor") Long fkFornecedor, @RequestParam("idProduto") Long idProduto) {

		Fornecedor f = new Fornecedor();
		f.setId(fkFornecedor);
		produto.setFornecedor(f);
		if(idProduto != null){
			produto.setId(idProduto);
		}
		dao = new ProdutoDAO();
		
		try {
			dao.merge(produto);
			if(produto.getId() != null){
				json = new ResponseJson("2", "Produto editado");
			} else {
				json = new ResponseJson("1", "Produto adicionado ao forncedor");
			}
		} catch (Exception e) {
			json = new ResponseJson("0", "Erro ao produto ao fornecedor forncedor");
		}
		return gson.toJson(json);
	}

	@RequestMapping(value = "/listProduto", method = RequestMethod.GET)
	public ModelAndView listProdutos(ModelMap model) {
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		ModelAndView modelAndView = new ModelAndView("listProduto");
		return modelAndView;

	}

	@RequestMapping(value = "/getProduto/{id}", method = RequestMethod.GET)
	public ModelAndView getProdutoById(@PathVariable Long id, ModelMap model) {
		ProdutoDAO dao = new ProdutoDAO();
		Produto produto = dao.buscarId(id);
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		model.addAttribute("idProduto", produto.getId());
		model.addAttribute("fornecedor", produto.getFornecedor().getId());
		model.addAttribute("descricao", produto.getDescricao());
		model.addAttribute("valor", produto.getValor());
		ModelAndView modelAndView = new ModelAndView("addProduto");
		
		return modelAndView;
	}

	@RequestMapping(value = "/getProdutos", method = RequestMethod.GET)
	public @ResponseBody List getProdutos() {
		Session_Factory.openSession();
		Criteria consulta = Session_Factory.session().createCriteria(Produto.class, "produto");
		return consulta.list();
//		consulta.createAlias("fornecedor", "fornecedor"); // inner join by default
//		consulta.add(Restrictions.eq("produto.fkFornecedor", "fornecedor.id"));
//		Query query = Session_Factory.session().
//		query.setParameter("code", "7277");
//		List list = query.list();
	}

	@RequestMapping(value = "/getProduto/remove/{id}", method = RequestMethod.POST)
	public @ResponseBody String produtoByid(@PathVariable Long id) {
		dao = new ProdutoDAO();
		Produto produto = dao.buscarId(id);		
		try {
			dao.remove(produto);
			json = new ResponseJson("1", "Produto exclu�do com sucesso");
		} catch (Exception e) {
			json = new ResponseJson("0", "Erro ao exclu�r produto");
		}
		return gson.toJson(json);
	}


}
