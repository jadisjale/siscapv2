package br.com.siscap.controller;

import java.util.ArrayList;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import br.com.siscap.dao.UsuarioDAO;
import br.com.siscap.model.Funcao;
import br.com.siscap.model.Usuario;
import br.com.siscap.util.ResponseJson;

@Controller
@RequestMapping("/user")
public class UserController extends ControllerService {
	
	Gson gson = new Gson();
	ResponseJson json;
	UsuarioDAO dao;
	
	@RequestMapping(value = "/addUser", method = RequestMethod.GET)
	public ModelAndView pageUser(ModelMap model) {
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		ModelAndView modelAndView = new ModelAndView("addUser");
		return modelAndView;
	}

	@RequestMapping(value = "/getUsers", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Usuario> getUsers() {
		dao = new UsuarioDAO();
		return (ArrayList<Usuario>) dao.list();
	}

	@RequestMapping(value = "/listUser", method = RequestMethod.GET)
	public ModelAndView listUser(ModelMap model) {
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		ModelAndView modelAndView = new ModelAndView("listUser");
		return modelAndView;
	}

	@RequestMapping(value = "/saveUser", method = RequestMethod.POST)
	public @ResponseBody String saveUser(Usuario usuario, @RequestParam("fkFuncao") Long fkFuncao) {

		SimpleHash hash = new SimpleHash("md5", usuario.getSenha());
		usuario.setSenha(hash.toHex());
		Funcao funcao = new Funcao();
		funcao.setId(fkFuncao);
		usuario.setFuncao(funcao);
		dao = new UsuarioDAO();
		try {
			dao.merge(usuario);
			if(usuario.getId() != null ){
				json = new ResponseJson("1", "Usu�rio " + usuario.getNome() + ", alterado com sucesso");
			} else {
				json = new ResponseJson("1", "Usu�rio " + usuario.getNome() + ", inserido com sucesso");
			} 
			return gson.toJson(json);
		} catch (Exception e) {
			json = new ResponseJson("0", "Erro ao inserir usu�rio");
			return gson.toJson(json);
		}
	}

	@RequestMapping(value = "/getUser/{id}", method = RequestMethod.GET)
	public ModelAndView getContactById(@PathVariable Long id, ModelMap model) {
		dao = new UsuarioDAO();
		Usuario usuario = dao.buscarId(id);
		
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		
		model.addAttribute("id", usuario.getId());
		model.addAttribute("funcao", usuario.getFuncao().getId());
		model.addAttribute("nome", usuario.getNome());
		model.addAttribute("matricula", usuario.getMatricula());
		model.addAttribute("senha", usuario.getSenha());
		ModelAndView modelAndView = new ModelAndView("addUser");
		return modelAndView;
	}
	
	@RequestMapping(value = "/getUser/remove/{id}", method = RequestMethod.POST)
	public @ResponseBody String removeByid(@PathVariable Long id) {
		dao = new UsuarioDAO();
		Usuario usuario = dao.buscarId(id);
		try {
			System.out.println(usuario.getNome());
			dao.remove(usuario);
			json = new ResponseJson("1", "Usu�rio " + usuario.getNome() + " deletado com sucesso!");
			return gson.toJson(json);
		} catch (Exception e) {
			json = new ResponseJson("0", "Erro ao deletar usu�rio");
			return gson.toJson(json);
		}
	}

}
