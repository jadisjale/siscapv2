package br.com.siscap.controller;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import br.com.siscap.dao.FuncaoDAO;
import br.com.siscap.dao.UsuarioDAO;
import br.com.siscap.model.Funcao;
import br.com.siscap.model.Usuario;
import br.com.siscap.util.ResponseJson;

@Controller
@RequestMapping("/funcao")
public class FuncaoController extends ControllerService {
	
	Gson gson = new Gson();
	ResponseJson json;
	FuncaoDAO dao;
	
	@RequestMapping(value = "/addFuncao", method = RequestMethod.GET)
	public ModelAndView pageFuncao() {
		ModelAndView modelAndView = new ModelAndView("addFuncao");
		return modelAndView;
	}
	
	@RequestMapping(value = "/saveFuncao", method = RequestMethod.POST)
	public @ResponseBody String saveFuncao(Funcao funcao) {

		dao = new FuncaoDAO();
		try {
			dao.merge(funcao);
			if(funcao.getId() != null ){
				json = new ResponseJson("1", "Fun��o " + funcao.getFuncao() + ", alterado com sucesso");
			} else {
				json = new ResponseJson("1", "Fun��o " + funcao.getFuncao() + ", inserido com sucesso");
			} 
			return gson.toJson(json);
		} catch (Exception e) {
			json = new ResponseJson("0", "Erro ao inserir fun��o");
			return gson.toJson(json);
		}
	}
	
	@RequestMapping(value = "/getFuncoes", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Funcao> getFuncoes() {
		dao = new FuncaoDAO();
		return (ArrayList<Funcao>) dao.list();
	}

	@RequestMapping(value = "/listFuncao", method = RequestMethod.GET)
	public ModelAndView listFuncao() {
		ModelAndView modelAndView = new ModelAndView("listFuncao");
		return modelAndView;
	}
	
	@RequestMapping(value = "/getFuncao/{id}", method = RequestMethod.GET)
	public ModelAndView getContactById(@PathVariable Long id, ModelMap model) {
		dao = new FuncaoDAO();
		Funcao funcao = dao.buscarId(id);
		model.addAttribute("id", funcao.getId());
		model.addAttribute("funcao", funcao.getFuncao());
		ModelAndView modelAndView = new ModelAndView("addFuncao");
		return modelAndView;
	}
	
	@RequestMapping(value = "/getFuncao/remove/{id}", method = RequestMethod.POST)
	public @ResponseBody String removeByid(@PathVariable Long id) {
		dao = new FuncaoDAO();
		Funcao funcao = dao.buscarId(id);
		try {
			System.out.println(funcao.getFuncao());
			dao.remove(funcao);
			json = new ResponseJson("1", "Fun��o " + funcao.getFuncao() + " deletada com sucesso!");
			return gson.toJson(json);
		} catch (Exception e) {
			json = new ResponseJson("0", "Erro ao deletar fun��o");
			return gson.toJson(json);
		}
	}


}
