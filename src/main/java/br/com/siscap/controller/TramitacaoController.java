package br.com.siscap.controller;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.hibernate.Criteria;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import br.com.siscap.Jdbc.Session_Factory;
import br.com.siscap.dao.TramitacaoDAO;
import br.com.siscap.model.Processo;
import br.com.siscap.model.Secretaria;
import br.com.siscap.model.Tramitacao;
import br.com.siscap.model.Usuario;
import br.com.siscap.util.ResponseJson;

@Controller
@RequestMapping(value = "/tramitacao")
public class TramitacaoController extends ControllerService {
	
	Gson gson = new Gson();
	ResponseJson json;
	TramitacaoDAO dao;
		
	@RequestMapping(value = "/addTramite")
	public ModelAndView addTramite(ModelMap model) {
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		ModelAndView modelAndView = new ModelAndView("addTramite");
		return modelAndView;
	}
	
	@RequestMapping(value = "/saveTramite", method = RequestMethod.POST)
	public @ResponseBody String saveTramite(Tramitacao tramitacao, @RequestParam("fkProcesso") Long fkProcesso,
			@RequestParam("fkSecretaria") Long fkSecretaria, @RequestParam("retornoTramit") String dataRetornor,
			@RequestParam("saidaTramit") String dataEntrada, @RequestParam("id") Long idTramite) throws ParseException{
		
		
		if(idTramite != null){
			tramitacao.setId(idTramite);
		}
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date retorno = new Date(format.parse(dataEntrada).getTime());
		Date entrada = new Date(format.parse(dataRetornor).getTime());
		
		tramitacao.setRetornoTramit(retorno);
		tramitacao.setSaidaTramit(entrada);
		
		Processo processo = new Processo();
		processo.setId(fkProcesso);
		tramitacao.setProcesso(processo);
		
		Secretaria secretaria = new Secretaria();
		secretaria.setId(fkSecretaria);
		tramitacao.setSecretaria(secretaria); 
		
		try {
			dao = new TramitacaoDAO();
			dao.merge(tramitacao);
			if(tramitacao.getId() != null ){
				json = new ResponseJson("1", "Tramita��o alterado com sucesso");
			} else {
				json = new ResponseJson("1", "Tramita��o cadastrado com sucesso");
			} 
			return gson.toJson(json);
		} catch (Exception e) {
			json = new ResponseJson("0", "Erro ao inserir tramita��o");
			return gson.toJson(json);
		}
		
	}
	
	@RequestMapping(value = "/listTramitacao")
	public ModelAndView listTramitacao(ModelMap model) {
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());
		ModelAndView modelAndView = new ModelAndView("listTramitacao");
		return modelAndView;
	}
	
	@RequestMapping(value = "/getTramites", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Tramitacao> getTramites() {
		Session_Factory.openSession();
		Criteria criteria = Session_Factory.session().createCriteria(Tramitacao.class);
		return (ArrayList<Tramitacao>) criteria.list();
	}
	
	@RequestMapping(value = "/getTramitacao/{id}", method = RequestMethod.GET)
	public ModelAndView getTramitacao(@PathVariable Long id, ModelMap model) {

		dao = new TramitacaoDAO();
		Tramitacao tramitacao = dao.buscarId(id);
		
		model.addAttribute("funcaoUsuarioLogado", this.returnaFuncao());

		model.addAttribute("idTramitacao", tramitacao.getId());
		model.addAttribute("idProcesso", tramitacao.getProcesso().getId());
		model.addAttribute("idSecretaria", tramitacao.getSecretaria().getId());
		model.addAttribute("motivoTramit", tramitacao.getMotivoTramit());
		model.addAttribute("responsTramit", tramitacao.getResponsTramit());
		model.addAttribute("saidaTramit", tramitacao.getDataFormatada(tramitacao.getSaidaTramit()));
		model.addAttribute("retornoTramit", tramitacao.getDataFormatada(tramitacao.getRetornoTramit()));
		
		ModelAndView modelAndView = new ModelAndView("addTramite");
		return modelAndView;
		
	}
	
	@RequestMapping(value = "/getTramitacao/remove/{id}", method = RequestMethod.POST)
	public @ResponseBody String produtoByid(@PathVariable Long id) {
		dao = new TramitacaoDAO();
		Tramitacao tramitacao = dao.buscarId(id);		
		try {
			dao.remove(tramitacao);
			json = new ResponseJson("1", "Tramita��o exclu�do com sucesso");
		} catch (Exception e) {
			json = new ResponseJson("0", "Erro ao exclu�r tramita��o");
		}
		return gson.toJson(json);
	}

}
