package br.com.siscap.main;

import java.util.List;

import org.junit.Test;

import br.com.siscap.dao.UsuarioDAO;
import br.com.siscap.model.Usuario;

public class Main {
	
	
	public static void main(String[] args) {

		// Criamos uma instancia da classe UsuarioDAO.
		UsuarioDAO dao = new UsuarioDAO();

		// busca todas os usu�rios cadastrados no banco, retorna uma lista.
		List<Usuario> listUser = dao.list();
		for (Usuario user : listUser)
			System.out.println("findAll: " + user.getId() + " " + user);

	}

}
