package br.com.siscap.util;

public class ResponseJson {
	
	private String cod;
	private String message;
	
	public ResponseJson (String cod, String message) {
		this.cod = cod;
		this.message = message;
	}
	
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
