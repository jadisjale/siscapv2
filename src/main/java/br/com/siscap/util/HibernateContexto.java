package br.com.siscap.util;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class HibernateContexto implements ServletContextListener {

    public void contextDestroyed(ServletContextEvent event) {
        HibernateUtil.getSessionFactory().close();
    }

    public void contextInitialized(ServletContextEvent event) {
        HibernateUtil.getSessionFactory();
    }

}
