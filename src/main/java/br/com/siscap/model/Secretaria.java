package br.com.siscap.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.validator.constraints.br.CNPJ;

@SuppressWarnings("serial")
@Entity
public class Secretaria extends IdGeneric implements SerializableIterface {

	@Column(length = 45)
	private String gerenteSec;

	@Column(length = 45)
	private String telGerenteSec;

	@Column(length = 45)
	private String celGerenteSec;

	@Column(length = 45)
	private String emailGerenteSec;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "fkLicitanteJuridico")
	private LicitanteJuridico licitanteJuridico;

	public String getGerenteSec() {
		return gerenteSec;
	}

	public void setGerenteSec(String gerenteSec) {
		this.gerenteSec = gerenteSec;
	}

	public String getTelGerenteSec() {
		return telGerenteSec;
	}

	public void setTelGerenteSec(String telGerenteSec) {
		this.telGerenteSec = telGerenteSec;
	}

	public String getCelGerenteSec() {
		return celGerenteSec;
	}

	public void setCelGerenteSec(String celGerenteSec) {
		this.celGerenteSec = celGerenteSec;
	}

	public String getEmailGerenteSec() {
		return emailGerenteSec;
	}

	public void setEmailGerenteSec(String emailGerenteSec) {
		this.emailGerenteSec = emailGerenteSec;
	}

	public LicitanteJuridico getLicitanteJuridico() {
		return licitanteJuridico;
	}

	public void setLicitanteJuridico(LicitanteJuridico licitanteJuridico) {
		this.licitanteJuridico = licitanteJuridico;
	}

	@Override
	public String toString() {
		return "Secretaria{" + "gerenteSec = " + gerenteSec + ", telGerenteSec = " + telGerenteSec
				+ ", celGerenteSec = " + celGerenteSec + ", emailGerenteSec = " + emailGerenteSec
				+ ", licitanteJuridico = " + licitanteJuridico + '}';
	}

}
