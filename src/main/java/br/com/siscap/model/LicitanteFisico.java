package br.com.siscap.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import org.hibernate.validator.constraints.br.CPF;

@Entity
public class LicitanteFisico extends IdGeneric implements SerializableIterface {

    @CPF
    @Column(unique = true, nullable = false, length = 14)
    private String cpf;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "fkOrgaoLicitante")
    private OrgaoLicitante orgaoLicitante;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public OrgaoLicitante getOrgaoLicitante() {
        return orgaoLicitante;
    }

    public void setOrgaoLicitante(OrgaoLicitante orgaoLicitante) {
        this.orgaoLicitante = orgaoLicitante;
    }

    @Override
    public String toString() {
        return "LicitanteFisico{" + "cpf = " + cpf + ", orgaoLicitante = " + orgaoLicitante + '}';
    }

}
