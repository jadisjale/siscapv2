package br.com.siscap.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@SuppressWarnings("serial")
@Entity
public class Funcao extends IdGeneric implements SerializableIterface {
	
	@Column(nullable = false, length = 50, unique = true)
    private String funcao;

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    @Override
    public String toString() {
        return "Funcao{" + "funcao = " + funcao + '}';
    }

}
