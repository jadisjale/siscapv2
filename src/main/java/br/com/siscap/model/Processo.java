package br.com.siscap.model;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Processo extends IdGeneric implements SerializableIterface {

	@Column(length = 10)
	@Temporal(TemporalType.DATE)
	private Date inicioProcesso;

	@Column(length = 50)
	private String numeroProcesso;

	@Column(length = 10)
	@Temporal(TemporalType.DATE)
	private Date entradaProcesso;

	@Column(precision = 11, scale = 2)
	private BigDecimal vlorEstimadoProc;

	@Column(length = 50)
	private String numeroSolicProc;

	@Column(length = 50)
	private String numModalProc;

	@Column(length = 50)
	private String modalidadeProc;

	@Column(length = 50)
	private String tipoProc;

	@Column(length = 50)
	private String origemRecProc;

	@Column(length = 50)
	private String comissaoRespProc;

	@Column(length = 80)
	private String objetoProc;

	@Column(length = 50)
	private String situacaoProc;

	@Column(length = 50)
	private String observacaoProc;

	@Column(length = 10)
	@Temporal(TemporalType.DATE)
	private Date dataContrato;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkFornecedor")
	private Fornecedor fornecedor;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkSecretaria")
	private Secretaria secretaria;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkSecretaria2")
	private Secretaria secretaria2;

	public Date getInicioProcesso() {
		return inicioProcesso;
	}

	public void setInicioProcesso(Date inicioProcesso) {
		this.inicioProcesso = inicioProcesso;
	}

	public String getNumeroProcesso() {
		return numeroProcesso;
	}

	public void setNumeroProcesso(String numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}

	public Date getEntradaProcesso() {
		return entradaProcesso;
	}

	public void setEntradaProcesso(Date entradaProcesso) {
		this.entradaProcesso = entradaProcesso;
	}

	public BigDecimal getVlorEstimadoProc() {
		return vlorEstimadoProc;
	}

	public void setVlorEstimadoProc(BigDecimal vlorEstimadoProc) {
		this.vlorEstimadoProc = vlorEstimadoProc;
	}

	public String getNumeroSolicProc() {
		return numeroSolicProc;
	}

	public void setNumeroSolicProc(String numeroSolicProc) {
		this.numeroSolicProc = numeroSolicProc;
	}

	public String getNumModalProc() {
		return numModalProc;
	}

	public void setNumModalProc(String numModalProc) {
		this.numModalProc = numModalProc;
	}

	public String getModalidadeProc() {
		return modalidadeProc;
	}

	public void setModalidadeProc(String modalidadeProc) {
		this.modalidadeProc = modalidadeProc;
	}

	public String getTipoProc() {
		return tipoProc;
	}

	public void setTipoProc(String tipoProc) {
		this.tipoProc = tipoProc;
	}

	public String getOrigemRecProc() {
		return origemRecProc;
	}

	public void setOrigemRecProc(String origemRecProc) {
		this.origemRecProc = origemRecProc;
	}

	public String getComissaoRespProc() {
		return comissaoRespProc;
	}

	public void setComissaoRespProc(String comissaoRespProc) {
		this.comissaoRespProc = comissaoRespProc;
	}

	public String getObjetoProc() {
		return objetoProc;
	}

	public void setObjetoProc(String objetoProc) {
		this.objetoProc = objetoProc;
	}

	public String getSituacaoProc() {
		return situacaoProc;
	}

	public void setSituacaoProc(String situacaoProc) {
		this.situacaoProc = situacaoProc;
	}

	public String getObservacaoProc() {
		return observacaoProc;
	}

	public void setObservacaoProc(String observacaoProc) {
		this.observacaoProc = observacaoProc;
	}

	public Date getDataContrato() {
		return dataContrato;
	}

	public void setDataContrato(Date dataContrato) {
		this.dataContrato = dataContrato;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public Secretaria getSecretaria() {
		return secretaria;
	}

	public void setSecretaria(Secretaria secretaria) {
		this.secretaria = secretaria;
	}

	public Secretaria getSecretaria2() {
		return secretaria2;
	}

	public void setSecretaria2(Secretaria secretaria2) {
		this.secretaria2 = secretaria2;
	}

	@Override
	public String toString() {
		return "Processo{" + "inicioProcesso = " + inicioProcesso + ", numeroProcesso = " + numeroProcesso
				+ ", entradaProcesso = " + entradaProcesso + ", vlorEstimadoProc=" + vlorEstimadoProc
				+ ", numeroSolicProc=" + numeroSolicProc + ", numModalProc=" + numModalProc + ", modalidadeProc="
				+ modalidadeProc + ", tipoProc=" + tipoProc + ", origemRecProc=" + origemRecProc + ", comissaoRespProc="
				+ comissaoRespProc + ", objetoProc=" + objetoProc + ", situacaoProc=" + situacaoProc
				+ ", observacaoProc=" + observacaoProc + ", dataContrato=" + dataContrato + ", secretaria = "
				+ secretaria + '}';
	}
	
	public String getDataFormatada(Date date){
	    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	    return sdf.format(date);
	}

}
