package br.com.siscap.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
public class Fornecedor extends IdGeneric implements SerializableIterface {

	@Column(length = 50)
	private String numRegstroForn;

	@Column(length = 50)
	@Temporal(TemporalType.DATE)
	private Date validadeForn;

	@Column(length = 50)
	@Temporal(TemporalType.DATE)
	private Date expedicaoForn;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "fkLicitanteFisico")
	private LicitanteFisico licitanteFisico;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "fkLicitanteJuridico")
	private LicitanteJuridico licitanteJuridico;
	
	public String getNumRegstroForn() {
		return numRegstroForn;
	}

	public void setNumRegstroForn(String numRegstroForn) {
		this.numRegstroForn = numRegstroForn;
	}

	public Date getValidadeForn() {
		return validadeForn;
	}

	public void setValidadeForn(Date validadeForn) {
		this.validadeForn = validadeForn;
	}

	public Date getExpedicaoForn() {
		return expedicaoForn;
	}

	public void setExpedicaoForn(Date expedicaoForn) {
		this.expedicaoForn = expedicaoForn;
	}

	public LicitanteFisico getLicitanteFisico() {
		return licitanteFisico;
	}

	public void setLicitanteFisico(LicitanteFisico licitanteFisico) {
		this.licitanteFisico = licitanteFisico;
	}

	public LicitanteJuridico getLicitanteJuridico() {
		return licitanteJuridico;
	}

	public void setLicitanteJuridico(LicitanteJuridico licitanteJuridico) {
		this.licitanteJuridico = licitanteJuridico;
	}

	@Override
	public String toString() {
		return "Fornecedor{" + "numRegstroForn = " + numRegstroForn + ", validadeForn = " + validadeForn
				+ ", expedicaoForn = " + expedicaoForn + ", licitanteFisico = " + licitanteFisico
				+ ", licitanteJuridico = " + licitanteJuridico + '}';
	}
	
	public String getDataFormatada(Date date){
	    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	    return sdf.format(date);
	}

}
