package br.com.siscap.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

@Entity
public class Usuario extends IdGeneric implements SerializableIterface {

    @Column(length = 50, nullable = false)
    private String nome;

    @Column(length = 50, nullable = false)
    private String senha;

    @Transient
    private String senhaSemCriptografia;

    @Column(length = 50, unique = true, nullable = false)
    private String matricula;
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fkFuncao")
    private Funcao funcao;

    public Funcao getFuncao() {
        return funcao;
    }

    public void setFuncao(Funcao funcao) {
        this.funcao = funcao;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getSenhaSemCriptografia() {
        return senhaSemCriptografia;
    }

    public void setSenhaSemCriptografia(String senhaSemCriptografia) {
        this.senhaSemCriptografia = senhaSemCriptografia;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @Override
    public String toString() {
        return "Usuario{" + "nome = " + nome + ", senha = " + senha
                + ", senhaSemCriptografia = " + senhaSemCriptografia
                + ", matricula = " + matricula 
                + ", funcao = " + funcao + '}';
    }

}
