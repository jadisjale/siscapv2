package br.com.siscap.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
public class Tramitacao extends IdGeneric implements SerializableIterface {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fkProcesso")
    private Processo processo;
    
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "fkSecretaria")
    private Secretaria secretaria;

    @Column(length = 10)
    @Temporal(TemporalType.DATE)
    private Date saidaTramit;

    @Column(length = 50)
    private String responsTramit;

    @Column(length = 80)
    private String motivoTramit;

    @Column(length = 10)
    @Temporal(TemporalType.DATE)
    private Date retornoTramit;

    public Date getSaidaTramit() {
        return saidaTramit;
    }

    public void setSaidaTramit(Date saidaTramit) {
        this.saidaTramit = saidaTramit;
    }

    public String getResponsTramit() {
        return responsTramit;
    }

    public void setResponsTramit(String responsTramit) {
        this.responsTramit = responsTramit;
    }

    public String getMotivoTramit() {
        return motivoTramit;
    }

    public void setMotivoTramit(String motivoTramit) {
        this.motivoTramit = motivoTramit;
    }

    public Date getRetornoTramit() {
        return retornoTramit;
    }

    public void setRetornoTramit(Date retornoTramit) {
        this.retornoTramit = retornoTramit;
    }

    public Processo getProcesso() {
        return processo;
    }

    public void setProcesso(Processo processo) {
        this.processo = processo;
    }

    public Secretaria getSecretaria() {
        return secretaria;
    }

    public void setSecretaria(Secretaria secretaria) {
        this.secretaria = secretaria;
    }
    
    @Override
    public String toString() {
        return "Tramitacao{" + " processo = " + processo + ", saidaTramit = " + saidaTramit
                + ", responsTramit = " + responsTramit
                + ", motivoTramit = " + motivoTramit + ", retornoTramit = " + retornoTramit + '}';
    }
    
    public String getDataFormatada(Date date){
	    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	    return sdf.format(date);
	}

}
