package br.com.siscap.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@SuppressWarnings("serial")
@Entity
public class OrgaoLicitante extends IdGeneric implements SerializableIterface {

    @Column(length = 50, nullable = false)
    private String nomeLicitante;

    @Column(length = 50)
    private String titularLicitante;

    @Column(length = 20)
    private String telefoneLicitante;

    @Column(length = 50)
    private String faxLicitante;

    @Column(length = 50)
    private String emailLicitante;

    @Column(length = 20)
    private String telefoneTitularLicitante;

    @Column(length = 50)
    private String emailTitularLicitante;

    @Column(length = 50)
    private String rua;

    @Column(length = 80)
    private String bairro;

    @Column(length = 50)
    private String cidade;

    @Column(length = 20)
    private String estado;

    @Column(length = 10)
    private String cep;

    public String getNomeLicitante() {
        return nomeLicitante;
    }

    public void setNomeLicitante(String nomeLicitante) {
        this.nomeLicitante = nomeLicitante;
    }

    public String getTitularLicitante() {
        return titularLicitante;
    }

    public void setTitularLicitante(String titularLicitante) {
        this.titularLicitante = titularLicitante;
    }

    public String getTelefoneLicitante() {
        return telefoneLicitante;
    }

    public void setTelefoneLicitante(String telefoneLicitante) {
        this.telefoneLicitante = telefoneLicitante;
    }

    public String getFaxLicitante() {
        return faxLicitante;
    }

    public void setFaxLicitante(String faxLicitante) {
        this.faxLicitante = faxLicitante;
    }

    public String getEmailLicitante() {
        return emailLicitante;
    }

    public void setEmailLicitante(String emailLicitante) {
        this.emailLicitante = emailLicitante;
    }

    public String getTelefoneTitularLicitante() {
        return telefoneTitularLicitante;
    }

    public void setTelefoneTitularLicitante(String telefoneTitularLicitante) {
        this.telefoneTitularLicitante = telefoneTitularLicitante;
    }

    public String getEmailTitularLicitante() {
        return emailTitularLicitante;
    }

    public void setEmailTitularLicitante(String emailTitularLicitante) {
        this.emailTitularLicitante = emailTitularLicitante;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    @Override
    public String toString() {
        return "OrgaoLicitante{" + "nomeLicitante = " + nomeLicitante + ", titularLicitante = " + titularLicitante
                + ", telefoneLicitante = " + telefoneLicitante + ", faxLicitante = " + faxLicitante
                + ", emailLicitante = " + emailLicitante + ", telefoneTitularLicitante = " + telefoneTitularLicitante
                + ", emailTitularLicitante = " + emailTitularLicitante + ", rua = " + rua + ", bairro = " + bairro
                + ", cidade = " + cidade + ", estado = " + estado + ", cep = " + cep + '}';
    }

}
