package br.com.siscap.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import org.hibernate.validator.constraints.br.CNPJ;

@Entity
public class LicitanteJuridico extends IdGeneric implements SerializableIterface {

    @CNPJ
    @Column(unique = true, nullable = false, length = 18)
    private String cnpj;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "fkOrgaoLicitante")
    private OrgaoLicitante orgaoLicitante;

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public OrgaoLicitante getOrgaoLicitante() {
        return orgaoLicitante;
    }

    public void setOrgaoLicitante(OrgaoLicitante orgaoLicitante) {
        this.orgaoLicitante = orgaoLicitante;
    }

    @Override
    public String toString() {
        return "LicitanteJuridico{" + "cnpj = " + cnpj + ", orgaoLicitante = " + orgaoLicitante + '}';
    }
}
