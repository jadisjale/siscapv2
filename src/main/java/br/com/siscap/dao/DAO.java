package br.com.siscap.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;
import javax.persistence.Query;
import org.hibernate.criterion.Restrictions;

import com.google.gson.Gson;

import br.com.siscap.util.HibernateUtil;
import br.com.siscap.util.ResponseJson;
import br.com.siscap.Jdbc.Session_Factory;
import br.com.siscap.model.SerializableIterface;

public abstract class DAO<Entity extends SerializableIterface> {

    private final Class<Entity> classe;
	
    @SuppressWarnings("unchecked")
	public DAO() {
        this.classe = (Class<Entity>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];

    }

    public void merge(Entity object) {
        try {
            Session_Factory.openSession();
            Session_Factory.session().merge(object);
            Session_Factory.transaction().commit();
          
        } catch (RuntimeException e) {
            if (Session_Factory.transaction() != null) {
                Session_Factory.transaction().rollback();
            }
            throw e;
        } finally {
            Session_Factory.close();
        }
    }

    public void save(Entity object) {
        try {
            Session_Factory.openSession();
            Session_Factory.session().save(object);
            Session_Factory.transaction().commit();
        } catch (RuntimeException e) {
            if (Session_Factory.transaction() != null) {
                Session_Factory.transaction().rollback();
            }
            throw e;
        } finally {
            Session_Factory.close();
        }
    }

    public void remove(Entity object) {
        try {
            Session_Factory.openSession();
            Session_Factory.session().delete(object);
            Session_Factory.transaction().commit();
        } catch (RuntimeException e) {
            if (Session_Factory.transaction() != null) {
                Session_Factory.transaction().rollback();
            }
            throw e;
        } finally {
            Session_Factory.close();
        }
    }

    public void update(Entity object) {
        try {
            Session_Factory.openSession();
            Session_Factory.session().update(object);
            Session_Factory.transaction().commit();
        } catch (RuntimeException e) {
            if (Session_Factory.transaction() != null) {
                Session_Factory.transaction().rollback();
            }
            throw e;
        } finally {
            Session_Factory.close();
        }
    }

    @SuppressWarnings("unchecked")
	public List<Entity> list() {
		try {
			return HibernateUtil.getSession().createCriteria(this.classe).list();
        } catch (RuntimeException e) {
            throw e;
        } finally {
            Session_Factory.close();
        }
    }

    @SuppressWarnings("unchecked")
	public Entity buscarId(Long id) {
        try {
        	return (Entity) HibernateUtil.getSession().createCriteria(this.classe)
					.add(Restrictions.idEq(id)).uniqueResult();
        } catch (RuntimeException e) {
            throw e;
        } finally {
            Session_Factory.close();
        }
    }

    public List<Entity> finParam(Map<String, Object> params, Entity t) {
        try {
            Session_Factory.openSession();
            Query q = (Query) Session_Factory.session().createQuery("");
            for (String p : params.keySet()) {
                q.setParameter(p, params.get(p));
            }
            return q.getResultList();
        } catch (RuntimeException e) {
            throw e;
        } finally {
            Session_Factory.close();
        }
    }
}
