package br.com.siscap.dao;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import br.com.siscap.Jdbc.Session_Factory;
import br.com.siscap.model.Usuario;

public class UsuarioDAO extends DAO<Usuario> {

    public Usuario autenticar(String matricula, String senha) {
        Usuario resultado;
        try {
            Session_Factory.openSession();
            Criteria consulta = Session_Factory.session().createCriteria(Usuario.class);
            /* consulta.createAlias("usuario", "u");  só usa o alias qdo tem relacionamento de entidades */
            consulta.add(Restrictions.eq("matricula", matricula));
            SimpleHash hash = new SimpleHash("md5", senha);
            consulta.add(Restrictions.eq("senha", hash.toHex()));
            return resultado = (Usuario) consulta.uniqueResult();

        } catch (RuntimeException erro) {
            throw erro;
        } finally {
            Session_Factory.close();
        }
    }
    
     public Usuario porMatricula(String matricula) {
        Usuario resultado;
        try {
            Session_Factory.openSession();
            Criteria consulta = Session_Factory.session().createCriteria(Usuario.class);
            consulta.add(Restrictions.eq("matricula", matricula));
            return resultado = (Usuario) consulta.uniqueResult();

        } catch (RuntimeException erro) {
            throw erro;
        } finally {
            Session_Factory.close();
        }
    }


}
