var fornecedorFisisco = {
		index: function(){
			// input mask telefone
			 $('#telGerenteSec').mask('(99) 99999-9999');
			 $('#celGerenteSec').mask('(99) 99999-9999');
			 $('#telefoneTitularLicitante').mask('(99) 99999-9999');
			 $('#telefoneLicitante').mask('(99) 99999-9999');		 
			// fim mask telefone
			 
			 $('#cpf').mask('999.999.999-99');
			 
			 $('.datepicker').datepicker({
					language : 'pt-BR',
					format : 'dd/mm/yyyy',
					startDate : '-3d',
				});

				$(".datepicker").each(function(index, element) {
					var context = $(this);
					context.on("blur", function(e) {
						// The setTimeout is the key here.
						setTimeout(function() {
							if (!context.is(':focus')) {
								$(context).datepicker("hide");
							}
						}, 250);
					});
				});
				
				fornecedorFisisco.validadorForm();
		},
			
		validadorForm : function() {
							
			var validator = $('#saveFormFornecedorFisico').validate({
				
		        rules: {
		        	nomeLicitante: {
		                required: true,
		                minlength: 3
		            },
		            telefoneLicitante: {
		                required: true,
		                minlength: 3
		            },
		            faxLicitante: {
		                required: true,
		                minlength: 3
		            },
		            telefoneTitularLicitante: {
		                required: true,
		                minlength: 3
		            },
		            emailTitularLicitante: {
		                required: true,
		                minlength: 3
		            },
		            rua: {
		                required: true,
		                minlength: 3
		            },
		            bairro: {
		                required: true,
		                minlength: 3
		            },
		            cidade: {
		                required: true,
		                minlength: 3
		            },
		            estado: {
		                required: true,
		                minlength: 3
		            },
		            cpf: {
		                required: true,
		                minlength: 3
		            },
		            expedicaoFornParam: {
		                required: true,
		                minlength: 3
		            },
		            numRegstroForn: {
		                required: true,
		                minlength: 3
		            },
		            validadeFornParam: {
		                required: true,
		                minlength: 3
		            },
		        },
		        
		        highlight: function(element) {
		            var id_attr = "#" + $( element ).attr("id") + "1";
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');         
		        },
		        
		        unhighlight: function(element) {
		            var id_attr = "#" + $( element ).attr("id") + "1";
		            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');         
		        },
		        
		        errorElement: 'span',
	            errorClass: 'help-block',
	            errorPlacement: function(error, element) {
	                if(element.length) {
	                    error.insertAfter(element);
	                } else {
	                error.insertAfter(element);
	                }
	            }, 
		        
		        submitHandler: function( form ) {
		        	console.log('passou');
		        	return false;
		        },
		        
		    });
		},
		
		ajaxSaveFornecedorFisisco : function() {
			var nomeLicitatne = $('#nomeLicitante').val();
			var titularLicitante = $('#titularLicitante').val();
			var telefoneLicitante = $('#telefoneLicitante').val();
			var faxLicitante = $('#faxLicitante').val();
			var emailLicitante = $('#emailLicitante').val();
			var telefoneTitularLicitante = $('#telefoneTitularLicitante').val();
			var emailTitularLicitante = $('#emailTitularLicitante').val();
			var rua = $('#rua').val();
			var bairro = $('#bairro').val();
			var cidade = $('#cidade').val();
			var estado = $('#estado').val();
			var cep = $('#cep').val();
			var cpf = $('#cpf').val();
			var expedicaoForn = $('#expedicaoForn').val();
			var numRegstroForn = $('#numRegstroForn').val();
			var validadeForn = $('#validadeForn').val();
			
			tools.ajax('/SisCapV2/fornecedor/saveFornecedorFisico', $('#saveFormFornecedorFisico').serialize(), function(result) {
				var json = JSON.parse(result);
				if (json.cod != 0) {
					tools.messageToast('success', json.message);
					tools.resetForm('form-save-user');
					tools.inputFocus('inputNameUser');
				} else {
					tools.messageToast('error', json.message);
				}
			});
			
//			$.ajax({
//				url : '/SisCapV2/fornecedor/saveFornecedorFisico',
//				type : 'POST',
//				data : $('#saveFormFornecedorFisico').serialize(),
//				success : function(data) {
//					adminUser.messageSuccess($('#nomeLicitante').val());
//					adminFornecedorFisico.clearInputs();
//				},
//				error : function(data) {
//					console.log("error");
//					$("form").append("<div class='alert alert-danger' role='alert' >Registro duplicado!</div>");
//					},
//					complete : function(data) {
//
//					}
//
//			});
		},
}