var adminTramite = {
		index : function () {
			adminTramite.ajaxLoadSecretaria();
			adminTramite.ajaxLoadProcesso();
			adminTramite.loadTableTramite();
			adminTramite.validation();
			
			$('.datepicker').datepicker({
				language : 'pt-BR',
				format : 'dd/mm/yyyy',
				startDate : '-3d',
			});

			$(".datepicker").each(function(index, element) {
				var context = $(this);
				context.on("blur", function(e) {
					// The setTimeout is the key here.
					setTimeout(function() {
						if (!context.is(':focus')) {
							$(context).datepicker("hide");
						}
					}, 250);
				});
			});
		},
		
		ajaxLoadSecretaria : function (){
			$.ajax({
				url : '/SisCapV2/admin/getSecretariasIdName',
				type : 'GET',
				success : function(data) {
					var cmb = "";
					$.each(data, function(i, value) {
						cmb = cmb + '<option value="' + value.id + '">'
						+ value.licitanteJuridico.orgaoLicitante.nomeLicitante + '</option>';
					});
					$('#fkSecretaria').append(cmb);
				},
				error : function(data) {
					console.log(data);
				},
	 		});
		},
		
		ajaxLoadProcesso : function (){
			$.ajax({
				url : '/SisCapV2/admin/getProcessoNumId',
				type : 'GET',
				success : function(data) {
					var cmb = "";
					$.each(data, function(i, value) {
						cmb = cmb + '<option value="' + value.id + '">'
						+ value.numeroProcesso + '</option>';
					});
					$('#fkProcesso').append(cmb);
				},
				error : function(data) {
					console.log(data);
				},
	 		});
		},

		validation : function(){
			
			var validator = $('#saveFormTramite').validate({
			
			rules: {
				motivoTramit: {
			        required: true,
			        minlength: 3
			    },
			    responsTramit: {
			        required: true,
			        minlength: 3
			    },
			    retornoTramit: {
			        required: true,
			    },
			    saidaTramit: {
			        required: true,
			    },
			    fkProcesso: {
			        required: true,
			    },
			    fkSecretaria: {
			        required: true,
			    },
			   
			},
			
			highlight: function(element) {
			    var id_attr = "#" + $( element ).attr("id") + "1";
			    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			    $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');         
			},
			
			unhighlight: function(element) {
			    var id_attr = "#" + $( element ).attr("id") + "1";
			    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			    $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');         
			},
			
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
			    if(element.length) {
			        error.insertAfter(element);
			    } else {
			    error.insertAfter(element);
			    }
			}, 
			
			submitHandler: function( form ) {
				adminTramite.ajaxSaveTramite();
				return false;
			},
			
		});

		
		},
		ajaxSaveTramite : function (){
			console.log('passou da validacao');
//			$.ajax({
//				url : '/SisCapV2/admin/saveTramite',
//				type : 'POST',
//				data : $('#saveFormTramite').serialize(),
//				success : function(data) {
//					$('#motivoTramit').val("");
//					$('#responsTramit').val("");
//					$('#retornoTramit').val("");
//					$('#saidaTramit').val("");
//				},
//				error : function(data) {
//					$("form").append(
//						"<div class='alert alert-danger' role='alert' >Registro duplicado!</div>");
//				},
//				complete: function(data){
//						
//				}
//
//	 		});
		},
		
		loadTableTramite: function(){
			$('#tableTramite').dataTable( {
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
		        },
				   "ajax": {
				      "url": window.location.protocol + '//' + window.location.host + "/SisCapV2/admin/getTramites",
				      "dataSrc": "",
				   },
				   "columns": [
				      { "data": "" },
				      { "data": ""},
				      { "data": ""},
				      { "data": "id"},
				      { "data": "id"},
				      { "data": "id"}
				   ],
				   columnDefs: [
				                {
				                    render: function(data) {
				                        return "<a href= '/SisCapV2/admin/getSecretaria/" +data+ "' class='edit'><i class='glyphicon glyphicon-pencil'></i></a>";
				                    },
				                    targets: 3
				                },
				                {
				                    render: function(data) {
				                        return "<a href= 'javascript:void(0);' class='del'><i class='glyphicon glyphicon-remove'></i></a>"
				                    },
				                    targets: 4
				                },
				                {
				                    render: function(data) {
				                        return "<a href= '/SisCapV2/admin/dtlLicitanteJuridico/" +data+ "' class='edit'><i class='glyphicon glyphicon-eye-open'></i></a>";
				                    },
				                    targets: 5
				                },
				                { orderable: false,  targets: [3, 4, 5] }
				            ],
				            	
				            createdRow: function (row, data, index) {
				                adminSecretaria.setClickInCreatedRow(row, data);
				            }
				});

		},
		
		setClickInCreatedRow: function(htmlRow, data) {
	        $(htmlRow).find('.del').click(function(event) {
	        	event.preventDefault();
	        	$.confirm({
	                title: 'Confirme o delete!',
	                text: 'Deseja deletar o registro?',
	                confirmButton: 'Delete',
	                cancelButton: 'Cancelar',
	                confirmButtonClass: 'btn-primary',
	                cancelButtonClass: 'btn-danger',
	                confirm: function() { adminSecretaria.ajaxDeleteSecretaria(data.id); },
	                cancel: function() { }
	            });
	        });
	    },
	    
	    ajaxDeleteSecretaria : function (id){
	    	$.ajax({
				url : '/SisCapV2/admin/secretaria/remove/'+id,
				type : 'POST',
				data : {
					id : id	
				},
				success : function(data) {
					$('#tableSecretaria').DataTable().ajax.reload();
				},
				error : function(data) {
					$("form").append(
						"<div class='alert alert-danger' role='alert' >Erro ao excluir</div>");
				},
				complete: function(data){
					$('#myModal').modal('hide');	
				}

	 			});
	    },
}