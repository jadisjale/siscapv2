var adminFornecedorJuridico = {

		index: function(){
			// input mask telefone
			 $('#telGerenteSec').mask('(99) 99999-9999');
			 $('#celGerenteSec').mask('(99) 99999-9999');
			 $('#telefoneTitularLicitante').mask('(99) 99999-9999');
			 $('#telefoneLicitante').mask('(99) 99999-9999');		 
			// fim mask telefone
			 
			 $('#cnpj').mask('99.999.999/9999-99');
			 
			 $('.datepicker').datepicker({
					language : 'pt-BR',
					format : 'dd/mm/yyyy',
					startDate : '-3d',
				});

				$(".datepicker").each(function(index, element) {
					var context = $(this);
					context.on("blur", function(e) {
						// The setTimeout is the key here.
						setTimeout(function() {
							if (!context.is(':focus')) {
								$(context).datepicker("hide");
							}
						}, 250);
					});
				});
				
				adminFornecedorJuridico.validador();
		},
		
		indexList : function(){
			adminFornecedorJuridico.loadTableJuridico();
		},
		
		loadTableJuridico: function(){
			$('#tableJuridica').dataTable( {
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
		        },
				   "ajax": {
				      "url": window.location.protocol + '//' + window.location.host + "/SisCapV2/admin/getJuridicos",
				      "dataSrc": "",
				   },
				   "columns": [
				      { "data": "nomeLicitante" },
				      { "data": "titularLicitante"},
				      { "data": "emailTitularLicitante"},
				      { "data": "id"},
				      { "data": "id"},
				      { "data": "id"}
				   ],
				   columnDefs: [
				                {
				                    render: function(data) {
				                        return "<a href= '/SisCapV2/admin/getLicitanteJuridico/" +data+ "' class='edit'><i class='glyphicon glyphicon-pencil'></i></a>";
				                    },
				                    targets: 3,
				                    className: "dt-body-center"
				                },
				                {
				                    render: function(data) {
				                        return "<a href= 'javascript:void(0);' class='del'><i class='glyphicon glyphicon-remove'></i></a>"
				                    },
				                    targets: 4,
				                    className: "dt-body-center"
				                },
				                {
				                    render: function(data) {
				                        return "<a href= '/SisCapV2/admin/dtlLicitanteJuridico/" +data+ "' class='edit'><i class='glyphicon glyphicon-eye-open'></i></a>";
				                    },
				                    targets: 5,
				                    className: "dt-body-center"
				                },
				                { orderable: false,  targets: [3, 4, 5] }
				            ],
				            	
				            createdRow: function (row, data, index) {
				                adminFornecedorJuridico.setClickInCreatedRow(row, data);
				            }
				});
	
		},
		
		setClickInCreatedRow: function(htmlRow, data) {
	        $(htmlRow).find('.del').click(function(event) {
	        	event.preventDefault();
	        	$.confirm({
	                title: 'Confirme o delete!',
	                text: 'Deseja deletar o registro?',
	                confirmButton: 'Delete',
	                cancelButton: 'Cancelar',
	                confirmButtonClass: 'btn-primary',
	                cancelButtonClass: 'btn-danger',
	                confirm: function() { adminFornecedorJuridico.ajaxDeleteJuridico(data.id); },
	                cancel: function() { }
	            });
	        });
	    },
	    
	    ajaxDeleteJuridico : function (id){
	    	$.ajax({
				url : '/SisCapV2/admin/getJuridico/remove/'+id,
				type : 'POST',
				data : {
					id : id	
				},
				success : function(data) {
					$('#tableJuridica').DataTable().ajax.reload();
				},
				error : function(data) {
					$("form").append(
						"<div class='alert alert-danger' role='alert' >Erro ao excluir</div>");
				},
				complete: function(data){
					$('#myModal').modal('hide');	
				}

	 			});
	    },
		
		// se correto retorna true
		valideEmail : function(email) {
			er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
			if (er.exec(email))
				return true;
			else
				return false;
		},
		
		isCNPJValid : function(cnpjParam) {
			cnpjParam = cnpjParam.replace(/[^\d]+/g, '');
			if (cnpjParam == '')
				return false;
			if (cnpjParam.length != 14)
				return false;
			if (cnpjParam == "00000000000000" || cnpjParam == "11111111111111"
					|| cnpjParam == "22222222222222" || cnpjParam == "33333333333333"
					|| cnpjParam == "44444444444444" || cnpjParam == "55555555555555"
					|| cnpjParam == "66666666666666" || cnpjParam == "77777777777777"
					|| cnpjParam == "88888888888888" || cnpjParam == "99999999999999")
				return false;

			tamanho = cnpjParam.length - 2
			numeros = cnpjParam.substring(0, tamanho);
			digitos = cnpjParam.substring(tamanho);
			soma = 0;
			pos = tamanho - 7;
			for (i = tamanho; i >= 1; i--) {
				soma += numeros.charAt(tamanho - i) * pos--;
				if (pos < 2)
					pos = 9;
			}
			resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			if (resultado != digitos.charAt(0))
				return false;

			tamanho = tamanho + 1;
			numeros = cnpjParam.substring(0, tamanho);
			soma = 0;
			pos = tamanho - 7;
			for (i = tamanho; i >= 1; i--) {
				soma += numeros.charAt(tamanho - i) * pos--;
				if (pos < 2)
					pos = 9;
			}
			resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
			if (resultado != digitos.charAt(1))
				return false;

			return true;
		},
			
		validador : function() {
			
			$('#submitSaveJuridico').click(function(event){
				event.preventDefault();
							
				var nomeLicitatne = $('#nomeLicitante').val();
				var titularLicitante = $('#titularLicitante').val();
				var telefoneLicitante = $('#telefoneLicitante').val();
				var faxLicitante = $('#faxLicitante').val();
				var emailLicitante = $('#emailLicitante').val();
				var telefoneTitularLicitante = $('#telefoneTitularLicitante').val();
				var emailTitularLicitante = $('#emailTitularLicitante').val();
				var rua = $('#rua').val();
				var bairro = $('#bairro').val();
				var cidade = $('#cidade').val();
				var estado = $('#estado').val();
				var cep = $('#cep').val();
				var cnpj = $('#cnpj').val();
				var expedicaoForn = $('#expedicaoForn').val();
				var numRegstroForn = $('#numRegstroForn').val();
				var validadeForn = $('#validadeForn').val();
				
				$( ".alert-danger" ).remove();
				
				if(nomeLicitatne.length > 3 && nomeLicitatne != null){
					adminUser.removeBorderMessage('.form-nome-licitante', '#border-nome-licitante', '#message-nome-licitante');
					if(titularLicitante.length > 3 && titularLicitante != null){
						adminUser.removeBorderMessage('.form-titular-licitante', '#border-titular-licitante', '#message-titular-licitante');
						if(telefoneLicitante.length > 3 && telefoneLicitante != null){
							adminUser.removeBorderMessage('.form-telefone-licitante', '#border-telefone-licitante', '#message-telefone-licitante');
							if(faxLicitante.length > 3 && faxLicitante != null){
								adminUser.removeBorderMessage('.form-fax-licitante', '#border-fax-licitante', '#message-fax-licitante');
								if(adminSecretaria.valideEmail(emailLicitante)== true){
									adminUser.removeBorderMessage('.form-email-licitante', '#border-email-licitante', '#message-email-licitante');
									if(telefoneTitularLicitante.length > 3 && telefoneTitularLicitante != null){
										adminUser.removeBorderMessage('.form-telefone-titular', '#border-telefone-titular', '#message-telefone-titular');
										if(adminSecretaria.valideEmail(emailTitularLicitante)== true){
											adminUser.removeBorderMessage('.form-email-titular', '#border-email-titular', '#message-email-titular');
											if(rua.length > 3 && rua != null){
												adminUser.removeBorderMessage('.form-rua', '#border-rua', '#message-rua');
												if(bairro.length > 3 && bairro != null){
													adminUser.removeBorderMessage('.form-bairro', '#border-bairro', '#message-bairro');
													if(cidade.length > 3 && cidade != null){
														adminUser.removeBorderMessage('.form-cidade', '#border-cidade', '#message-cidade');
														if(estado.length > 3 && estado != null){
															adminUser.removeBorderMessage('.form-estado', '#border-estado', '#message-estado');
															if(cep.length > 3 && cep != null){
																adminUser.removeBorderMessage('.form-cep', '#border-cep', '#message-cep');
																if(adminFornecedorJuridico.isCNPJValid(cnpj) == true){
																	adminUser.removeBorderMessage('.form-cnpj', '#border-cnpj', '#message-cnpj');
																	if(expedicaoForn.length > 3 && expedicaoForn != null){
																		adminUser.removeBorderMessage('.form-expedicaoForn', '#border-expedicaoForn', '#message-expedicaoForn');
																		if(numRegstroForn.length > 3 && numRegstroForn != null){
																			adminUser.removeBorderMessage('.form-numRegstroForn', '#border-numRegstroForn', '#message-numRegstroForn');
																			if(validadeForn.length > 3 && validadeForn != null){
																				adminUser.removeBorderMessage('.form-validadeForn', '#bordervalidadeForn', '#message-validadeForn');
																				adminFornecedorJuridico.ajaxSaveFornecedorJuridico();
																			} else {
																					adminUser.addBorderMessage('.form-validadeForn', '#bordervalidadeForn', '#message-validadeForn', 'Validade Fornecedor', '#validadeForn');
																				} 
																			} else {
																				adminUser.addBorderMessage('.form-numRegstroForn', '#border-numRegstroForn', '#message-numRegstroForn', 'Número Registro Fornecedor', '#numRegstroForn');
																			}
																		} else {
																			adminUser.addBorderMessage('.form-expedicaoForn', '#border-expedicaoForn', '#message-expedicaoForn', 'Expedição Fornecedor', '#expedicaoForn');
																		}
																	} else {
																		adminUser.addBorderMessage('.form-cnpj', '#border-cnpj', '#message-cnpj', 'CNPJ', '#cnpj');
																	}
																} else {
																	adminUser.addBorderMessage('.form-cep', '#border-cep', '#message-cep', 'CEP', '#cep');
																}
															} else {
																adminUser.addBorderMessage('.form-estado', '#border-estado', '#message-estado', 'Estado', '#estado');
															}
														} else {
															adminUser.addBorderMessage('.form-cidade', '#border-cidade', '#message-cidade', 'Cidade', '#cidade');
														}
													} else {
														adminUser.addBorderMessage('.form-bairro', '#border-bairro', '#message-bairro', 'Bairro', '#bairro');
													}
												} else {
													adminUser.addBorderMessage('.form-rua', '#border-rua', '#message-rua', 'Rua', '#rua');
												}
											}else {
												adminUser.addBorderMessage('.form-email-titular', '#border-email-titular', '#message-email-titular', 'Email Titular', '#emailTitularLicitante');
											}
										} else {
											adminUser.addBorderMessage('.form-telefone-titular', '#border-telefone-titular', '#message-telefone-titular', 'Telefone Celular', '#telefoneTitularLicitante');
										}
									}else{
										adminUser.addBorderMessage('.form-email-licitante', '#border-email-licitante', '#message-email-licitante', 'Email Licitante', '#emailLicitante');
									}
								} else {
									adminUser.addBorderMessage('.form-fax-licitante', '#border-fax-licitante', '#message-fax-licitante', 'Fax Licitante', '#faxLicitante');
								}
							} else {
								adminUser.addBorderMessage('.form-telefone-licitante', '#border-telefone-licitante', '#message-telefone-licitante', 'Telefone Licitante', '#telefoneLicitante');
							}
						} else {
							adminUser.addBorderMessage('.form-titular-licitante', '#border-nome-licitante', '#message-titular-licitante', 'Titular Licitante', '#titularLicitante');
						}
					} else {
						adminUser.addBorderMessage('.form-nome-licitante', '#border-nome-licitante', '#message-nome-licitante', 'Nome Licitante', '#nomeLicitante');
					}
			});
			
		},
		
		ajaxSaveFornecedorJuridico : function() {
			$.ajax({
				url : '/SisCapV2/admin/saveFornecedorJuridico',
				type : 'POST',
				data : $('#saveFormFornecedorJuridico').serialize(),
				success : function(data) {
					adminUser.messageSuccess($('#nomeLicitante').val());
					adminFornecedorJuridico.clearInputs();
				},
				error : function(data) {
					console.log("error");
					$("form").append("<div class='alert alert-danger' role='alert' >Registro duplicado!</div>");
					},
					complete : function(data) {

					}

			});
		},
		
		clearInputs : function (){
			$('#nomeLicitante').val("");
			$('#titularLicitante').val("");
			$('#telefoneLicitante').val("");
			$('#faxLicitante').val("");
			$('#emailLicitante').val("");
			$('#telefoneTitularLicitante').val("");
			$('#emailTitularLicitante').val("");
			$('#rua').val("");
			$('#bairro').val("");
			$('#cidade').val("");
			$('#estado').val("");
			$('#cep').val("");
			$('#cnpj').val("");
			$('#expedicaoForn').val("");
			$('#numRegstroForn').val("");
			$('#validadeForn').val("");
		}

}