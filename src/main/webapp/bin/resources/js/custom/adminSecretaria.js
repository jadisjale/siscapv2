var adminSecretaria = {

	index : function() {
		adminSecretaria.validador();
		$('#gerenteSec').focus();
		$('#telGerenteSec').mask('(99) 9999-9999');
		$('#celGerenteSec').mask('(99) 99999-9999');
	},

	indexList : function() {
		adminSecretaria.loadTableSecretaria();
	},
	
	loadTableSecretaria: function(){
		$('#tableSecretaria').dataTable( {
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
	        },
			   "ajax": {
			      "url": "/SisCapV2/secretaria/getSecretarias",
			      "dataSrc": "",
			   },
			   "columns": [
			      { "data": "licitanteJuridico.orgaoLicitante.nomeLicitante" },
			      { "data": "gerenteSec"},
			      { "data": "emailGerenteSec"},
			      { "data": "id"},
			      { "data": "id"},
			      { "data": "id"}
			   ],
			   columnDefs: [
			                {
			                    render: function(data) {
			                        return "<a href= '/SisCapV2/admin/getSecretaria/" +data+ "' class='edit'><i class='glyphicon glyphicon-pencil'></i></a>";
			                    },
			                    targets: 3
			                },
			                {
			                    render: function(data) {
			                        return "<a href= 'javascript:void(0);' class='del'><i class='glyphicon glyphicon-remove'></i></a>"
			                    },
			                    targets: 4
			                },
			                {
			                    render: function(data) {
			                        return "<a href= '/SisCapV2/admin/dtlLicitanteJuridico/" +data+ "' class='edit'><i class='glyphicon glyphicon-eye-open'></i></a>";
			                    },
			                    targets: 5
			                },
			                { orderable: false,  targets: [3, 4, 5] }
			            ],
			            	
			            createdRow: function (row, data, index) {
			                adminSecretaria.setClickInCreatedRow(row, data);
			            }
			});

	},
	
	setClickInCreatedRow: function(htmlRow, data) {
        $(htmlRow).find('.del').click(function(event) {
        	event.preventDefault();
        	$.confirm({
                title: 'Confirme o delete!',
                text: 'Deseja deletar o registro?',
                confirmButton: 'Delete',
                cancelButton: 'Cancelar',
                confirmButtonClass: 'btn-primary',
                cancelButtonClass: 'btn-danger',
                confirm: function() { adminSecretaria.ajaxDeleteSecretaria(data.id); },
                cancel: function() { }
            });
        });
    },
    
    ajaxDeleteSecretaria : function (id){
    	$.ajax({
			url : '/SisCapV2/admin/secretaria/remove/'+id,
			type : 'POST',
			data : {
				id : id	
			},
			success : function(data) {
				$('#tableSecretaria').DataTable().ajax.reload();
			},
			error : function(data) {
				$("form").append(
					"<div class='alert alert-danger' role='alert' >Erro ao excluir</div>");
			},
			complete: function(data){
				$('#myModal').modal('hide');	
			}

 			});
    },

	carregarJuridico : function(idLicitanteJuridico) {
		$.ajax({
			url : '/SisCapV2/admin/getJuridicosIdName',
			type : 'GET',
			dataType : 'json',
			success : function(data) {
				var cmb = "";
				$.each(data, function(i, value) {
					if(value.id != idLicitanteJuridico){
						cmb = cmb + '<option value="' + value.id + '">'
						+ value.orgaoLicitante.nomeLicitante + '</option>';
					} 
				});
				$('#fkJuridico').append(cmb);
			},
			complete : function() {
				$('#fkJuridico').select2();
			}
		});
	},

	validador : function() {
	
		var validator = $('#saveFormSecretaria').validate({
			
			rules: {
				gerenteSec: {
			        required: true,
			        minlength: 3
			    },
			    celGerenteSec: {
			        required: true
			    },
			    telGerenteSec: {
			        required: true,
			        minlength: 3
			    },
			    emailGerenteSec: {
			        required: true,
			        email: true
			    },
			    fkJuridico: {
			        required: true,
			    },
			   
			},
			
			highlight: function(element) {
			    var id_attr = "#" + $( element ).attr("id") + "1";
			    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			    $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');         
			},
			
			unhighlight: function(element) {
			    var id_attr = "#" + $( element ).attr("id") + "1";
			    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			    $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');         
			},
			
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
			    if(element.length) {
			        error.insertAfter(element);
			    } else {
			    error.insertAfter(element);
			    }
			}, 
			
			submitHandler: function( form ) {
				user.saveUser();
				return false;
			},
			
		});
	},

	ajaxSaveSecretaria : function() {
		$
				.ajax({
					url : '/SisCapV2/admin/saveSecretaria',
					type : 'POST',
					data : $('#saveFormSecretaria').serialize(),
					success : function(data) {
						adminUser.messageSuccess($('#gerenteSec').val());
						adminSecretaria.clearInputs();
					},
					error : function(data) {
						$("form")
								.append(
										"<div class='alert alert-danger' role='alert' >Registro duplicado!</div>");
					},
					complete : function(data) {

					}

				});
	},

	// se correto retorna true
	valideEmail : function(email) {
		er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
		if (er.exec(email))
			return true;
		else
			return false;
	},

	// se correto retorna true
	isCNPJValid : function(cnpj) {
		cnpj = cnpj.replace(/[^\d]+/g, '');
		if (cnpj == '')
			return false;
		if (cnpj.length != 14)
			return false;
		if (cnpj == "00000000000000" || cnpj == "11111111111111"
				|| cnpj == "22222222222222" || cnpj == "33333333333333"
				|| cnpj == "44444444444444" || cnpj == "55555555555555"
				|| cnpj == "66666666666666" || cnpj == "77777777777777"
				|| cnpj == "88888888888888" || cnpj == "99999999999999")
			return false;

		tamanho = cnpj.length - 2
		numeros = cnpj.substring(0, tamanho);
		digitos = cnpj.substring(tamanho);
		soma = 0;
		pos = tamanho - 7;
		for (i = tamanho; i >= 1; i--) {
			soma += numeros.charAt(tamanho - i) * pos--;
			if (pos < 2)
				pos = 9;
		}
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(0))
			return false;

		tamanho = tamanho + 1;
		numeros = cnpj.substring(0, tamanho);
		soma = 0;
		pos = tamanho - 7;
		for (i = tamanho; i >= 1; i--) {
			soma += numeros.charAt(tamanho - i) * pos--;
			if (pos < 2)
				pos = 9;
		}
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(1))
			return false;

		return true;
	},

	clearInputs : function() {
		$('#gerenteSec').val('');
		$('#celGerenteSec').val('');
		$('#emailGerenteSec').val('');
		$('#telGerenteSec').val('');
	}

}