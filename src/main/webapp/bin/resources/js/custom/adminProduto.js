var adminProduto = {
	index : function() {
		$("#valor").maskMoney({thousands:'', decimal:'.'});
		$('#descricao').focus();
		adminProduto.validador();
		adminProduto.carregarNomesFornecedores();
	},
	
	indexList : function(){
		adminProduto.listProduto();
	},
	
	
	saveProduto : function(){
		$.ajax({
			url : '/SisCapV2/admin/saveProduto',
			type : 'POST',
			data : $('#saveFormProduto').serialize(),
			success : function(data) {
				adminUser.messageSuccess($('#descricao').val());
				adminProduto.clearInputs();
			},
			
			error : function(data) {
				$("form").append(
					"<div class='alert alert-danger' role='alert' >Erro ao salvar o produto!</div>");
			},
			
			complete : function() {
				
			}
		});
	},
	
listProduto : function (){
		
		$('#tableProduto').dataTable( {
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
	        },
			   "ajax": {
			      "url": window.location.protocol + '//' + window.location.host + "/SisCapV2/admin/getProdutos",
			      "dataSrc": "",
			   },
			   "columns": [
			      { "data": "descricao" },
			      { "data": "valor"},
			      { "data": "fornecedor.licitanteFisico.orgaoLicitante.nomeLicitante" || "fornecedor.licitanteJuridico.orgaoLicitante.nomeLicitante"},
			      { "data": "id"},
			      { "data": "id"}
			   ],
			   columnDefs: [
			                {
			                    render: function(data) {
			                        return "<a href= '/SisCapV2/admin/getProduto/" +data+ "' class='edit'><i class='glyphicon glyphicon-pencil'></i></a>";
			                    },
			                    targets: 3
			                },
			                {
			                    render: function(data) {
			                        return "<a href= 'javascript:void(0);' class='del'><i class='glyphicon glyphicon-remove'></i></a>"
			                    },
			                    targets: 4
			                },
			                
			                { orderable: false,  targets: [3, 4] }
			            ],
			            
			            createdRow: function (row, data, index) {
			                adminProduto.setClickInCreatedRow(row, data);
			            },  
			            
			});
	},
	
	setClickInCreatedRow: function(htmlRow, data) {
        $(htmlRow).find('.del').click(function(event) {
            
        	event.preventDefault();
        	$.confirm({
                title: 'Confirme o delete!',
                text: 'Deseja deletar o registro?',
                confirmButton: 'Delete',
                cancelButton: 'Cancelar',
                confirmButtonClass: 'btn-primary',
                cancelButtonClass: 'btn-danger',
                confirm: function() { adminProduto.ajaxDeleteProduto(data.id); },
                cancel: function() { }
            });
        });
    },
    
    ajaxDeleteProduto : function (id){
    	$.ajax({
			url : '/SisCapV2/admin//getProduto/remove/'+id,
			type : 'POST',
			data : {
				id : id	
			},
			success : function(data) {
				$('#tableProduto').DataTable().ajax.reload();
			},
			error : function(data) {
				$("form").append(
					"<div class='alert alert-danger' role='alert' >Erro ao excluir</div>");
			},
			complete: function(data){
				
			}

 			});
    },
	
	clearInputs : function(){
		$("#id").val('');
		$( "#descricao" ).val('');
		$('#valor').val('');
		
	},
	
	carregarNomesFornecedores : function(){
		$.ajax({
			url : '/SisCapV2/admin/getNomeFornecedor',
			type : 'GET',
			dataType : 'json',
			success : function(data) {
				var cmb = "";
				$.each(data, function(i, value) {
					cmb = cmb + '<option value="' + value.id + '">'
							+ value.nomeLicitante + '</option>';
				});
				$('#fkFornecedor').append(cmb);
			},
			complete : function() {
				$('#fkFornecedor').select2();
			}
		});
	},

	validador : function() {
		var validator = $('#saveFormProduto').validate({
			
		rules: {
			fkFornecedor: {
		        required: true,
		    },
		    valor: {
		        required: true,
		    },
		    descricao: {
		        required: true,
		        minlength: 3
		    },
		},
		
		highlight: function(element) {
		    var id_attr = "#" + $( element ).attr("id") + "1";
		    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		    $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');         
		},
		
		unhighlight: function(element) {
		    var id_attr = "#" + $( element ).attr("id") + "1";
		    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		    $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');         
		},
		
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
		    if(element.length) {
		        error.insertAfter(element);
		    } else {
		    error.insertAfter(element);
		    }
		}, 
		
		submitHandler: function( form ) {
			juridico.ajaxSaveFornecedorJuridico();
			return false;
		},
		
		});

	},
}