<%@include file="home.jsp"%>

<div class="container">
	<h2>
		Cadastro de Produto
		<h2>
</div>
<hr>

<div class="container">
	<form role="form" id="saveFormProduto">

		<input type="hidden" name="id" value="${id}"></input>

		<div class="form-group form-descricao">
			<label for="email">Descri��o:</label> 
			<input type="text" class="form-control" id="descricao" name="descricao">
			<span id="border-descricao"></span>
			<span id="message-descricao"></span>
		</div>
		
		<div class="form-group form-valor">
			<label for="email">Valor:</label> 
			<input type="text" class="form-control" id="valor" name="valor">
			<span id="border-valor"></span>
			<span id="message-valor"></span>
		</div>
		
		<div class="form-group form-fkFornecedor">
			<label for="email">Fornecedor:</label>
			<select id="fkFornecedor" class="form-control selectField" name="fkFornecedor">	
				
			</select>
			<span id="border-fkFornecedor"></span> 
			<span id="message-fkFornecedor"></span>
		</div>
		
		<button type="submit" class="btn btn-primary"
			id="submitSaveProduto">Salvar</button>
		
	</form>
</div>
<%@include file="footer.jsp"%>
<script type="text/javascript">
	$(document).ready(function() {
		adminProduto.index();
	});
</script>