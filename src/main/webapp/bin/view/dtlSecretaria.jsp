<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

</body>
</html><%@include file="home.jsp"%>
<div class="container">
	<h2>Detalhes da Secretria</h2>
</div>
<hr>

<div class="container">

	<div class="panel panel-primary">
		<div class="panel-heading">Gerente Secretaria</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<label>Nome Gerente:</label> <label id='resposta'>${nomeLicitante}</label>
				</div>
				<div class="col-md-6">
					<label>Celular gerente:</label> <label id='resposta'>${titularLicitante}</label>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<label>Telefone Gerente:</label> <label id='resposta'>${telefoneLicitante}</label>

				</div>
				<div class="col-md-6">
					<label>Email Gerente:</label> <label id='resposta'>${faxLicitante}</label>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-primary">
		<div class="panel-heading">Secretaria</div>
		<div class="panel-body">
			<div class="row">
					<div class="col-md-6">
						<label>Nome:</label> <label id='resposta'>${rua}</label>
				</div>
			</div>

			<div class="form-group">
		<div>
			<br>
			<button type="submit" class="btn btn-primary" id="detalheJuridico">Voltar</button>
		</div>
	</div>

</div>
<hr>
<%@include file="footer.jsp"%>
<script>
	$(document).ready(function() {
		$("#detalheJuridico").click(function() {
			window.location = "/SisCapV2/admin/listLicitanteJuridico";
		});
	});
</script>