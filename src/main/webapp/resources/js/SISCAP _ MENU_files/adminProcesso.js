var adminProcesso = {

	indexList : function() {
		adminProcesso.listProcesso();
	},
	
	listProcesso : function (){
		$('#tableProcesso').dataTable( {
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
	        },
			   "ajax": {
			      "url": window.location.protocol + '//' + window.location.host + "/SisCapV2/admin/getProcesso",
			      "dataSrc": "",
			   },
			   "columns": [
			      { "data": "numeroProcesso" },
			      { "data": "secretaria.nomeLicitante"},
			      { "data": "secretaria2.nomeLicitante"},
			      { "data": "id"},
			      { "data": "id"},
			      { "data": "id"}
			   ],
			   columnDefs: [
			                {
			                    render: function(data) {
			                        return "<a href= '/SisCapV2/admin/getSecretaria/" +data+ "' class='edit'><i class='glyphicon glyphicon-pencil'></i></a>";
			                    },
			                    targets: 3
			                },
			                {
			                    render: function(data) {
			                        return "<a href= '/SisCapV2/admin/detalheSecretaria/" +data+ "' class='open'><i class='glyphicon glyphicon-info-sign'></i></a>"
			                    },
			                    targets: 4
			                },
			                {
			                    render: function(data) {
			                        return "<a href= 'javascript:void(0);' class='del'><i class='glyphicon glyphicon-remove'></i></a>"
			                    },
			                    targets: 5
			                },
			                { orderable: false,  targets: [3, 4, 5] }
			            ],
			            
			            createdRow: function (row, data, index) {
			                adminProcesso.setClickInCreatedRow(row, data);
			            },  
			            
			});
	},
	
	setClickInCreatedRow: function(htmlRow, data) {
        $(htmlRow).find('.del').click(function(event) {
            
        	event.preventDefault();
        	$.confirm({
                title: 'Confirme o delete!',
                text: 'Deseja deletar o registro?',
                confirmButton: 'Delete',
                cancelButton: 'Cancelar',
                confirmButtonClass: 'btn-primary',
                cancelButtonClass: 'btn-danger',
                confirm: function() { adminProcesso.ajaxDeleteProcesso(data.id); },
                cancel: function() { }
            });
        });
    },
    
    ajaxDeleteProcesso : function (id){
    	$.ajax({
			url : '/SisCapV2/admin/getProcesso/remove/'+id,
			type : 'POST',
			data : {
				id : id	
			},
			success : function(data) {
				$('#tableProcesso').DataTable().ajax.reload();
			},
			error : function(data) {
				$("form").append(
					"<div class='alert alert-danger' role='alert' >Erro ao excluir</div>");
			},
			complete: function(data){
				
			}

 			});
    },

	index : function() {

		$('.datepicker').datepicker({
			language : 'pt-BR',
			format : 'dd/mm/yyyy',
			startDate : '-3d',
		});

		$(".datepicker").each(function(index, element) {
			var context = $(this);
			context.on("blur", function(e) {
				// The setTimeout is the key here.
				setTimeout(function() {
					if (!context.is(':focus')) {
						$(context).datepicker("hide");
					}
				}, 250);
			});
		});

		$('#vlorEstimadoProc').maskMoney({
			thousands : '',
			decimal : '.'
		});

		adminProcesso.carregarSelect();

		adminProcesso.validador();
	},

	carregarSelect : function() {
		$.ajax({
			url : '/SisCapV2/admin/getProcessoSecretaria',
			type : 'GET',
			dataType : 'json',
			success : function(data) {
				var cmb = "";
				$.each(data, function(i, value) {
					cmb = cmb + '<option value="' + value.id + '">'
							+ value.licitanteJuridico.orgaoLicitante.nomeLicitante + '</option>';
				});
				$('#idSecretariaDois').append(cmb);
				$('#idSecretariaUm').append(cmb);
			},
			complete : function() {
				$('#idSecretariaUm').select2();
				$('#idSecretariaDois').select2();
			}
		});
	},

	validador : function() {
		var validator = $('#saveFormProcesso').validate({
			
		rules: {
			inicioProcessoParam: {
		        required: true,
		    },
		    numeroProcesso: {
		        required: true
		    },
		    entradaProcessoParam: {
		        required: true,
		    },
		    vlorEstimadoProcParam: {
		        required: true,
		    },
		    numeroSolicProc:{
		    	required: true,
		    },
		    numModalProc: {
		        required: true,
		    },
		    modalidadeProc: {
		        required: true,
		    },
		    tipoProc: {
		        required: true,
		    },
		    origemRecProc: {
		        required: true,
		    },
		    comissaoRespProc: {
		        required: true,
		    },
		    objetoProc: {
		        required: true,
		    },
		    situacaoProc: {
		        required: true,
		        minlength: 3
		    },
		    observacaoProc: {
		    	required: true,
		    },
		    dataContratoParam: {
		        required: true,
		    },
		    id1: {
		        required: true,
		    },
		    id2: {
		        required: true,
		        minlength: 3
		    },
		},
		
		highlight: function(element) {
		    var id_attr = "#" + $( element ).attr("id") + "1";
		    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		    $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');         
		},
		
		unhighlight: function(element) {
		    var id_attr = "#" + $( element ).attr("id") + "1";
		    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		    $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');         
		},
		
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
		    if(element.length) {
		        error.insertAfter(element);
		    } else {
		    error.insertAfter(element);
		    }
		}, 
		
		submitHandler: function( form ) {
			alert("Passou da validacao");
			return false;
		},
		
		});
	},

	ajaxSaveProcesso : function() {
		$
				.ajax({
					url : '/SisCapV2/admin/saveProcesso',
					type : 'POST',
					data : $('#saveFormProcesso').serialize(),
					success : function(data) {
						console.log("success");
					},
					error : function(data) {
						console.log("error");
						$("form")
								.append(
										"<div class='alert alert-danger' role='alert' >Registro duplicado!</div>");
					},
					complete : function(data) {

					}

				});
	},

}