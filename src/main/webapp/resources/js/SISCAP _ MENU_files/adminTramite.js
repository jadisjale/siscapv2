var adminTramite = {
		index : function () {
			adminTramite.ajaxLoadSecretaria();
			adminTramite.ajaxLoadProcesso();
			adminTramite.validation();
			adminTramite.loadTableTramite();
			
			$('.datepicker').datepicker({
				language : 'pt-BR',
				format : 'dd/mm/yyyy',
				startDate : '-3d',
			});

			$(".datepicker").each(function(index, element) {
				var context = $(this);
				context.on("blur", function(e) {
					setTimeout(function() {
						if (!context.is(':focus')) {
							$(context).datepicker("hide");
						}
					}, 250);
				});
			});
		},
		
		ajaxLoadSecretaria : function (){
			$.ajax({
				url : '/SisCapV2/admin/getSecretariasIdName',
				type : 'GET',
				success : function(data) {
					var cmb = "";
					$.each(data, function(i, value) {
						cmb = cmb + '<option value="' + value.id + '">'
						+ value.licitanteJuridico.orgaoLicitante.nomeLicitante + '</option>';
					});
					$('#fkSecretaria').append(cmb);
				},
				error : function(data) {
					console.log(data);
				},
	 		});
		},
		
		ajaxLoadProcesso : function (){
			$.ajax({
				url : '/SisCapV2/admin/getProcessoNumId',
				type : 'GET',
				success : function(data) {
					var cmb = "";
					$.each(data, function(i, value) {
						cmb = cmb + '<option value="' + value.id + '">'
						+ value.numeroProcesso + '</option>';
					});
					$('#fkProcesso').append(cmb);
				},
				error : function(data) {
					console.log(data);
				},
	 		});
		},

		validation : function(){
			$('#submitSaveTramite').click(function(event){
				event.preventDefault();
				$(".alert-danger").remove();
				var motivo = $('#motivoTramit').val();
				var responsavel = $('#responsTramit').val();
				var retorno = $('#retornoTramit').val();
				var saida = $('#saidaTramit').val();
				var processo = $('#fkProcesso').val();
				var secretaria = $('#fkSecretaria').val();
				
				if(motivo.length > 3 && motivo != null){
					adminUser.removeBorderMessage('.form-motivoTramit','#border-motivoTramit', '#message-motivoTramit');
					if(responsavel.length > 3 && responsavel != null){
						adminUser.removeBorderMessage('.form-responsTramit','#border-responsTramit', '#message-responsTramit');
						if(retorno.length > 3 && retorno != null){
							adminUser.removeBorderMessage('.form-retornoTramit','#border-retornoTramit', '#message-retornoTramit');
							if(saida.length > 3 && saida != null){
								adminUser.removeBorderMessage('.form-saidaTramit','#border-saidaTramit', '#message-saidaTramit');
								if(processo != null){
									adminUser.removeBorderMessage('.form-fkProcesso','#border-fkProcesso', '#message-fkProcesso');
									if(secretaria != null){
										adminUser.removeBorderMessage('.form-fkSecretaria','#border-fkSecretaria', '#message-fkSecretaria');
										adminTramite.ajaxSaveSecretaria();
									}else {
										adminUser.addBorderMessage('.form-fkSecretaria','#border-fkSecretaria', '#message-fkSecretaria','Secretaria', '#fkSecretaria');
									}
								}else {
									adminUser.addBorderMessage('.form-fkProcesso','#border-fkProcesso', '#message-fkProcesso','Processo', '#fkProcesso');
								}
							}else {
								adminUser.addBorderMessage('.form-saidaTramit','#border-saidaTramit', '#message-saidaTramit','Saída', '#saidaTramit');
							}
						}else {
							adminUser.addBorderMessage('.form-retornoTramit','#border-retornoTramit', '#message-retornoTramit','Retorno', '#retornoTramit');
						}
					}else {
						adminUser.addBorderMessage('.form-responsTramit','#border-responsTramit', '#message-responsTramit','Responsável', '#responsTramit');
					}
				}else {
					adminUser.addBorderMessage('.form-motivoTramit','#border-motivoTramit', '#message-motivoTramit','Motivo', '#motivoTramit');
				}
			});
		},
		
		ajaxSaveSecretaria : function (){
			$.ajax({
				url : '/SisCapV2/admin/saveTramite',
				type : 'POST',
				data : $('#saveFormTramite').serialize(),
				success : function(data) {
					$('#motivoTramit').val("");
					$('#responsTramit').val("");
					$('#retornoTramit').val("");
					$('#saidaTramit').val("");
				},
				error : function(data) {
					$("form").append(
						"<div class='alert alert-danger' role='alert' >Registro duplicado!</div>");
				},
				complete: function(data){
						
				}

	 		});
		},
		
		loadTableTramite: function(){
			$('#tableTramite').dataTable( {
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
		        },
				   "ajax": {
				      "url": window.location.protocol + '//' + window.location.host + "/SisCapV2/admin/getTramites",
				      "dataSrc": "",
				   },
				   "columns": [
				      { "data": "motivoTramit" },
				      { "data": "responsTramit"},
				      { "data": "secretaria.licitanteJuridico.orgaoLicitante.nomeLicitante"},
				      { "data": "id"},
				      { "data": "id"},
				      { "data": "id"}
				   ],
				   columnDefs: [
				                {
				                    render: function(data) {
				                        return "<a href= '/SisCapV2/admin/getSecretaria/" +data+ "' class='edit'><i class='glyphicon glyphicon-pencil'></i></a>";
				                    },
				                    targets: 3
				                },
				                {
				                    render: function(data) {
				                        return "<a href= 'javascript:void(0);' class='del'><i class='glyphicon glyphicon-remove'></i></a>"
				                    },
				                    targets: 4
				                },
				                {
				                    render: function(data) {
				                        return "<a href= '/SisCapV2/admin/dtlLicitanteJuridico/" +data+ "' class='edit'><i class='glyphicon glyphicon-eye-open'></i></a>";
				                    },
				                    targets: 5
				                },
				                { orderable: false,  targets: [3, 4, 5] }
				            ],
				            	
				            createdRow: function (row, data, index) {
				                adminSecretaria.setClickInCreatedRow(row, data);
				            }
				});

		},
		
		setClickInCreatedRow: function(htmlRow, data) {
	        $(htmlRow).find('.del').click(function(event) {
	        	event.preventDefault();
	        	$.confirm({
	                title: 'Confirme o delete!',
	                text: 'Deseja deletar o registro?',
	                confirmButton: 'Delete',
	                cancelButton: 'Cancelar',
	                confirmButtonClass: 'btn-primary',
	                cancelButtonClass: 'btn-danger',
	                confirm: function() { adminSecretaria.ajaxDeleteSecretaria(data.id); },
	                cancel: function() { }
	            });
	        });
	    },
	    
	    ajaxDeleteSecretaria : function (id){
	    	$.ajax({
				url : '/SisCapV2/admin/secretaria/remove/'+id,
				type : 'POST',
				data : {
					id : id	
				},
				success : function(data) {
					$('#tableSecretaria').DataTable().ajax.reload();
				},
				error : function(data) {
					$("form").append(
						"<div class='alert alert-danger' role='alert' >Erro ao excluir</div>");
				},
				complete: function(data){
					$('#myModal').modal('hide');	
				}

	 			});
	    },

}