var adminFornecedorJuridico = {

		index: function(){
			// input mask telefone
			 $('#telGerenteSec').mask('(99) 99999-9999');
			 $('#celGerenteSec').mask('(99) 99999-9999');
			 $('#telefoneTitularLicitante').mask('(99) 99999-9999');
			 $('#telefoneLicitante').mask('(99) 99999-9999');		 
			// fim mask telefone
			 
			 $('#cnpj').mask('99.999.999/9999-99');
			 
			 $('.datepicker').datepicker({
					language : 'pt-BR',
					format : 'dd/mm/yyyy',
					startDate : '-3d',
				});

				$(".datepicker").each(function(index, element) {
					var context = $(this);
					context.on("blur", function(e) {
						// The setTimeout is the key here.
						setTimeout(function() {
							if (!context.is(':focus')) {
								$(context).datepicker("hide");
							}
						}, 250);
					});
				});
				
				adminFornecedorJuridico.validador();
		},
		
		indexList : function(){
			adminFornecedorJuridico.loadTableJuridico();
		},
		
		loadTableJuridico: function(){
			$('#tableJuridica').dataTable( {
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
		        },
				   "ajax": {
				      "url": window.location.protocol + '//' + window.location.host + "/SisCapV2/admin/getJuridicos",
				      "dataSrc": "",
				   },
				   "columns": [
				      { "data": "nomeLicitante" },
				      { "data": "titularLicitante"},
				      { "data": "emailTitularLicitante"},
				      { "data": "id"},
				      { "data": "id"},
				      { "data": "id"}
				   ],
				   columnDefs: [
				                {
				                    render: function(data) {
				                        return "<a href= '/SisCapV2/admin/getLicitanteJuridico/" +data+ "' class='edit'><i class='glyphicon glyphicon-pencil'></i></a>";
				                    },
				                    targets: 3
				                },
				                {
				                    render: function(data) {
				                        return "<a href= 'javascript:void(0);' class='del'><i class='glyphicon glyphicon-remove'></i></a>"
				                    },
				                    targets: 4
				                },
				                {
				                    render: function(data) {
				                        return "<a href= '/SisCapV2/admin/dtlLicitanteJuridico/" +data+ "' class='edit'><i class='glyphicon glyphicon-eye-open'></i></a>";
				                    },
				                    targets: 5
				                },
				                { orderable: false,  targets: [3, 4, 5] }
				            ],
				            	
				            createdRow: function (row, data, index) {
				                adminFornecedorJuridico.setClickInCreatedRow(row, data);
				            }
				});
	
		},
		
		setClickInCreatedRow: function(htmlRow, data) {
	        $(htmlRow).find('.del').click(function(event) {
	        	event.preventDefault();
	        	$.confirm({
	                title: 'Confirme o delete!',
	                text: 'Deseja deletar o registro?',
	                confirmButton: 'Delete',
	                cancelButton: 'Cancelar',
	                confirmButtonClass: 'btn-primary',
	                cancelButtonClass: 'btn-danger',
	                confirm: function() { adminFornecedorJuridico.ajaxDeleteJuridico(data.id); },
	                cancel: function() { }
	            });
	        });
	    },
	    
	    ajaxDeleteJuridico : function (id){
	    	$.ajax({
				url : '/SisCapV2/admin/getJuridico/remove/'+id,
				type : 'POST',
				data : {
					id : id	
				},
				success : function(data) {
					$('#tableJuridica').DataTable().ajax.reload();
				},
				error : function(data) {
					$("form").append(
						"<div class='alert alert-danger' role='alert' >Erro ao excluir</div>");
				},
				complete: function(data){
					$('#myModal').modal('hide');	
				}

	 			});
	    },
			
		validador : function() {
		
//			var nomeLicitatne = $('#nomeLicitante').val();
//			var titularLicitante = $('#titularLicitante').val();
//			var telefoneLicitante = $('#telefoneLicitante').val();
//			var faxLicitante = $('#faxLicitante').val();
//			var emailLicitante = $('#emailLicitante').val();
//			var telefoneTitularLicitante = $('#telefoneTitularLicitante').val();
//			var emailTitularLicitante = $('#emailTitularLicitante').val();
//			var rua = $('#rua').val();
//			var bairro = $('#bairro').val();
//			var cidade = $('#cidade').val();
//			var estado = $('#estado').val();
//			var cep = $('#cep').val();
//			var cnpj = $('#cnpj').val();
//			var expedicaoForn = $('#expedicaoForn').val();
//			var numRegstroForn = $('#numRegstroForn').val();
//			var validadeForn = $('#validadeForn').val();
			
			var validator = $('#saveFormFornecedorJuridico').validate({
				
		        rules: {
		        	nomeLicitante: {
		                required: true,
		                minlength: 3
		            },
		            titularLicitante: {
		                required: true,
		                minlength: 3
		            },
		            telefoneLicitante: {
		                required: true,
		                minlength: 3
		            },
		            faxLicitante: {
		                required: true,
		                minlength: 3
		            },
		            emailLicitante: {
		            	required: true,
		            	email: true
		            },
		            telefoneTitularLicitante: {
		                required: true,
		                minlength: 3
		            },
		            emailTitularLicitante: {
		            	required: true,
		            	email: true
		            },
		            rua: {
		                required: true,
		                minlength: 3
		            },
		            bairro: {
		                required: true,
		                minlength: 3
		            },
		            cidade: {
		                required: true,
		                minlength: 3
		            },
		            estado: {
		            	required: true
		            },
		            cep: {
		                required: true,
		            },
		            cnpj: {
		            	cnpj: true, 
		            	required: true
		            },
		            expedicaoFornParam: {
		                required: true,
		            },
		            numRegstroForn: {
		            	required: true,
		            },
		            validadeFornParam: {
		                required: true,
		            },
		        },
		        
		        highlight: function(element) {
		            var id_attr = "#" + $( element ).attr("id") + "1";
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');         
		        },
		        
		        unhighlight: function(element) {
		            var id_attr = "#" + $( element ).attr("id") + "1";
		            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');         
		        },
		        
		        errorElement: 'span',
	            errorClass: 'help-block',
	            errorPlacement: function(error, element) {
	                if(element.length) {
	                    error.insertAfter(element);
	                } else {
	                error.insertAfter(element);
	                }
	            }, 
		        
		        submitHandler: function( form ) {
		        	adminFornecedorFisico.ajaxSaveFornecedorFisisco();
		        	return false;
		        },
		        
		    });
			
		},
		
		ajaxSaveFornecedorJuridico : function() {
			$.ajax({
				url : '/SisCapV2/admin/saveFornecedorJuridico',
				type : 'POST',
				data : $('#saveFormFornecedorJuridico').serialize(),
				success : function(data) {
					adminUser.messageSuccess($('#nomeLicitante').val());
					adminFornecedorJuridico.clearInputs();
				},
				error : function(data) {
					console.log("error");
					$("form").append("<div class='alert alert-danger' role='alert' >Registro duplicado!</div>");
					},
					complete : function(data) {

					}

			});
		},
		
		clearInputs : function (){
			$('#nomeLicitante').val("");
			$('#titularLicitante').val("");
			$('#telefoneLicitante').val("");
			$('#faxLicitante').val("");
			$('#emailLicitante').val("");
			$('#telefoneTitularLicitante').val("");
			$('#emailTitularLicitante').val("");
			$('#rua').val("");
			$('#bairro').val("");
			$('#cidade').val("");
			$('#estado').val("");
			$('#cep').val("");
			$('#cnpj').val("");
			$('#expedicaoForn').val("");
			$('#numRegstroForn').val("");
			$('#validadeForn').val("");
		}

}