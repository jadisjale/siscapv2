var adminTramite = {
		index : function () {
			adminTramite.ajaxLoadSecretaria();
			adminTramite.ajaxLoadProcesso();
			adminTramite.loadTableTramite();
			adminTramite.validation();
			
			$('.datepicker').datepicker({
				language : 'pt-BR',
				format : 'dd/mm/yyyy',
				autoclose: true,
			}).on('changeDate', function (ev) {
			    $(this).datepicker('hide');
			});

		},
		
		ajaxLoadSecretaria : function (){
	
			tools.ajax('/SisCapV2/secretaria/getSecretariasIdName', null, function(result) {
				var cmb = "";
				$.each(result, function(i, value) {
					cmb = cmb + '<option value="' + value.id + '">'
					+ value.licitanteJuridico.orgaoLicitante.nomeLicitante + '</option>';
				});
				$('#fkSecretaria').append(cmb);
				$('#fkSecretaria').select2();
			}); 
		},
		
		ajaxLoadProcesso : function (){
			
			tools.ajax('/SisCapV2/processo/getProcessoNumId', null, function(result) {
				var cmb = "";
				$.each(result, function(i, value) {
					cmb = cmb + '<option value="' + value.id + '">'
					+ value.numeroProcesso + '</option>';
				});
				$('#fkProcesso').append(cmb);
				$('#fkProcesso').select2();
			}); 
		
		},

		validation : function(){
			
			var validator = $('#saveFormTramite').validate({
			
			rules: {
				motivoTramit: {
			        required: true,
			        minlength: 3
			    },
			    responsTramit: {
			        required: true,
			        minlength: 3
			    },
			    retornoTramit: {
			        required: true,
			    },
			    saidaTramit: {
			        required: true,
			    },
			    fkProcesso: {
			        required: true,
			    },
			    fkSecretaria: {
			        required: true,
			    },
			   
			},
			
			highlight: function(element) {
			    var id_attr = "#" + $( element ).attr("id") + "1";
			    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			    $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');         
			},
			
			unhighlight: function(element) {
			    var id_attr = "#" + $( element ).attr("id") + "1";
			    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			    $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');         
			},
			
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
			    if(element.length) {
			        error.insertAfter(element);
			    } else {
			    error.insertAfter(element);
			    }
			}, 
			
			submitHandler: function( form ) {
				adminTramite.ajaxSaveTramite();
				return false;
			},
			
		});

		
		},
		ajaxSaveTramite : function (){
			tools.ajax('/SisCapV2/tramitacao/saveTramite', $('#saveFormTramite').serialize(), function(result) {
				console.log(result);
				var json = JSON.parse(result);
				if (json.cod != 0) {
					tools.messageToast('success', json.message);
					tools.resetForm('saveFormTramite');
					tools.inputFocus('motivoTramit');
				} else {
					tools.messageToast('error', json.message);
				}
			});
		},
		
		loadTableTramite: function(){
			$('#tableTramite').dataTable( {
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
		        },
				   "ajax": {
				      "url": window.location.protocol + '//' + window.location.host + "/SisCapV2/tramitacao/getTramites",
				      "dataSrc": "",
				   },
				   "columns": [
				      { "data": "responsTramit"},
				      { "data": "motivoTramit"},
				      { "data": "processo.numeroProcesso"},
				      { "data": "secretaria.licitanteJuridico.orgaoLicitante.nomeLicitante"},
				      { "data": "id"},
				      { "data": "id"},
				      { "data": "id"}
				   ],
				   columnDefs: [
				                {
				                    render: function(data) {
				                        return "<a href= '/SisCapV2/tramitacao/getTramitacao/" +data+ "' class='edit'><i class='glyphicon glyphicon-pencil'></i></a>";
				                    },
				                    targets: 4,
				                    className: "dt-body-center"
				                },
				                {
				                    render: function(data) {
				                        return "<a href= 'javascript:void(0);' class='del'><i class='glyphicon glyphicon-remove'></i></a>"
				                    },
				                    targets: 5,
				                    className: "dt-body-center"
				                },
				                {
				                    render: function(data) {
				                        return "<a href= '/SisCapV2/admin/dtlLicitanteJuridico/" +data+ "' class='edit'><i class='glyphicon glyphicon-eye-open'></i></a>";
				                    },
				                    targets: 6,
				                    className: "dt-body-center"
				                },
				                { orderable: false,  targets: [4, 5, 6] }
				            ],
				            	
				            createdRow: function (row, data, index) {
				                adminTramite.setClickInCreatedRow(row, data);
				            }
				});

		},
		
		setClickInCreatedRow: function(htmlRow, data) {
	        $(htmlRow).find('.del').click(function(event) {
	        	console.log('delete');
	        	event.preventDefault();
	        	$.confirm({
	                title: 'Confirme o delete!',
	                text: 'Deseja deletar o registro?',
	                confirmButton: 'Delete',
	                cancelButton: 'Cancelar',
	                confirmButtonClass: 'btn-primary',
	                cancelButtonClass: 'btn-danger',
	                confirm: function() { adminTramite.deleteTramite(data.id); },
	                cancel: function() { }
	            });
	        });
	    },
	    
	    deleteTramite : function (id){
			var data = {'id' : id};
			tools.ajax('/SisCapV2/tramitacao/getTramitacao/remove/'+id, data, function(result) {
				var json = JSON.parse(result);
				if (json.cod != 0) {
					$('#tableTramite').DataTable().ajax.reload();
					tools.messageToast('success', json.message);
				} else {
					tools.messageToast('error', json.message);
				}
			});
	    },
}