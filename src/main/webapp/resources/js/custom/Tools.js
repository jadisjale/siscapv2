var tools = {
	
	ajax : function ( url, data, callback ){
		$.ajax({
			'url' : url,
			'type' : 'POST',
			'data' : data,
			success : function(result) {
				callback(result);
			},
			error : function(result) {
				callback(result);
			},
		});
	},

	messageToast : function ( type, message ) {
		
		toastr.options = {
		  "closeButton": true,
		  "debug": false,
		  "newestOnTop": false,
		  "progressBar": true,
		  "positionClass": "toast-top-right",
		  "preventDuplicates": true,
		  "onclick": null,
		  "showDuration": "300",
		  "hideDuration": "1000",
		  "timeOut": "5000",
		  "extendedTimeOut": "1000",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "fadeIn",
		  "hideMethod": "fadeOut"
		}
		
		toastr[type](message);
	},
	
	resetForm : function (form) {
		$('input').val('');
	},
	
	inputFocus : function ( input ){
		$('#'+input).focus();
	},
	
	addValidationCpf : function (){
		jQuery.validator.addMethod("cpf", function(value, element) {
			   value = jQuery.trim(value);

			    value = value.replace('.','');
			    value = value.replace('.','');
			    cpf = value.replace('-','');
			    while(cpf.length < 11) cpf = "0"+ cpf;
			    var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
			    var a = [];
			    var b = new Number;
			    var c = 11;
			    for (i=0; i<11; i++){
			        a[i] = cpf.charAt(i);
			        if (i < 9) b += (a[i] * --c);
			    }
			    if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11-x }
			    b = 0;
			    c = 11;
			    for (y=0; y<10; y++) b += (a[y] * c--);
			    if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11-x; }

			    var retorno = true;
			    if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) retorno = false;

			    return this.optional(element) || retorno;

			}, "Informe um CPF correto");
	},
	
	valideEmail : function(email) {
		er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
		if (er.exec(email))
			return true;
		else
			return false;
	},
	
	iscpf : function (cpfValue) {
        cpfValue = cpfValue.replace(/[^\d]+/g,'');    
           if(cpfValue == '') return false; 
           // Elimina cpfValues invalidos conhecidos
           if (cpfValue.length != 11 || 
               cpfValue == "00000000000" || 
               cpfValue == "11111111111" || 
               cpfValue == "22222222222" || 
               cpfValue == "33333333333" || 
               cpfValue == "44444444444" || 
               cpfValue == "55555555555" || 
               cpfValue == "66666666666" || 
               cpfValue == "77777777777" || 
               cpfValue == "88888888888" || 
               cpfValue == "99999999999")
                   return false;       
           // Valida 1o digito
           add = 0;    
           for (i=0; i < 9; i ++)       
               add += parseInt(cpfValue.charAt(i)) * (10 - i);  
               rev = 11 - (add % 11);  
               if (rev == 10 || rev == 11)     
                   rev = 0;    
               if (rev != parseInt(cpfValue.charAt(9)))     
                   return false;       
           // Valida 2o digito
           add = 0;    
           for (i = 0; i < 10; i ++)        
               add += parseInt(cpfValue.charAt(i)) * (11 - i);  
           rev = 11 - (add % 11);  
           if (rev == 10 || rev == 11) 
               rev = 0;    
           if (rev != parseInt(cpfValue.charAt(10)))
               return false;       
           return true;   
   },
   
   isCNPJValid : function(cnpjParam) {
		cnpjParam = cnpjParam.replace(/[^\d]+/g, '');
		if (cnpjParam == '')
			return false;
		if (cnpjParam.length != 14)
			return false;
		if (cnpjParam == "00000000000000" || cnpjParam == "11111111111111"
				|| cnpjParam == "22222222222222" || cnpjParam == "33333333333333"
				|| cnpjParam == "44444444444444" || cnpjParam == "55555555555555"
				|| cnpjParam == "66666666666666" || cnpjParam == "77777777777777"
				|| cnpjParam == "88888888888888" || cnpjParam == "99999999999999")
			return false;

		tamanho = cnpjParam.length - 2
		numeros = cnpjParam.substring(0, tamanho);
		digitos = cnpjParam.substring(tamanho);
		soma = 0;
		pos = tamanho - 7;
		for (i = tamanho; i >= 1; i--) {
			soma += numeros.charAt(tamanho - i) * pos--;
			if (pos < 2)
				pos = 9;
		}
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(0))
			return false;

		tamanho = tamanho + 1;
		numeros = cnpjParam.substring(0, tamanho);
		soma = 0;
		pos = tamanho - 7;
		for (i = tamanho; i >= 1; i--) {
			soma += numeros.charAt(tamanho - i) * pos--;
			if (pos < 2)
				pos = 9;
		}
		resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
		if (resultado != digitos.charAt(1))
			return false;

		return true;
	},
	
	validationCNPJ : function () {
		jQuery.validator.addMethod("cnpj", function(cnpj, element) {
			return tools.isCNPJValid(cnpj);
		}, "Informe um CNPJ correto.");
	},

	setValueOptionSelect2 : function (value, select) {
		if (value !==0 ) {
			setTimeout(function(){ $('#'+ select).val(value).change(); }, 1000);
		}
	},

	setTitle: function (title) {
		$('#title-page').text(title);
	},
	
	scrollDiv: function (element) {
		$('html, body').animate({scrollTop:$('.'+element).position().top}, 'slow');
	},
	
	clickSpanScroll: function () {
		$('.group-button span').click(function(){
			var value = $(this).attr('id');
			console.log(value);
			tools.scrollDiv(value);
		});
		
		$('.group-button-footer span').click(function(){
			var value = $(this).attr('id');
			tools.scrollDiv(value);
		});
	},
	
	formatDateBr: function(dateObject){
		var d = new Date(dateObject);
	    var day = d.getDate();
	    var month = d.getMonth() + 1;
	    var year = d.getFullYear();
	    if (day < 10) {
	        day = "0" + day;
	    }
	    if (month < 10) {
	        month = "0" + month;
	    }
	    var date = day + "/" + month + "/" + year;
	    return date;
	},

	hide: function(select) {
		$('#'+select).hide();
	} 
	
}