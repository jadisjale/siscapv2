var adminFornecedorFisico = {
		index: function(){
			// input mask telefone
			 $('#telGerenteSec').mask('(99) 99999-9999');
			 $('#celGerenteSec').mask('(99) 99999-9999');
			 $('#telefoneTitularLicitante').mask('(99) 99999-9999');
			 $('#telefoneLicitante').mask('(99) 99999-9999');		 
			// fim mask telefone
			 
			 $('#cpf').mask('999.999.999-99');
			 
			 $('.datepicker').datepicker({
				language : 'pt-BR',
				format : 'dd/mm/yyyy',
				autoclose: true,
			 }).on('changeDate', function (ev) {
			    $(this).datepicker('hide');
			 });
				
				adminFornecedorFisico.validadorForm();
		},
		
		validadorForm : function() {
							
//			var nomeLicitatne = $('#nomeLicitante').val();
//			var titularLicitante = $('#titularLicitante').val();
//			var telefoneLicitante = $('#telefoneLicitante').val();
//			var faxLicitante = $('#faxLicitante').val();
//			var emailLicitante = $('#emailLicitante').val();
//			var telefoneTitularLicitante = $('#telefoneTitularLicitante').val();
//			var emailTitularLicitante = $('#emailTitularLicitante').val();
//			var rua = $('#rua').val();
//			var bairro = $('#bairro').val();
//			var cidade = $('#cidade').val();
//			var estado = $('#estado').val();
//			var cep = $('#cep').val();
//			var cpf = $('#cpf').val();
//			var expedicaoForn = $('#expedicaoForn').val();
//			var numRegstroForn = $('#numRegstroForn').val();
//			var validadeForn = $('#validadeForn').val();
				
			var validator = $('#saveFormFornecedorFisico').validate({
				
		        rules: {
		        	nomeLicitante: {
		                required: true,
		                minlength: 3
		            },
		            titularLicitante: {
		                required: true,
		                minlength: 3
		            },
		            cep: {
		                required: true,
		                minlength: 3
		            },
		            telefoneLicitante: {
		                required: true,
		                minlength: 3
		            },
		            faxLicitante: {
		                required: true,
		                minlength: 3
		            },
		            telefoneTitularLicitante: {
		                required: true,
		                minlength: 3
		            },
		            rua: {
		                required: true,
		                minlength: 3
		            },
		            bairro: {
		                required: true,
		                minlength: 3
		            },
		            cidade: {
		                required: true,
		                minlength: 3
		            },
		            estado: {
		                required: true,
		            },
		            cpf: {
		            	cpf: true, 
		            	required: true
		            },
		            expedicaoFornParam: {
		                required: true,
		            },
		            numRegstroForn: {
		                required: true,
		                minlength: 3
		            },
		            emailTitularLicitante: {
		                required: true,
		                email: true
		            },
		            emailLicitante: {
		            	required: true,
		                email: true
		            },
		            validadeFornParam: {
		                required: true,
		            },
		        },
		        
		        highlight: function(element) {
		            var id_attr = "#" + $( element ).attr("id") + "1";
		            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		            $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');         
		        },
		        
		        unhighlight: function(element) {
		            var id_attr = "#" + $( element ).attr("id") + "1";
		            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		            $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');         
		        },
		        
		        errorElement: 'span',
	            errorClass: 'help-block',
	            errorPlacement: function(error, element) {
	                if(element.length) {
	                    error.insertAfter(element);
	                } else {
	                error.insertAfter(element);
	                }
	            }, 
		        
		        submitHandler: function( form ) {
		        	adminFornecedorFisico.ajaxSaveFornecedorFisisco();
		        	return false;
		        },
		        
		    });
		},
		
		ajaxSaveFornecedorFisisco : function() {
			tools.ajax('/SisCapV2/fornecedor/saveFornecedorFisico', $('#saveFormFornecedorFisico').serialize(), function(result) {
				var json = JSON.parse(result);
				if (json.cod != 0) {
					tools.messageToast('success', json.message);
					tools.resetForm('saveFormFornecedorFisico');
					tools.inputFocus('nomeLicitante');
				} else {
					tools.messageToast('error', json.message);
				}
			});
		},
		
loadTableFisico: function(){

	$('#tableFisico').DataTable( {
		"language": {
			"url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
        },
		   "ajax": {
		      "url": "/SisCapV2/orgaolicitante/listLicitanteFisico",
		      "dataSrc": "",
		   },
		   "columns": [
		      { "data": "orgaoLicitante.nomeLicitante"},
		      { "data": "orgaoLicitante.titularLicitante"},
		      { "data": "orgaoLicitante.emailTitularLicitante"},
		      { "data": "id"},
		      { "data": "id"},
		      { "data": "id"}
		   ],
		   columnDefs: [
	                {
	                    render: function(data) {
	                        return "<a href= '/SisCapV2/orgaolicitante/getLicitanteFisico/" +data+ "' class='edit'><i class='glyphicon glyphicon-pencil'></i></a>";
	                    },
	                    targets: 3,
	                    className: "dt-body-center"
	                },
	                {
	                    render: function(data) {
	                        return "<a href= 'javascript:void(0);' class='del'><i class='glyphicon glyphicon-remove'></i></a>"
	                    },
	                    targets: 4,
	                    className: "dt-body-center"
	                },
	                {
	                    render: function(data) {
	                        return "<a href= '/SisCapV2/orgaolicitante/dtlLicitanteFisico/" +data+ "' class='edit'><i class='glyphicon glyphicon-eye-open'></i></a>";
	                    },
	                    targets: 5,
	                    className: "dt-body-center"
	                },
	                { orderable: false,  targets: [3, 4, 5] }
	            ],
	            	
	            createdRow: function (row, data, index) {
	            	adminFornecedorFisico.setClickInCreatedRow(row, data);
	            }
		});
	},
	
	setClickInCreatedRow: function(htmlRow, data) {
	    $(htmlRow).find('.del').click(function(event) {
	    	event.preventDefault();
	    	$.confirm({
	            title: 'Confirme o delete!',
	            text: 'Deseja deletar o registro?',
	            confirmButton: 'Delete',
	            cancelButton: 'Cancelar',
	            confirmButtonClass: 'btn-primary',
	            cancelButtonClass: 'btn-danger',
	            confirm: function() { adminFornecedorFisico.ajaxDeleteFisico(data.id); },
	            cancel: function() { }
	        });
	    });
	},
	
	ajaxDeleteFisico : function (id) {
		var data = {'id' : id};
		tools.ajax('/SisCapV2/orgaolicitante/getLicito/remove/'+id, data, function(result) {
			var json = JSON.parse(result);
			if (json.cod != 0) {
				tools.messageToast('success', json.message);
				$('#tableFisico').DataTable().ajax.reload();
			} else {
				tools.messageToast('error', json.message);
			}
		});
	},
}