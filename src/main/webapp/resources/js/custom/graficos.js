var graficos = {
                
	index: function(){
		graficos.cadastroFornecedorFisico();
		graficos.cadastroFornecedorJuridico();
	},
	
	cadastroFornecedorFisico: function (){
		
		tools.ajax('/SisCapV2/admin/getFisicos', null, function(result) {
			var count = Object.keys(result).length;
			Morris.Donut({
				  element: 'cadastro-fornecedor-fisico',
				  data: [
				    {label: "Cadastros", value: count},
				  ]
			});
		});
//		
		
	},

	cadastroFornecedorJuridico: function (){
		tools.ajax('/SisCapV2/admin/getJuridicos', null, function(result) {
			var count = Object.keys(result).length;
			Morris.Donut({
				  element: 'cadastro-fornecedor-juridico',
				  data: [
				     {label: "Cadastros", value: count},
				  ]
			});
		});
	}

}