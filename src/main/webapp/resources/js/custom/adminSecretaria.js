var adminSecretaria = {
                       
	table: '',

	index : function() {
		adminSecretaria.validador();
		$('#gerenteSec').focus();
		$('#telGerenteSec').mask('(99) 99999-9999');
		$('#celGerenteSec').mask('(99) 99999-9999');
		$('#cnpj').mask('99.999.999/9999-99');
		$('#telGerenteSec').mask('(99) 99999-9999');
		$('#celGerenteSec').mask('(99) 99999-9999');
		$('#telefoneTitularLicitante').mask('(99) 99999-9999');
		$('#telefoneLicitante').mask('(99) 99999-9999');		 
	},

	indexList : function() {
		adminSecretaria.loadTableSecretaria();
	},
	
	loadTableSecretaria: function(){
		adminSecretaria.table = $('#tableSecretaria').DataTable( {
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
	        },
			   "ajax": {
			      "url": "/SisCapV2/secretaria/getSecretarias",
			      "dataSrc": "",
			   },
			   "columns": [
			      { "data": "licitanteJuridico.orgaoLicitante.nomeLicitante" },
			      { "data": "gerenteSec"},
			      { "data": "emailGerenteSec"},
			      { "data": "id"},
			      { "data": "id"},
			      { "data": "id"}
			   ],
			   columnDefs: [
		                {
		                    render: function(data) {
		                        return "<a href= '/SisCapV2/secretaria/getSecretaria/" +data+ "' class='edit'><i class='glyphicon glyphicon-pencil'></i></a>";
		                    },
		                    targets: 3,
		                    className: "dt-body-center"
		                },
		                {
		                    render: function(data) {
		                        return "<a href= 'javascript:void(0);' class='del'><i class='glyphicon glyphicon-remove'></i></a>"
		                    },
		                    targets: 4,
		                    className: "dt-body-center"
		                },
		                {
		                    render: function(data) {
		                        return "<a href= '/SisCapV2/secretaria/detalheSecretaria/" +data+ "' class='edit'><i class='glyphicon glyphicon-eye-open'></i></a>";
		                    },
		                    targets: 5,
		                    className: "dt-body-center"
		                },
		                { orderable: false,  targets: [3, 4, 5] }
		            ],
		            	
		            createdRow: function (row, data, index) {
		                adminSecretaria.setClickInCreatedRow(row, data);
		            }
			});

	},
	
	setClickInCreatedRow: function(htmlRow, data) {
        $(htmlRow).find('.del').click(function(event) {
        	event.preventDefault();
        	$.confirm({
                title: 'Confirme o delete!',
                text: 'Deseja deletar o registro?',
                confirmButton: 'Delete',
                cancelButton: 'Cancelar',
                confirmButtonClass: 'btn-primary',
                cancelButtonClass: 'btn-danger',
                confirm: function() { adminSecretaria.ajaxDeleteSecretaria(data.id); },
                cancel: function() { }
            });
        });
    },
    
    ajaxDeleteSecretaria : function (id){
    	var data = {'id' : id};
    	tools.ajax('/SisCapV2/secretaria/secretaria/remove/'+id, data, function(result) {
			var json = JSON.parse(result);
			if (json.cod != 0) {
				tools.messageToast('success', json.message);	
				adminSecretaria.table.ajax.reload();
			} else {
				tools.messageToast('error', json.message);
			}
		});
    },

	carregarJuridico : function(idLicitanteJuridico) {
		tools.ajax('/SisCapV2/orgaolicitante/getJuridicosIdName', null, function(result) {
			var cmb = "";
			$.each(result, function(i, value) {
				if(value.id != idLicitanteJuridico){
					cmb = cmb + '<option value="' + value.id + '">'
					+ value.orgaoLicitante.nomeLicitante + '</option>';
				} 
			});
			$('#fkJuridico').append(cmb);
			$('#fkJuridico').select2();
		});
	},

	validador : function() {
	
		var validator = $('#saveFormSecretaria').validate({
			
			rules: {
				gerenteSec: {
			        required: true,
			        minlength: 3
			    },
			    celGerenteSec: {
			        required: true
			    },
			    telGerenteSec: {
			        required: true,
			        minlength: 3
			    },
			    emailGerenteSec: {
			        required: true,
			        email: true
			    },
			    fkJuridico: {
			        required: true,
			    },
			    nomeLicitante: {
	                required: true,
	                minlength: 3
	            },
	            titularLicitante: {
	                required: true,
	                minlength: 3
	            },
	            telefoneLicitante: {
	                required: true,
	                minlength: 3
	            },
	            faxLicitante: {
	                required: true,
	                minlength: 3
	            },
	            emailLicitante: {
	            	required: true,
	            	email: true
	            },
	            telefoneTitularLicitante: {
	                required: true,
	                minlength: 3
	            },
	            emailTitularLicitante: {
	            	required: true,
	            	email: true
	            },
	            rua: {
	                required: true,
	                minlength: 3
	            },
	            bairro: {
	                required: true,
	                minlength: 3
	            },
	            cidade: {
	                required: true,
	                minlength: 3
	            },
	            estado: {
	            	required: true
	            },
	            cep: {
	                required: true,
	            },
	            cnpj: {
	            	cnpj: true, 
	            	required: true
	            }
			},
			
			highlight: function(element) {
			    var id_attr = "#" + $( element ).attr("id") + "1";
			    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			    $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');         
			},
			
			unhighlight: function(element) {
			    var id_attr = "#" + $( element ).attr("id") + "1";
			    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			    $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');         
			},
			
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
			    if(element.length) {
			        error.insertAfter(element);
			    } else {
			    error.insertAfter(element);
			    }
			}, 
			
			submitHandler: function( form ) {
				adminSecretaria.ajaxSaveSecretaria();
				return false;
			},
			
		});
	},

	ajaxSaveSecretaria : function() {	
		tools.ajax('/SisCapV2/secretaria/saveSecretaria', $('#saveFormSecretaria').serialize(), function(result) {
			var json = JSON.parse(result);
			if (json.cod != 0) {
				tools.messageToast('success', json.message);
				tools.resetForm('saveFormSecretaria');
				tools.inputFocus('gerenteSec');
			} else {
				tools.messageToast('error', json.message);
			}
		});
	},
}