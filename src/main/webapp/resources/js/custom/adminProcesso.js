var adminProcesso = {

	indexList : function() {
		adminProcesso.listProcesso();
	},
	
	listProcesso : function (){
		$('#tableProcesso').dataTable( {
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
	        },
			   "ajax": {
			      "url": window.location.protocol + '//' + window.location.host + "/SisCapV2/processo/getProcesso",
			      "dataSrc": "",
			   },
			   "columns": [
			      { "data": "numeroProcesso" },
			      { "data": "secretaria.licitanteJuridico.orgaoLicitante.nomeLicitante"},
			      { "data": "secretaria2.licitanteJuridico.orgaoLicitante.nomeLicitante"},
			      { "data": "id"},
			      { "data": "id"},
//			      { "data": "id"}
			   ],
			   columnDefs: [
			                {
			                    render: function(data) {
			                        return "<a href= '/SisCapV2/processo/getProcesso/" +data+ "' class='edit'><i class='glyphicon glyphicon-pencil'></i></a>";
			                    },
			                    targets: 3
			                },
			                {
			                    render: function(data) {
			                        return "<a href= '/SisCapV2/admin/detalheSecretaria/" +data+ "' class='open'><i class='glyphicon glyphicon-info-sign'></i></a>"
			                    },
			                    targets: 4
			                },
//			                {
//			                    render: function(data) {
//			                        return "<a href= 'javascript:void(0);' class='del'><i class='glyphicon glyphicon-remove'></i></a>"
//			                    },
//			                    targets: 5
//			                },
			                { orderable: false,  targets: [3, 4] }
			            ],
			            
			            createdRow: function (row, data, index) {
			                adminProcesso.setClickInCreatedRow(row, data);
			            },  
			            
			});
	},
	
	setClickInCreatedRow: function(htmlRow, data) {
        $(htmlRow).find('.del').click(function(event) {
            
        	event.preventDefault();
        	$.confirm({
                title: 'Confirme o delete!',
                text: 'Deseja deletar o registro?',
                confirmButton: 'Delete',
                cancelButton: 'Cancelar',
                confirmButtonClass: 'btn-primary',
                cancelButtonClass: 'btn-danger',
                confirm: function() { adminProcesso.ajaxDeleteProcesso(data.id); },
                cancel: function() { }
            });
        });
    },
    
    ajaxDeleteProcesso : function (id){    
    	var data = {'id' : id};
    	console.log(data);
		tools.ajax('/SisCapV2/processo/getProcesso/remove/'+id, data, function(result) {
			var json = JSON.parse(result);
			console.log(json);
			if (json.cod != 0) {
				$('#tableProcesso').DataTable().ajax.reload();
				tools.messageToast('success', json.message);
			} else {
				tools.messageToast('error', json.message);
			}
		}); 			
    },

	index : function() {

		$('.datepicker').datepicker({
				language : 'pt-BR',
				format : 'dd/mm/yyyy',
				autoclose: true,
			}).on('changeDate', function (ev) {
			    $(this).datepicker('hide');
		});

		$('#vlorEstimadoProc').maskMoney({
			thousands : '',
			decimal : '.'
		});

		adminProcesso.carregarSelect();
		adminProcesso.carregarSelectFornecedor();
		adminProcesso.validador();
	},
	
	carregarSelectFornecedor: function(){
		tools.ajax('/SisCapV2/secretaria/getFornecedores', null, function(result) {
			var cmb = "";
			$.each(result, function(i, value) {
				if(value.licitanteJuridico !== undefined) {
					cmb = cmb + '<option value="' + value.id + '">'
					+ value.licitanteJuridico.orgaoLicitante.nomeLicitante + '</option>';
				} else {
					cmb = cmb + '<option value="' + value.id + '">'
					+ value.licitanteFisico.orgaoLicitante.nomeLicitante + '</option>';
				}
			});
			
			$('#fornecedor').append(cmb);
			$('#fornecedor').select2();
		});
	},

	carregarSelect : function() {
		tools.ajax('/SisCapV2/secretaria/getProcessoSecretaria', null, function(result) {
			var cmb = "";
			$.each(result, function(i, value) {
				cmb = cmb + '<option value="' + value.id + '">'
						+ value.licitanteJuridico.orgaoLicitante.nomeLicitante + '</option>';
			});
			
			$('#idSecretariaDois').append(cmb);
			$('#idSecretariaUm').append(cmb);
			
			$('#idSecretariaUm').select2();
			$('#idSecretariaDois').select2();
		});
	},

	validador : function() {
		var validator = $('#saveFormProcesso').validate({
			
		rules: {
			inicioProcessoParam: {
		        required: true,
		    },
		    numeroProcesso: {
		        required: true
		    },
		    entradaProcessoParam: {
		        required: true,
		    },
		    vlorEstimadoProcParam: {
		        required: true,
		    },
		    numeroSolicProc:{
		    	required: true,
		    },
		    numModalProc: {
		        required: true,
		    },
		    modalidadeProc: {
		        required: true,
		    },
		    tipoProc: {
		        required: true,
		    },
		    origemRecProc: {
		        required: true,
		    },
		    comissaoRespProc: {
		        required: true,
		    },
		    objetoProc: {
		        required: true,
		    },
		    situacaoProc: {
		        required: true,
		        minlength: 3
		    },
		    observacaoProc: {
		    	required: true,
		    },
		    dataContratoParam: {
		        required: true,
		    },
		    id1: {
		        required: true,
		    },
		    id2: {
		        required: true,
		    },
		    idFornecedor: {
		    	required: true,
		    }
		},
		
		highlight: function(element) {
		    var id_attr = "#" + $( element ).attr("id") + "1";
		    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		    $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');         
		},
		
		unhighlight: function(element) {
		    var id_attr = "#" + $( element ).attr("id") + "1";
		    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
		    $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');         
		},
		
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
		    if(element.length) {
		        error.insertAfter(element);
		    } else {
		    error.insertAfter(element);
		    }
		}, 
		
		submitHandler: function( form ) {
			adminProcesso.ajaxSaveProcesso();
			return false;
		},
		
		});
	},

	ajaxSaveProcesso : function() {				
		tools.ajax('/SisCapV2/processo/saveProcesso', $('#saveFormProcesso').serialize(), function(result) {
			var json = JSON.parse(result);
			if (json.cod != 0) {
				tools.messageToast('success', json.message);
				tools.resetForm('saveFormProcesso');
				tools.inputFocus('inicioProcesso');
			} else {
				tools.messageToast('error', json.message);
			}
		});
	},

}