var adminProduto = {
	index : function() {
		$("#valor").maskMoney({thousands:'', decimal:'.'});
		$('#descricao').focus();
		adminProduto.validador();
		adminProduto.carregarNomesFornecedores();
	},
	
	indexList : function(){
		adminProduto.listProduto();
	},
	
	
	saveProduto : function() {
		var dados = {
		    'idProduto': $('#idProduto').val(),
		    'valor': $('#valor').val(),
		    'descricao': $('#descricao').val(),
		    'fkFornecedor': $('#fkFornecedor').val()
		}
		tools.ajax('/SisCapV2/produto/saveProduto', dados, function(result) {
			console.log(result);
			var json = JSON.parse(result);
			if (json.cod != 0) {
				tools.messageToast('success', json.message);
				tools.resetForm('saveFormProduto');
				tools.inputFocus('descricao');
			} else {
				tools.messageToast('error', json.message);
			}
		});
	},
	
	listProduto : function (){
		$('#tableProduto').dataTable( {
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
	        },
			   "ajax": {
			      "url": window.location.protocol + '//' + window.location.host + "/SisCapV2/produto/getProdutos",
			      "dataSrc": "",
			   },
			   "columns": [
			      { "data": "descricao" },
			      { "data": "valor"},
			      { "data": "fornecedor.licitanteJuridico.orgaoLicitante.nomeLicitante"},
			      { "data": "id"},
			      { "data": "id"}
			   ],
			   columnDefs: [
	                {
	                    render: function(data) {
	                        return "<a href= '/SisCapV2/produto/getProduto/" +data+ "' class='edit'><i class='glyphicon glyphicon-pencil'></i></a>";
	                    },
	                    targets: 3,
	                    className: "dt-body-center"
	                },
	                {
	                    render: function(data) {
	                        return "<a href= 'javascript:void(0);' class='del'><i class='glyphicon glyphicon-remove'></i></a>"
	                    },
	                    targets: 4,
	                    className: "dt-body-center"
	                },
	                
	                { orderable: false,  targets: [3, 4] }
	            ],
	            
	            createdRow: function (row, data, index) {
	                adminProduto.setClickInCreatedRow(row, data);
	            },  
			});
	},
	
	setClickInCreatedRow: function(htmlRow, data) {
        $(htmlRow).find('.del').click(function(event) {
        	event.preventDefault();
        	$.confirm({
                title: 'Confirme o delete!',
                text: 'Deseja deletar o registro?',
                confirmButton: 'Delete',
                cancelButton: 'Cancelar',
                confirmButtonClass: 'btn-primary',
                cancelButtonClass: 'btn-danger',
                confirm: function() { adminProduto.ajaxDeleteProduto(data.id); },
                cancel: function() { }
            });
        });
    },
    
    ajaxDeleteProduto : function (id){
    	var data = {'id' : id};
		tools.ajax('/SisCapV2/produto/getProduto/remove/'+id, data, function(result) {
			var json = JSON.parse(result);
			if (json.cod != 0) {
				$('#tableProduto').DataTable().ajax.reload();
				tools.messageToast('success', json.message);
			} else {
				tools.messageToast('error', json.message);
			}
		});
    },
		
	carregarNomesFornecedores : function(){
		tools.ajax('/SisCapV2/fornecedor/getNomeFornecedor', null, function(result) {
			var cmb = "";
			$.each(result, function(i, value) {
				if(value.hasOwnProperty('licitanteFisico')){
					cmb = cmb + '<option value="' + value.id + '">'
					+ value.licitanteFisico.orgaoLicitante.nomeLicitante + '</option>';
				} else {
					cmb = cmb + '<option value="' + value.id + '">'
					+ value.licitanteJuridico.orgaoLicitante.nomeLicitante + '</option>';console.log(2);
				}			
			});
			$('#fkFornecedor').append(cmb);
			$('#fkFornecedor').select2();
		});
	},

	validador : function() {
	
		var validator = $('#saveFormProduto').validate({
			
			rules: {
				fkFornecedor: {
			        required: true,
			    },
			    valor: {
			        required: true,
			    },
			    descricao: {
			        required: true,
			        minlength: 3
			    },
			},
			
			highlight: function(element) {
			    var id_attr = "#" + $( element ).attr("id") + "1";
			    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
			    $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');         
			},
			
			unhighlight: function(element) {
			    var id_attr = "#" + $( element ).attr("id") + "1";
			    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
			    $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');         
			},
			
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
			    if(element.length) {
			        error.insertAfter(element);
			    } else {
			    error.insertAfter(element);
			    }
			}, 
			
			submitHandler: function( form ) {
				adminProduto.saveProduto();
				return false;
			},
		
		});

	},
}