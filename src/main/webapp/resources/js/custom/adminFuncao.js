funcao = {
		
	table : '',
	
	validationForm : function () {
		var validator = $('#form-save-funcao').validate({
			
	        rules: {
	        	funcao: {
	                required: true,
	                minlength: 3
	            },
	        },
	        
	        highlight: function(element) {
	            var id_attr = "#" + $( element ).attr("id") + "1";
	            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
	            $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');         
	        },
	        
	        unhighlight: function(element) {
	            var id_attr = "#" + $( element ).attr("id") + "1";
	            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
	            $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');         
	        },
	        
	        errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.length) {
                    error.insertAfter(element);
                } else {
                error.insertAfter(element);
                }
            }, 
	        
	        submitHandler: function( form ) {
	        	funcao.saveFuncao();
	        	return false;
	        },
	        
	    });
	},	
	
	saveFuncao : function () {
		var data = {
			'id' : $( "#id" ).val(),
			'funcao' : $( "#inputFuncao" ).val()
		}
		tools.ajax('/SisCapV2/funcao/saveFuncao', data, function(result) {
			var json = JSON.parse(result);
			if (json.cod != 0) {
				tools.messageToast('success', json.message);
				tools.resetForm('form-save-funcao');
				tools.inputFocus('inputFuncao');
			} else {
				tools.messageToast('error', json.message);
			}
		});
	},
	
	loadTableFuncao: function(){
		
		funcao.table = $('#tableFuncao').DataTable( {
			"language": {
				"url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
	        },
			   "ajax": {
			      "url": "/SisCapV2/funcao/getFuncoes",
			      "dataSrc": "",
			   },
			   "columns": [
			      { "data": "funcao"},
			      { "data": "id"},
			      { "data": "id"}
			   ],
			   columnDefs: [
	                {
	                    render: function(data) {
	                        return "<a href= '/SisCapV2/funcao/getFuncao/" +data+ "' class='edit'><i class='glyphicon glyphicon-pencil'></i></a>";
	                    },
	                    targets: 3,
	                    "searchable": false,
	                    className: "dt-body-center"
	                },
	                {
	                    render: function(data) {
	                        return "<a href= 'javascript:void(0);' class='del'><i class='glyphicon glyphicon-remove'></i></a>"
	                    },
	                    targets: 4,
	                    "searchable": false,
	                    className: "dt-body-center"
	                },
	                { orderable: false,  targets: [3, 4] }
	            ],
	            	
	            createdRow: function (row, data, index) {
	                user.setClickInCreatedRow(row, data);
	            }
		});
	},
	
	setClickInCreatedRow: function(htmlRow, data) {
        $(htmlRow).find('.del').click(function(event) {
        	event.preventDefault();
        	$.confirm({
                title: 'Confirme o delete!',
                text: 'Deseja deletar a funçao <b>' + data.funcao + '</b>?',
                confirmButton: 'Delete',
                cancelButton: 'Cancelar',
                confirmButtonClass: 'btn-primary',
                cancelButtonClass: 'btn-danger',
                confirm: function() { funcao.ajaxDeleteFuncao(data.id); },
                cancel: function() { }
            });
        });
    },
    
    ajaxDeleteFuncao : function (id) {
    	var data = {'id' : id};
    	tools.ajax('/SisCapV2/funcao/getFuncao/remove/'+data.id, data, function(result) {
			var json = JSON.parse(result);
			if (json.cod != 0) {
				tools.messageToast('success', json.message);	
				funcao.table.ajax.reload();
			} else {
				tools.messageToast('error', json.message);
			}
		});
    }
	
}