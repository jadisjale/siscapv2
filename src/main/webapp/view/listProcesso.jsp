<%@include file="home.jsp"%>
<div class="container">
	<h2> Lista de Processos <h2>
</div>
<hr>
<div class="container">
	<div class="row link_page">
		<div class="col-sm-12">
			<span> Adicionar um novo Processo </span><a
				href="/SisCapV2/processo/addProcesso">
				<i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
			</a>
		</div>
	</div>
	<div class="ibox-content">
		<table id="tableProcesso" class="table table-striped table-bordered" width="100%">
			<thead>
				<tr>
					<th>N�mero Processo</th>
					<th>Secretaria Remetente</th>
					<th>Secretaria Destinat�rio</th>
					<th width="10px"></th>
					<th width="10px"></th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<hr>
<%@include file="footer.jsp"%>
<script>
$(document).ready(function() {	
	adminController.index();
	tools.setTitle('Lista de Processo');
	adminProcesso.indexList();
});
</script> 
