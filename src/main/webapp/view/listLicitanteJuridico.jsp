<%@include file="home.jsp"%>
<div class="container">
	<h2>
		Lista de Licitante Jur�dico
		<h2>
</div>
<hr>
<div class="container">

	<div class="row link_page">
		<div class="col-sm-12">
			<span> Adicionar um novo Fornecedor Jur�dico </span><a
				href="/SisCapV2/fornecedor/addFornecedorJuridico">
				<i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
			</a>
		</div>
	</div>
	
	<div class="ibox-content">
		<table id="tableJuridica" class="table table-striped table-bordered"
			width="100%">
			<thead>
				<tr>
					<th>Nome do Licitante</th>
					<th>Titular Licitante</th>
					<th>Email Licitante</th>
					<th width="10px"></th>
					<th width="10px"></th>
					<th width="10px"></th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<hr>
<%@include file="footer.jsp"%>
<script>
	$(document).ready(function() {
		adminController.index();
		tools.setTitle('Lista de Pessoas Jur�dicas');
		juridico.loadTableJuridico();
	});
</script>

