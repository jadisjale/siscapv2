<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="home.jsp"%>

<div class="container">
	<form class="form-horizontal" id="form-save-user" autocomplete="off">
		<input type="hidden" name="id" id="id" value="${id}">
		<div class="form-group has-feedback">
			<label class="col-xs-2 control-label" for="inputSuccess">Nome</label>
			<div class="col-xs-10">
				<input type="text" name="nome" id="inputNameUser"
					class="form-control" placeholder="Nome do novo Usuário" value="${nome}">
					<span class="glyphicon form-control-feedback" id="inputNameUser1"></span> 
			</div>
		</div>
		<div class="form-group has-feedback">
			<label class="col-xs-2 control-label" for="inputSuccess">Registro</label>
			<div class="col-xs-10">
				<input type="text" name="matricula" id="inputRegistrationUser"
					class="form-control" placeholder="Registro do novo Usuário"
					value="${matricula}">
					<span class="glyphicon form-control-feedback" id="inputRegistrationUser1"></span>
			</div>
		</div>
		<div class="form-group has-feedback">
			<label class="col-xs-2 control-label" for="inputSuccess">Senha</label>
			<div class="col-xs-10">
				<input type="password" name="senha" id="inputPasswordUser"
					class="form-control" placeholder="Senha do novo Usuário"
					value="${senha}">
					<span class="glyphicon form-control-feedback" id="inputPasswordUser1"></span>
			</div>
		</div>
		<div class="form-group has-feedback">
			<label class="col-xs-2 control-label" for="inputError">Função</label>
			<div class="col-xs-10">
				<select class="form-control selectpicker show-tick"
					id="functionUser" name="funcao">

					<c:if test="${funcao == 1}"> <option value="1" selected="selected">ADMIN</option> </c:if>
					<c:if test="${funcao != 1}"> <option value="1">ADMIN</option></c:if>
					<c:if test="${funcao == 2}"> <option value="2" selected="selected">MASTER</option> </c:if>
					<c:if test="${funcao != 2}"> <option value="2">MASTER</option></c:if>
					<c:if test="${funcao == 3}"> <option value="3" selected="selected">PREMIUN</option></c:if>
					<c:if test="${funcao != 3}"> <option value="3">PREMIUN</option></c:if>
					<c:if test="${funcao == 4}"> <option value="4" selected="selected">USUARIO</option> </c:if>
					<c:if test="${funcao != 4}"><option value="4">USUARIO</option></c:if>
					
				</select>
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-offset-2 col-xs-10">
				<button type="submit" class="btn btn-success" id="submitSaveUser"> <i class="fa fa-user-plus" aria-hidden="true"></i> Salvar Usuário</button>
				
				<button type="button" class="btn btn-primary" id="listUser"><i class="fa fa-users" aria-hidden="true"></i> Listar Usuários</button>
			
			</div>
		</div>
	</form>
</div>
<hr>
<%@include file="footer.jsp"%>
<script type="text/javascript">
	$(document).ready(function() {
		adminController.index();
		user.validationForm();	
		var id = $('#id').val();
		if (id) {
			tools.setTitle('Editar Usuário');
		} else {
			tools.setTitle('Novo Usuário');
		}
		$('#listUser').click(function (){
			window.location = "/SisCapV2/user/listUser";
		});
	});
</script>
