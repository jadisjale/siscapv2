<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

</body>
</html><%@include file="home.jsp"%>

<div class="container">
	<h2>Detalhes da Secretria</h2>
</div>

<hr>

<div class="container">

	<div class="panel panel-primary">
		<div class="panel-heading">Gerente Secretaria</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<label>Nome Gerente:</label> <label id='resposta'>${gerenteSec}</label>
				</div>
				<div class="col-md-6">
					<label>Celular gerente:</label> <label id='resposta'>${celGerenteSec}</label>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<label>Telefone Gerente:</label> <label id='resposta'>${telGerenteSec}</label>

				</div>
				<div class="col-md-6">
					<label>Email Gerente:</label> <label id='resposta'>${emailGerenteSec}</label>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">

	<div class="panel panel-primary">
		<div class="panel-heading">Org�o</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<label>Licitante:</label> <label id='resposta'>${nomeLicitante}</label>
				</div>
				<div class="col-md-6">
					<label>CNPJ:</label> <label id='resposta'>${cnpj}</label>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<label>Titular:</label> <label id='resposta'>${titular}</label>
				</div>
				
				<div class="col-md-6">
					<label>Email Titular:</label> <label id='resposta'>${emailTitular}</label>
				</div>
				
				<div class="col-md-6">
					<label>Fax:</label> <label id='resposta'>${fax}</label>
				</div>
				
			</div>
		</div>
	</div>
</div>

<div class="container">

	<div class="panel panel-primary">
		<div class="panel-heading">Endere�o</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<label>CEP:</label> <label id='resposta'>${cep}</label>
				</div>
				<div class="col-md-6">
					<label>Rua:</label> <label id='resposta'>${rua}</label>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<label>Bairro:</label> <label id='resposta'>${bairro}</label>

				</div>
				<div class="col-md-6">
					<label>Cidade:</label> <label id='resposta'>${cidade}</label>
				</div>
				<div class="col-md-6">
					<label>Estado:</label> <label id='resposta'>${estado}</label>
				</div>
			</div>
		</div>
	</div>
</div>



<!-- Cria uma nova divisao azul 
<div class="container">
	<div class="panel panel-primary">
		titulo
		<div class="panel-heading">Org�o</div>
		<div class="panel-body">
			cria uma linha com dois inputs
			<div class="row">
				<div class="col-md-6">
					<label>Rua:</label> <label id='resposta'>${rua}</label>
				</div>
				<div class="col-md-6">
					no ${cnpj} � o valor que vem do controller do model.atribute, {chave, valor}
					ex: model.addAttribute("rua", secretaria.getLicitanteJuridico().getOrgaoLicitante().getRua());
					<label>CNPJ:</label> <label id='resposta'>${rua}</label>
				</div>
			</div>
			fim

			outra linha com dois inouts
			<div class="row">
				<div class="col-md-6">
					<label>Titular:</label> <label id='resposta'>${titular}</label>
				</div>
				<div class="col-md-6">
					<label>Email Titular:</label> <label id='resposta'>${emailTitular}</label>
				</div>
			</div>
		</div>
	</div>
</div>
fim da divisao azul
 -->
<div class="container">
		<div>
			<br>
			<button type="submit" class="btn btn-primary" id="detalheJuridico">Voltar</button>
		</div>
</div>

<hr>
<%@include file="footer.jsp"%>
<script>
	$(document).ready(function() {
		adminController.index();
		tools.setTitle('Detalhe Secretaria');
		$("#detalheJuridico").click(function() {
			window.location = "/SisCapV2/secretaria/listSecretaria";
		});
	});
</script>