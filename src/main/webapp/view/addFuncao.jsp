<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="home.jsp"%>

<div class="container">
	<form class="form-horizontal" id="form-save-funcao" autocomplete="off">
		<input type="hidden" name="id" id="id" value="${id}">
		<div class="form-group has-feedback">
			<label class="col-xs-2 control-label" for="inputSuccess">Função</label>
			<div class="col-xs-10">
				<input type="text" name="funcao" id="inputFuncao"
					class="form-control" placeholder="Nome da nova Função" value="${funcao}">
					<span class="glyphicon form-control-feedback" id="inputFuncao1"></span> 
			</div>
		</div>
	
		<div class="form-group">
			<div class="col-xs-offset-2 col-xs-10">
				<button type="submit" class="btn btn-primary" id="submitSaveFuncao"> <i class="fa fa-funcao-plus" aria-hidden="true"></i> Salvar Função</button>
				
				<button type="submit" class="btn btn-primary" id="listFuncao"><i class="fa fa-funcoes" aria-hidden="true"></i> Listar Função</button>
			
			</div>
		</div>
	</form>
</div>
<hr>
<%@include file="footer.jsp"%>
<script type="text/javascript">
	$(document).ready(function() {
		adminController.index();
		funcao.validationForm();	
		var id = $('#id').val();
		if (id) {
			tools.setTitle('Editar Função');
		} else {
			tools.setTitle('Nova Função');
		}
		$('#listFuncao').click(function(){
			window.location = "/SisCapV2/funcao/listFuncao";
		});
	});
</script>
