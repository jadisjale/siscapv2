<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

</body>
</html><%@include file="home.jsp"%>
<div class="container">
	<h2>Detalhes da Pessoa Juridico</h2>
</div>
<hr>

<div class="container">

	<div class="panel panel-primary">
		<div class="panel-heading">Licitante</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<label>Nome Licitante:</label> <label id='resposta'>${nomeLicitante}</label>
				</div>
				<div class="col-md-6">
					<label>Titular Licitante:</label> <label id='resposta'>${titularLicitante}</label>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<label>Telefone Licitante:</label> <label id='resposta'>${telefoneLicitante}</label>

				</div>
				<div class="col-md-6">
					<label>Fax Licitante:</label> <label id='resposta'>${faxLicitante}</label>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<label>Email Licitante:</label> <label id='resposta'>${emailLicitante}</label>

				</div>
				<div class="col-md-6">
					<label>Telefone Titular Licitante:</label> <label id='resposta'>${telefoneTitularLicitante}</label>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-primary">
		<div class="panel-heading">Endere�o</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<label>CNPJ:</label> <label id='resposta'>${cnpj}</label>

				</div>
				<div class="col-md-6">
					<label>Rua:</label> <label id='resposta'>${rua}</label>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<label>Bairro:</label> <label id='resposta'>${rua}</label>

				</div>
				<div class="col-md-6">
					<label>Cidade:</label> <label id='resposta'>${cidade}</label>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<label>CEP:</label> <label id='resposta'>${cep}</label>
				</div>
				<div class="col-md-6">
					<label>Estado:</label> <label id='resposta'>${estado}</label>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-primary">
		<div class="panel-heading">Fornecedor</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<label>Expedi��o Fornecedor:</label> <label id='resposta'>${expedicaoForn}</label>
				</div>
				<div class="col-md-6">
					<label>N�mero Registro:</label> <label id='resposta'>${numRegstroForn}</label>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<label>Validade Fornecedor:</label> <label id='resposta'>${validadeForn}</label>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<div>
			<br>
			<button type="submit" class="btn btn-primary" id="detalheJuridico">Voltar</button>
		</div>
	</div>

</div>
<hr>
<%@include file="footer.jsp"%>
<script>
	$(document).ready(function() {
		adminController.index();
		adminController.index();
		tools.setTitle('Detalhe Pessoa Jur�dico');
		$("#detalheJuridico").click(function() {
			window.location = "/SisCapV2/fornecedor/listLicitanteJuridico";
		});
	});
</script>