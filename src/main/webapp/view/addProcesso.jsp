<%@include file="home.jsp"%>
<div class="container">
	<h2>Cadastro Processo</h2>
</div>
<hr>
<div class="container">
   <div class="panel panel-primary">
      <div class="panel-heading">Cadastrar Processo</div>
      <div class="panel-body">
         <form role="form" id="saveFormProcesso">
            <input type="hidden" name="id" value="${id}"></input>
            <input type="hidden" id="secretaria1" value="${secretaria1}"></input>
            <input type="hidden" id="secretaria1" value="${secretaria2}"></input>
            <input type="hidden" id="idFornecedor" value="${idFornecedor}"></input>
            <div class="row">
               <div class="col-sm-6">
                  <div class="form-group has-feedback">
                     <label for="email">In�cio Processo:</label> <input type="text"
                        class="form-control datepicker" id="inicioProcesso"
                        name="inicioProcessoParam" value="${inicioProcessoParam}">
                     <span class="glyphicon form-control-feedback" id="inicioProcesso1"></span>
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group has-feedback">
                     <label for="pwd">N�mero Processo:</label> <input type="text"
                        class="form-control" id="numeroProcesso" name="numeroProcesso" value="${numeroProcesso}">
                     <span class="glyphicon form-control-feedback" id="numeroProcesso1"></span>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-6">
                  <div class="form-group has-feedback">
                     <label for="email">Entrada Processo:</label> <input type="text"
                        class="form-control datepicker" id="entradaProcesso"
                        name="entradaProcessoParam" value="${entradaProcessoParam}">
                     <span class="glyphicon form-control-feedback" id="entradaProcesso1"></span>
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group has-feedback">
                     <label for="pwd">Valor Estimado Processo:</label> <input type="text"
                        class="form-control " id="vlorEstimadoProc" name="vlorEstimadoProcParam"
                        value="${vlorEstimadoProcParam}">
                     <span class="glyphicon form-control-feedback" id="vlorEstimadoProc1"></span>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-6">
                  <div class="form-group has-feedback">
                     <label for="pwd">N�mero Solicita��o Processo:</label> <input
                        type="text" class="form-control" id="numeroSolicProc"
                        name="numeroSolicProc" value="${numeroSolicProc}">
                     <span class="glyphicon form-control-feedback" id="numeroSolicProc1"></span>
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group has-feedback">
                     <label for="pwd">N�mero Modalidade Processo:</label> <input
                        type="text" class="form-control" id="modalidade"
                        name="numModalProc" value="${numModalProc}">
                     <span class="glyphicon form-control-feedback" id="modalidade1"></span>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-6">
                  <div class="form-group has-feedback">
                     <label for="pwd">Modalidade Processo:</label> <input type="text"
                        class="form-control" id="modalidade_processo" name="modalidadeProc"
                        value="${modalidadeProc}">
                     <span class="glyphicon form-control-feedback" id="modalidade_processo1"></span>
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group has-feedback">
                     <label for="pwd">Tipo Processo:</label> <input type="text"
                        class="form-control" id="tipoProc" name="tipoProc"
                         value="${tipoProc}">
                     <span class="glyphicon form-control-feedback" id="tipoProc1"></span>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-6">
                  <div class="form-group has-feedback">
                     <label for="pwd">origemRecProc:</label> <input type="text"
                        class="form-control" id="origemRecProc" name="origemRecProc"
                        value="${origemRecProc}">
                     <span class="glyphicon form-control-feedback" id="origemRecProc1"></span>
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group has-feedback">
                     <label for="pwd">Comiss�o Respons�vel Processo:</label> <input
                        type="text" class="form-control" id="comissaoRespProc"
                        name="comissaoRespProc" value="${comissaoRespProc}">
                     <span class="glyphicon form-control-feedback" id="comissaoRespProc1"></span>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-6">
                  <div class="form-group has-feedback">
                     <label for="pwd">Objeto Processo:</label> <input type="text"
                        class="form-control" id="objeto" name="objetoProc"
                        value="${objetoProc}">
                     <span class="glyphicon form-control-feedback" id="objeto1"></span>
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group has-feedback">
                     <label for="pwd">Situac�o Processo:</label> <input type="text"
                        class="form-control" id="situacaoProc" name="situacaoProc"
                        value="${situacaoProc}">
                     <span class="glyphicon form-control-feedback" id="situacaoProc1"></span>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-6">
                  <div class="form-group has-feedback">
                     <label for="pwd">Observa��o Processo:</label> <input type="text"
                        class="form-control" id="observacaoProc" name="observacaoProc"
                        value="${observacaoProc}">
                     <span class="glyphicon form-control-feedback" id="observacaoProc1"></span>
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group has-feedback">
                     <label for="email">Data Contrato:</label> <input type="text"
                        class="form-control datepicker" id="dataContrato"
                        name="dataContratoParam" value="${dataContratoParam}"> 
                     <span class="glyphicon form-control-feedback" id="dataContrato1"></span>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-4">
                  <div class="form-group has-feedback">
                     <label for="email">Secretaria Remetente:</label>
                     <select class="form-control selectField" id="idSecretariaUm" name="id1">	
                     </select>
                     <span class="glyphicon form-control-feedback" id="idSecretariaUm1"></span>
                  </div>
               </div>
               <div class="col-sm-4">
                  <div class="form-group has-feedback">
                     <label for="email">Secretaria Destinat�rio:</label>
                     <select id="idSecretariaDois" class="form-control selectField" name="id2">	
                     </select>
                     <span class="glyphicon form-control-feedback" id="idSecretariaDois1"></span>
                  </div>
               </div>
               <div class="col-sm-4">
                  <div class="form-group has-feedback">
                    <label for="email">Fornecedor:</label>
                     <select id="fornecedor" class="form-control selectField" name="idFornecedor">	
                     </select>
                     <span class="glyphicon form-control-feedback" id="fornecedor1"></span>
                  </div>
               </div>
            </div>
            <button type="submit" class="btn btn-success"
               id="submitSaveProcesso"><i class="fa fa-floppy-o" aria-hidden="true"></i>Salvar Processo</button>
            <button type="button" class="btn btn-primary"
               id="listProcesso"><i class="fa fa-list" aria-hidden="true"></i>Listar Processo</button>   
         </form>
      </div>
   </div>
</div>
<hr>
<%@include file="footer.jsp"%>

<script type="text/javascript">
	$(document).ready(function() {
		tools.setTitle('Novo Processo');
		
		$('#listProcesso').click(function(){
			window.location.href = "/SisCapV2/processo/listProcesso";
		});

		adminProcesso.index();
		adminController.index();
		var secretaria1 = $('#secretaria1').val();
		tools.setValueOptionSelect2(secretaria1, 'idSecretariaUm');
		var secretaria2 = $('#secretaria2').val();
		tools.setValueOptionSelect2(secretaria2, 'idSecretariaDois');
		var idFornecedor = $('#idFornecedor').val();
		tools.setValueOptionSelect2(idFornecedor, 'fornecedor');
	});
</script>
