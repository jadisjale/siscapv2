<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
	#fof{display:block; position:relative; width:100%; margin:150px 0; text-align:center;}
#fof .positioned{display:block; width:85%; margin:0 auto; padding:20px; border:1px solid #CCCCCC;}
#fof .positioned h1{margin:0 0 0 20px; padding:0; display:inline; font-size:60px; text-transform:uppercase;}
#fof .positioned p{margin:25px 0 0 0; padding:0; font-size:16px;}
#fof a.go-back, #fof a.go-home{display:block; position:absolute; top:30px; width:100px; padding:20px 0; font-size:20px; text-transform:uppercase; color:#FFFFFF; background-color:#FF6600;}
#fof a.go-back{left:0;}
#fof a.go-home{right:0;}

/* CSS3 Elements Only Work In Latest Browsers */

#fof .positioned, #fof a.go-back, #fof a.go-home{
	/* Rounded Corners */
	-moz-border-radius:10px;
	-webkit-border-radius:10px;
	-khtml-border-radius:10px;
	border-radius:10px;
	}
</style>
</head>
<body>
	<div class="wrapper row2">
  <div id="container" class="clear">
    <section id="fof" class="clear">
      <a class="go-back" href="javascript:history.go(-1)">Voltar<br>&laquo;</a>
      <div class="positioned">
        <h1>404 Error !</h1>
        <p>Esta requisi��o n�o existe</p>
      </div>
      <a class="go-home" href="/SisCapV2/admin/graficos">In�cio<br>&raquo;</a>
    </section>
  </div>
</div>
</body>
</html>