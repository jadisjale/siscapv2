<%@include file="home.jsp"%>
<div class="container">
	<h2>
		Lista de Licitante F�sico
		</h2>
</div>
<hr>
<div class="container">

	<div class="row link_page">
		<div class="col-sm-12">
			<span> Adicionar um novo Fornecedor F�sico </span><a
				href="/SisCapV2/fornecedor/addFornecedorFisico">
				<i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
			</a>
		</div>
	</div>
	
	<div class="ibox-content">
		<table id="tableFisico" class="table table-striped table-bordered"
			width="100%">
			<thead>
				<tr>
					<th>Nome do Licitante</th>
					<th>Titular Licitante</th>
					<th>Email Licitante</th>
					<th width="10px"></th>
					<th width="10px"></th>
					<th width="10px"></th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<hr>
<%@include file="footer.jsp"%>
<script>
	$(document).ready(function() {
		adminController.index();
		tools.setTitle('Lista de Pessoas F�sicas');
		adminFornecedorFisico.loadTableFisico();
	});
</script>

