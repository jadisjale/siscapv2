<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@include file="home.jsp"%>
    
     <div class="container">
		<div class="row">
			<div class="col-sm-6">
				<div class="panel panel-primary">
					<div class="panel-heading">Número de cadastros Pessoa Física</div>
					<div class="panel-body">
						 <div id="cadastro-fornecedor-fisico"></div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="panel panel-primary">
					<div class="panel-heading">Número de cadastros Pessoa Jurídica</div>
					<div class="panel-body">
						 <div id="cadastro-fornecedor-juridico"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
    

<%@include file="footer.jsp"%>
<script type="text/javascript">

 graficos.index();
 adminController.index();

</script>
