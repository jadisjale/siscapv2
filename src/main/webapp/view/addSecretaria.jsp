<div class="topo"></div>
<%@include file="home.jsp"%>
<div class="container">
	<div class="row">
		<div class="col-sm-6">
			<h2>Cadastrar Org�o</h2>
		</div>
		<div class="col-sm-6">
			<div class="group-button">
				<span class="label label-primary" id="licitante">Dados do Gerente</span> <span
					class="label label-success" id="fornecedor">Dados do Org�o</span> <span
					class="label label-info" id="endereco">Endere�o</span>
			</div>
		</div>
	</div>

</div>
<hr>
<div class="licitante"></div>
<div class="container">
	<form role="form" id="saveFormSecretaria">
		<input type="hidden" name="idSecretaria" value="${idSecretaria}">
		<input type="hidden" name="idLicitanteJuridico" value="${idLicitanteJuridico}">
		<input type="hidden" name="idOrgaoLicitante" value="${idOrgaoLicitante}">
		
		<div class="panel panel-primary">
			<div class="panel-heading">Dados do Gerente do Org�o</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group has-feedback">
							<label for="email">Nome Gerente Secretaria:</label> <input
								type="text" class="form-control" id="gerenteSec"
								name="gerenteSec" value="${gerenteSec}"> <span
								class="glyphicon form-control-feedback" id="gerenteSec1"></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group has-feedback">
							<label for="pwd">Celular Gerente Secretaria:</label> <input
								type="text" class="form-control" id="celGerenteSec"
								name="celGerenteSec" value="${celGerenteSec}"> <span
								class="glyphicon form-control-feedback" id="celGerenteSec1"></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group has-feedback">
							<label for="pwd">Telefone Gerente Secretaria:</label> <input
								type="text" class="form-control" id="telGerenteSec"
								name="telGerenteSec" value="${telGerenteSec}"> <span
								class="glyphicon form-control-feedback" id="telGerenteSec1"></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group has-feedback">
							<label for="pwd">Email Gerente Secretaria:</label> <input
								type="text" class="form-control" id="emailGerenteSec"
								name="emailGerenteSec" value="${emailGerenteSec}"> <span
								class="glyphicon form-control-feedback" id="emailGerenteSec1"></span>
						</div>
					</div>
				</div>

			</div>
		</div>
		<div class="fornecedor"></div>
		<div class="panel panel-success">
			<div class="panel-heading">Dados do Org�o</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group has-feedback">
							<label for="email">Nome Licitante:</label> <input type="text"
								class="form-control" id="nomeLicitante" name="nomeLicitante"
								value="${nomeLicitante}"> <span
								class="glyphicon form-control-feedback" id="nomeLicitante1"></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group has-feedback">
							<label for="pwd">Titular Licitante:</label> <input type="text"
								class="form-control" id="titularLicitante"
								name="titularLicitante" value="${titularLicitante}"> <span
								class="glyphicon form-control-feedback" id="titularLicitante1"></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group has-feedback">
							<label for="pwd">Telefone Licitante:</label> <input type="text"
								class="form-control" id="telefoneLicitante"
								name="telefoneLicitante" value="${telefoneLicitante}"> <span
								class="glyphicon form-control-feedback" id="telefoneLicitante1"></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group has-feedback">
							<label for="pwd">Fax Licitante:</label> <input type="text"
								class="form-control" id="faxLicitante" name="faxLicitante"
								value="${faxLicitante}"> <span
								class="glyphicon form-control-feedback" id="faxLicitante1"></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group has-feedback">
							<label for="pwd">Email Licitante:</label> <input type="text"
								class="form-control" id="emailLicitante" name="emailLicitante"
								value="${emailLicitante}"> <span
								class="glyphicon form-control-feedback" id="emailLicitante1"></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group has-feedback">
							<label for="pwd">Telefone Titular Licitante:</label> <input
								type="text" class="form-control" id="telefoneTitularLicitante"
								name="telefoneTitularLicitante"
								value="${telefoneTitularLicitante}"> <span
								class="glyphicon form-control-feedback"
								id="telefoneTitularLicitante1"></span>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group has-feedback">
							<label for="pwd">Email Titular Licitante:</label> <input
								type="text" class="form-control" id="emailTitularLicitante"
								name="emailTitularLicitante" value="${emailTitularLicitante}">
							<span class="glyphicon form-control-feedback"
								id="emailTitularLicitante1"></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group has-feedback">
							<label for="cpf">CNPJ:</label> <input type="text"
								class="form-control" id="cnpj" name="cnpj" value="${cnpj}">
							<span class="glyphicon form-control-feedback" id="cnpj1"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="endereco"></div>
		<div class="panel panel-info">
			<div class="panel-heading">Endere�o</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group has-feedback">
							<label for="pwd">CEP:</label> <input type="text"
								class="form-control" id="cep" name="cep" value="${cep}">
							<span class="glyphicon form-control-feedback" id="cep1"></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group has-feedback">
							<label for="pwd">Bairro:</label> <input type="text"
								class="form-control" id="bairro" name="bairro" value="${bairro}">
							<span class="glyphicon form-control-feedback" id="bairro1"></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group has-feedback">
							<label for="pwd">Cidade:</label> <input type="text"
								class="form-control" id="cidade" name="cidade" value="${cidade}">
							<span class="glyphicon form-control-feedback" id="cidade1"></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group has-feedback">
							<label for="pwd">Estado:</label> <input type="text"
								class="form-control" id="estado" name="estado" value="${estado}">
							<span class="glyphicon form-control-feedback" id="estado1"></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group has-feedback">
							<label for="pwd">Rua:</label> <input type="text"
								class="form-control" id="rua" name="rua" value="${rua}">
							<span class="glyphicon form-control-feedback" id="rua1"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-sm-6">
				<button type="submit" class="btn btn-success"
					id="submitSaveSecretaria"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar Secretaria</button>
					
					<button type="button" class="btn btn-primary" id="lis-orgaos"><i class="fa fa-list" aria-hidden="true"></i>
					Listar Org�os</button>
			</div>

			<div class="col-sm-6">
				<div class="group-button-footer">
					<span class="label label-primary" id="topo"><i class="fa fa-arrow-up" aria-hidden="true"></i></span>
				</div>
			</div>
		</div>
	</form>

</div>
<hr>
<%@include file="footer.jsp"%>
<script type="text/javascript">
	$(document).ready(function() {
		adminController.index();
		tools.validationCNPJ();
		adminSecretaria.index();
		tools.clickSpanScroll();
		tools.setTitle('Cadastrar Nova Secretaria');
		$('#lis-orgaos').click(function(){
			window.location.href = "/SisCapV2/secretaria/listSecretaria";
		});
	});
</script>
