<%@ page contentType="text/html; charset=UTF-8" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <%@include file="home.jsp" %>

            <div class="container">
                <h2> Cadastro de Tramitações </h2>
            </div>

            <hr>

            <div class="container">
            
                <form class="form" id="saveFormTramite">

					<input type="hidden" name="id" id="idTramitacao" value="${idTramitacao}"/>
					<input type="hidden" name="idProcesso" id="idProcesso" value="${idProcesso}"/>
					<input type="hidden" name="idSecretaria" id="idSecretaria" value="${idSecretaria}"/>

                    <div class="panel panel-primary">
                        <div class="panel-heading">Dados da tramitação</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group has-feedback">
                                        <label for="motivoTramit">Motivo:</label>
                                        <input id="motivoTramit" class="form-control" name="motivoTramit" value="${motivoTramit}" placeholder="Motivo" />
                                        <span class="glyphicon form-control-feedback" id="motivoTramit1"></span>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group has-feedback">
                                        <label for="responsTramit">Responsável:</label>
                                        <input id="responsTramit" class="form-control" name=responsTramit value="${responsTramit}" placeholder="Responsável" />
                                        <span class="glyphicon form-control-feedback" id="motivoTramit1"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group has-feedback">
                                        <label for="retornoTramit">Data Retorno:</label>
                                        <input type="text" class="form-control datepicker" id="retornoTramit" name="retornoTramit" value="${retornoTramit}">
                                        <span class="glyphicon form-control-feedback" id="retornoTramit1"></span>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group has-feedback">
                                        <label for="saidaTramit">Data Saída:</label>
                                        <input type="text" class="form-control datepicker" id="saidaTramit" name="saidaTramit" value="${saidaTramit}">
                                        <span class="glyphicon form-control-feedback" id="saidaTramit1"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group has-feedback">
                                        <label for="fkProcesso">Processo:</label>
                                        <select id="fkProcesso" class="form-control selectField" name="fkProcesso">

                                        </select>
                                        <span class="glyphicon form-control-feedback" id="fkProcesso1"></span>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group has-feedback">
                                        <label for="fkSecretaria">Secretaria:</label>
                                        <select id="fkSecretaria" class="form-control selectField" name="fkSecretaria">

                                        </select>
                                        <span class="glyphicon form-control-feedback" id="fkSecretaria1"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-success" id="submitSaveTramite"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar Tramitação</button>

                            <button type="button" class="btn btn-primary" id="listar-tramitação"><i class="fa fa-list" aria-hidden="true"></i> Listar Tramitação</button>
                        </div>
                    </div>
                </form>
            </div>
            <hr>
            <%@include file="footer.jsp" %>
                <script type="text/javascript">
                    $(document).ready(function() {
                    	adminController.index();
                        adminTramite.index();
                        tools.setTitle('Novo Tramite');
                        var idProcesso = $('#idProcesso').val();
                        var idSecretaria = $('#idSecretaria').val();
                        tools.setValueOptionSelect2(idProcesso, 'fkProcesso');
                        tools.setValueOptionSelect2(idSecretaria, 'fkSecretaria');
                        $('#listar-tramitação').click(function(){
                        	window.location.href = '/SisCapV2/tramitacao/listTramitacao';
                    	});
                    });
                </script>