<%@include file="home.jsp"%>

<div class="container">
	
	<div class="row link_page">
		<div class="col-sm-12">
			<span> Adicionar um novo Usu�rio </span><a href="/SisCapV2/user/addUser"> <i class="fa fa-user-plus" aria-hidden="true"></i> </a>
		</div>
	</div>

	<div class="ibox-content">
		<table id="tableUser" class="table table-striped table-bordered" width="100%">
			<thead>
				<tr>
					<th>Nome</th>
					<th>Matr�cula</th>
					<th>Fun��o</th>
					<th width="10px"></th>
					<th width="10px"></th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<hr>
<%@include file="footer.jsp"%>
<script>
$(document).ready(function() {	
	adminController.index();
	tools.setTitle('Lista de Usu�rios');
	user.loadTableUser();
});
</script> 

