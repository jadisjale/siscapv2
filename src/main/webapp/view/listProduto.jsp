<%@include file="home.jsp"%>

<div class="container">
	<h2> Lista de Produtos </h2>
</div>

<div class="row link_page">
		<div class="col-sm-12">
			<span> Adicionar um novo produto </span><a
				href="/SisCapV2/produto/addProduto">
				<i class="fa fa-plus" aria-hidden="true"></i>
			</a>
		</div>
	</div>
<hr>
<hr>
<div class="container">
	<div class="ibox-content">
		<table id="tableProduto" class="table table-striped table-bordered" width="100%">
			<thead>
				<tr>
					<th>Descri��o</th>
					<th>Valor</th>
					<th>Fornecedor</th>
					<th width="15px"></th>
					<th width="15px"></th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<hr>
<%@include file="footer.jsp"%>
<script>
$(document).ready(function() {	
		adminController.index();
		tools.setTitle('Lista de Produto');
		adminProduto.indexList();
});
</script> 

