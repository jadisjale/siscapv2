<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SISCAP | LOGIN</title>

<!-- imports js -->
<script src="/SisCapV2/resources/js/jquery.js" type="text/javascript"></script>
<!-- finish -->

<!-- imports css -->
<link href="/SisCapV2/resources/css/bootstrap/css/bootstrap-theme.min.css"
	rel="stylesheet">
<link href="/SisCapV2/resources/css/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<link href="/SisCapV2/resources/css/custom/Login.css" rel="stylesheet">
<link href="/SisCapV2/resources/css/custom/font-awesine.css" rel="stylesheet">
<link href="/SisCapV2/resources/css/custom/animate.css" rel="stylesheet">

<!-- finish -->

</head>
<body>
<body class="gray-bg">
	<div class="middle-box text-center loginscreen animated fadeInDown">
		<div>
			<div>
				<h1 class="logo-name">SISCAP</h1>
			</div>
			<h3>Bem vindo(a) ao SISCAP</h3>
			<p>O SISCAP � um sistema para acompanhamento de licita��o.</p>
			<p>Entre. Para v�-lo em a��o.</p>
			<form class="m-t" role="form" action="/SisCapV2/j_spring_security_check" method="post">
				<div class="form-group">
					<input type="text" name="j_username" class="form-control" placeholder="Matr�cula"
						required="required" value="${not empty login_error ? SPRING_SECURITY_LAST_USERNAME : ''}" >
				</div>
				<div class="form-group">
					<input type="password" name="j_password" class="form-control" placeholder="Senha"
						required="">
				</div>
				<button type="submit" class="btn btn-primary block full-width m-b">Login</button>
				Motivo: ${fn:replace(SPRING_SECURITY_LAST_EXCEPTION.message, 'Bad credentials', 'Usu�rio/Senha incorreta')}
			</form>
			<p class="m-t">
				<small>Desenvolvido por WeSoluctions &copy; 2016</small>
			</p>
		</div>
	</div>
</body>

<script type="text/javascript" src="/SisCapV2/resources/js/custom/adminLogin.js"></script>
<script>
	$(document).ready(function() {

	});
</script>
</html>