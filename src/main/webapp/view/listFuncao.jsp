<%@include file="home.jsp"%>

<div class="container">
	
	<div class="row link_page">
		<div class="col-sm-12">
			<span> Adicionar uma nova Fun��o </span><a href="/SisCapV2/funcao/addFuncao"> <i class="fa fa-funcao-plus" aria-hidden="true"></i> </a>
		</div>
	</div>

	<div class="ibox-content">
		<table id="tableFuncao" class="table table-striped table-bordered" width="100%">
			<thead>
				<tr>
					<th>Fun��o</th>
					<th width="10px"></th>
					<th width="10px"></th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<hr>
<%@include file="footer.jsp"%>
<script>
$(document).ready(function() {	
	adminController.index();
	tools.setTitle('Lista de Fun��es');
	funcao.loadTableFuncao();
});
</script> 

