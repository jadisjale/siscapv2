<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>SISCAP | MENU</title>

    <link rel="ICON" href="favicon.ico" type="image/ico" />

    <link rel="stylesheet" href="/SisCapV2/resources/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/SisCapV2/resources/css/bootstrap/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="/SisCapV2/resources/css/bootstrap/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="/SisCapV2/resources/css/bootstrap/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/SisCapV2/resources/css/custom/siscap.css">
    <link rel="stylesheet" href="/SisCapV2/resources/css/bootstrap/css/datepicker.css">
    <link rel="stylesheet" href="/SisCapV2/resources/css/custom/select2.css">
    <link rel="stylesheet" href="/SisCapV2/resources/css/select2-bootstrap.css">
    <link rel="stylesheet" href="/SisCapV2/resources/css/toastr.min.css">
    <link rel="stylesheet" href="/SisCapV2/resources/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/SisCapV2/resources/js/morris.js-0.5.1/morris.css">

    <script src="/SisCapV2/resources/js/jquery.js"></script>
    <script src="/SisCapV2/resources/js/select2.js"></script>
    <script src="/SisCapV2/resources/js/mask.js"></script>
    <script src="/SisCapV2/resources/js/jquery.maskMoney.js"></script>
    <script src="/SisCapV2/resources/js/bootstrap/js/bootstrap.min.js"></script>
    <script src="/SisCapV2/resources/js/bootstrap/js/jquery.dataTables.js"></script>
    <script src="/SisCapV2/resources/js/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="/SisCapV2/resources/js/bootstrap/js/bootstrap-filestyle.js"></script>
    <script src="/SisCapV2/resources/js/bootstrap/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="/SisCapV2/resources/js/bootstrap/js/bootstrap-datepicker.js"></script>

    <script type="text/javascript" src="/SisCapV2/resources/js/custom/adminUser.js"></script>
    <script type="text/javascript" src="/SisCapV2/resources/js/custom/adminFuncao.js"></script>
    <script type="text/javascript" src="/SisCapV2/resources/js/custom/adminTramite.js"></script>
    <script type="text/javascript" src="/SisCapV2/resources/js/jquery.confirm.min.js"></script>
    <script type="text/javascript" src="/SisCapV2/resources/js/mask.js"></script>
    <script src="/SisCapV2/resources/js/raphael-min.js"></script>
    <script src="/SisCapV2/resources/js/morris.js-0.5.1/morris.min.js"></script>
    <script type="text/javascript" src="/SisCapV2/resources/js/custom/adminSecretaria.js"></script>
    <script type="text/javascript" src="/SisCapV2/resources/js/custom/adminProcesso.js"></script>
    <script type="text/javascript" src="/SisCapV2/resources/js/custom/adminFornecedorFisico.js"></script>
    <script type="text/javascript" src="/SisCapV2/resources/js/custom/adminFornecedorJuridico.js"></script>
    <script type="text/javascript" src="/SisCapV2/resources/js/custom/adminController.js"></script>
    <script type="text/javascript" src="/SisCapV2/resources/js/custom/adminProduto.js"></script>
    <script type="text/javascript" src="/SisCapV2/resources/js/custom/adminProduto.js"></script>
    <script type="text/javascript" src="/SisCapV2/resources/js/custom/Tools.js"></script>
    <script type="text/javascript" src="/SisCapV2/resources/js/toastr.min.js"></script>
    <script type="text/javascript" src="/SisCapV2/resources/js/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/SisCapV2/resources/js/custom/translate_validation.js"></script>
    <script type="text/javascript" src="/SisCapV2/resources/js/custom/graficos.js"></script>

</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/SisCapV2/admin/graficos">In�cio</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown"><a id="menu-fornecedor" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Fornecedor<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a id="fisico" href="/SisCapV2/fornecedor/addFornecedorFisico">Adicionar
								| F�sico</a></li>
                            <li><a id="juridico" href="/SisCapV2/fornecedor/addFornecedorJuridico">Adicionar
								| Jur�dico</a></li>
                            
                            <li><a id="produto" href="/SisCapV2/produto/addProduto">Adicionar
								Produto</a></li>
                            <li><a id="lista-juridico" href="/SisCapV2/fornecedor/listLicitanteJuridico">Lista jur�dica</a></li>
                            <li><a id="lista-fisico" href="/SisCapV2/orgaolicitante/listFisicos">Lista f�sicas</a></li>

                            <li><a id="lista-produto" href="/SisCapV2/produto/listProduto">Lista de
								Produtos</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a id="menu-processo" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Processo<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a id="processo" href="/SisCapV2/processo/addProcesso">Adicionar</a></li>
                            <li><a id="lista-processo" href="/SisCapV2/processo/listProcesso">Listar</a></li>
                        </ul>
                    </li>

                    <li class="dropdown"><a id="menu-secretaria" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Secretaria<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a id="secretaria" href="/SisCapV2/secretaria/addSecretaria">Adicionar</a></li>
                            <li><a id="lista-secretaria" href="/SisCapV2/secretaria/listSecretaria">Listar</a></li>
                        </ul>
                    </li>

                    <li class="dropdown"><a id="menu-tramitacao" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tramita��o<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a id="tramitacao" href="/SisCapV2/tramitacao/addTramite">Adicionar</a></li>
                            <li><a id="lista-tramitacao" href="/SisCapV2/tramitacao/listTramitacao">Listar</a></li>
                        </ul>
                    </li>

                    <li class="dropdown"><a id="menu-usuario" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Usu�rio<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a id="usuario" href="/SisCapV2/user/addUser">Adicionar</a></li>
                            <li><a id="listar-usuario" a href="/SisCapV2/user/listUser">Listar</a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Ajuda <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/SisCapV2/logout">Sair</a></li>
                            <li><a href="#">Sobre</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <img class="logo" src="/SisCapV2/resources/image/R�tuloCD5.png" alt="Siscap">
                </div>

                <div class="col-sm-10">
                    <h2 class="title-page" id="title-page"> T�tulo da p�gina </h2>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="usuario-logado-funcao" value="${funcaoUsuarioLogado}">

    <script type="text/javascript">
        tools.setTitle('Bem vindo ao SISCAP');
    </script>