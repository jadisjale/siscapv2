<%@include file="home.jsp"%>
<div class="container">
	<h2> Lista de Tramita��es</h2>
</div>
<hr>
<div class="container">
	<div class="row link_page">
		<div class="col-sm-12">
			<span> Adicionar uma nova Tramita��o </span><a
				href="/SisCapV2/tramitacao/addTramite">
				<i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
			</a>
		</div>
	</div>
	<div class="ibox-content">
		<table id="tableTramite" class="table table-striped table-bordered" width="100%">
			<thead>
				<tr>
					<th>Respons�vel</th>
					<th>Motivo</th>
					<th>N� Processo</th>
					<th>Licitante</th>
					<th width="10px"></th>
					<th width="10px"></th>
					<th width="10px"></th>
				</tr>
			</thead>
		</table>
	</div>
</div>

<hr>
<%@include file="footer.jsp"%>
<script>
$(document).ready(function() {	
	adminController.index();
	tools.setTitle('Lista de tramita��o');
	adminTramite.index();
});
</script> 

