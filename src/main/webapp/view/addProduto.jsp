<%@include file="home.jsp"%>

<div class="container">
	<form role="form" id="saveFormProduto">

		<input id="idProduto" type="hidden" name="idProduto" value="${idProduto}"></input> 
		<input type="hidden" id="fk" value="${fornecedor}"></input>

		<div class="panel panel-primary">
			<div class="panel-heading">Produto</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group has-feedback">
							<label for="email">Descri��o:</label> <input type="text"
								class="form-control" id="descricao" name="descricao"
								value="${descricao}"> <span
								class="glyphicon form-control-feedback" id="descricao1"></span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group has-feedback">
							<label for="email">Valor:</label> <input type="text"
								class="form-control" id="valor" name="valor" value="${valor}">
							<span class="glyphicon form-control-feedback" id="valor1"></span>
						</div>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group has-feedback">
							<label for="email">Fornecedor:</label> <select id="fkFornecedor"
								class="form-control selectField" name="fkFornecedor" id="">

							</select> <span class="glyphicon form-control-feedback" id="fkFornecedor1"></span>
						</div>
					</div>
				</div>
			</div>
		</div>

		<button type="submit" class="btn btn-success" id="submitSaveProduto"><i class="fa fa-floppy-o" aria-hidden="true"></i> Salvar Produto</button>
		
		<button type="button" class="btn btn-primary" id="listProdutos"><i class="fa fa-list" aria-hidden="true"></i>
					Listar Produtos</button>

	</form>
</div>
<hr>
<%@include file="footer.jsp"%>
<script type="text/javascript">
	$(document).ready(function() {
		adminController.index();
		adminProduto.index();
		var fk = $('#fk').val();
		tools.setValueOptionSelect2(fk, 'fkFornecedor');
		tools.setTitle('Novo Produto');
		$('#listProdutos').click(function(){
			window.location.href = "/SisCapV2/produto/listProduto";
		});
	});
</script>